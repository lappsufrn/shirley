
##############################################################################
#
# esys-escript:
# Copyright (c) 2003-2021 by The University of Queensland
# http://www.uq.edu.au
#
# Primary Business: Queensland, Australia
# Licensed under the Apache License, version 2.0
# http://www.apache.org/licenses/LICENSE-2.0
#
# Development until 2012 by Earth Systems Science Computational Center (ESSCC)
# Development 2012-2013 by School of Earth Sciences
# Development from 2014 by Centre for Geoscience Computing (GeoComp)
# Development from 2019 by School of Earth and Environmental Sciences
#
# Shirley domain module:
# Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
# and The University of Queensland
# https://www.ufrn.br
# http://www.uq.edu.au
#
# Development from 2018 by Departamento de Engenharia de Computação e Automação
#
# Primary Business: Rio Grande do Norte, Brazil
# Licensed under the Apache License, version 2.0
# http://www.apache.org/licenses/LICENSE-2.0
#
##############################################################################

import sys
import time

from esys.escript import *
from esys.speckley import Rectangle
from esys.escript.pdetools import Locator
from esys.downunder import Ricker
import numpy as np

from shirley.shirleycpp import ReadGmsh

order = 5  # Polynomial order
strategy = 2  # 0=Connectivity, 1=Distance, 2=Hilbert curves

# Horizontal axis: from 0 to 2000
# Vertical axis: from 0 to -1000
dom = ReadGmsh(filename="meshes/1m-horizon.msh", numDim=2, order=order,
               diracPoints=[(1000.0, -500.0)], diracTags=["wave-src"],
               traversalStrategy=strategy)

# Velocities in the layers
Velocities = [1500., 2500., 2000., 2500., 3000., 4000.]

# Rhos = Density of the medium in kg/m^3
Rhos = [2150.0, 1690.0, 2270.0, 2100.0, 2230.0, 2650.0]

v = Scalar(2000., Function(dom))
rho = Scalar(1000., Function(dom))

for i in range(len(Velocities)):
    insertTaggedValues(v, **{ "L%s" % (i + 1): Velocities[i] })
    insertTaggedValues(rho, **{ "L%s" % (i + 1): Rhos[i] })

# PDE coefficients.
# B = -1 * δ_kl
m_B = Tensor(-1. * kronecker(dom.getDim()), Function(dom))

# M = 1/K
m_M = (1.0 / (v**2 * rho))

# F = (1/rho) * δ_kl
m_F = (1.0 / rho) * kronecker(dom.getDim())

dom.setPdeCoefficients(F=m_F, M=m_M, B=m_B)

u0 = Scalar(0., Solution(dom))
v0 = Vector(0., Function(dom))
dom.setInitialSolution(u=u0, v=v0, currentTime=0.)

dt_adjust = 250.0 / 3.0
dom.calculateTimeIntegrationMatrices(timeStep=(0.0001 / dt_adjust))
print('Time step =', 0.0001 / dt_adjust, ' seconds')

f = 10.  # Frequency of the Ricker wavelet
dom.setRickerSignal(f_dom=f, t_dom=0., signalDelay=0.1)

cur_u = dom.getU()
cur_v = dom.getV()
output_dir = "output"
saveDataCSV(output_dir + "/u%s.csv" % 0, u=cur_u, x=cur_u.getFunctionSpace().getX())
print('u: inf=', inf(cur_u), ' , sup=', sup(cur_u))
print('v: inf=', inf(cur_v), ' , sup=', sup(cur_v))

start_time = time.perf_counter()

for k in range(80):
    print('k=', k)
    dom.progressToTime(finalTime=0.005 * (k + 1))
    cur_t = dom.getCurrentTime()
    cur_u = dom.getU()
    cur_v = dom.getV()
    saveDataCSV(output_dir + "/u%s.csv" % (k + 1), u=cur_u, x=cur_u.getFunctionSpace().getX())
    print('t=', cur_t)
    print('u: inf=', inf(cur_u), ', sup=', sup(cur_u))
    print('v: inf=', inf(cur_v), ', sup=', sup(cur_v))

end_time = time.perf_counter()
print('Elapsed time =', end_time - start_time, 'seconds')

print('Done.')
