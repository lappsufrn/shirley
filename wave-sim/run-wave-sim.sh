#! /bin/bash
#SBATCH --time=0-0:20160
#SBATCH --hint=compute_bound
#SBATCH --cpus-per-task=32
#SBATCH --exclusive

##############################################################################
#
# esys-escript:
# Copyright (c) 2003-2021 by The University of Queensland
# http://www.uq.edu.au
#
# Primary Business: Queensland, Australia
# Licensed under the Apache License, version 2.0
# http://www.apache.org/licenses/LICENSE-2.0
#
# Development until 2012 by Earth Systems Science Computational Center (ESSCC)
# Development 2012-2013 by School of Earth Sciences
# Development from 2014 by Centre for Geoscience Computing (GeoComp)
# Development from 2019 by School of Earth and Environmental Sciences
#
# Shirley domain module:
# Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
# and The University of Queensland
# https://www.ufrn.br
# http://www.uq.edu.au
#
# Development from 2018 by Departamento de Engenharia de Computação e Automação
#
# Primary Business: Rio Grande do Norte, Brazil
# Licensed under the Apache License, version 2.0
# http://www.apache.org/licenses/LICENSE-2.0
#
##############################################################################

export OMP_NUM_THREADS=32
export OMP_PROC_BIND=TRUE
export OMP_SCHEDULE=guided

echo 'Loading Environment Modules...'
module load compilers/gnu/7.3
module load libraries/binutils/2.31-gnu-7.3
module load libraries/boost/1.67-gnu-7.3
module load softwares/python/3.6.3-gnu-4.8_sh_lib

echo 'Defining LD_LIBRARY_PATH and PYTHONPATH variables for the Shirley module...'

# Contains "libboost_python38.so" or similar
BOOST_LIB_PATH="/usr/lib/x86_64-linux-gnu"
ESCRIPT_LIB_PATH="/path/to/escript-5.7/lib"
SHIRLEY_LIB_PATH="/path/to/shirley/lib/"

ESCRIPT_PATH="/path/to/escript-5.7"
SHIRLEY_PATH="/path/to/shirley"

export LD_LIBRARY_PATH=${BOOST_LIB_PATH}:${ESCRIPT_LIB_PATH}:${SHIRLEY_LIB_PATH}:${LD_LIBRARY_PATH}
export PYTHONPATH=${ESCRIPT_PATH}:${SHIRLEY_PATH}:${PYTHONPATH}

set -x

OUTPUT_DIR="output"

mkdir -p ${OUTPUT_DIR}

echo 'Running wave simulation...'
echo '' > ${OUTPUT_DIR}/wave-sim-output.txt
time python3 wave-sim.py  >> ${OUTPUT_DIR}/wave-sim-output.txt
