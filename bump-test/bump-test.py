
##############################################################################
#
# esys-escript:
# Copyright (c) 2003-2021 by The University of Queensland
# http://www.uq.edu.au
#
# Primary Business: Queensland, Australia
# Licensed under the Apache License, version 2.0
# http://www.apache.org/licenses/LICENSE-2.0
#
# Development until 2012 by Earth Systems Science Computational Center (ESSCC)
# Development 2012-2013 by School of Earth Sciences
# Development from 2014 by Centre for Geoscience Computing (GeoComp)
# Development from 2019 by School of Earth and Environmental Sciences
#
# Shirley domain module:
# Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
# and The University of Queensland
# https://www.ufrn.br
# http://www.uq.edu.au
#
# Development from 2018 by Departamento de Engenharia de Computação e Automação
#
# Primary Business: Rio Grande do Norte, Brazil
# Licensed under the Apache License, version 2.0
# http://www.apache.org/licenses/LICENSE-2.0
#
##############################################################################

import sys
import time

from esys.escript import *
from esys.speckley import Rectangle
from esys.escript.pdetools import Locator
import numpy as np

from shirley.shirleycpp import ReadGmsh

# Set up domain
dx = 40
nx = 32  # Should be even!
nz = nx

order = 5
strategy = 2  # 0=Connectivity, 1=Distance, 2=Space-filling curve
dom = ReadGmsh(filename="meshes/bump-mesh.msh", numDim=2, order=order,
               traversalStrategy=strategy)

# This is a case of a traveling wave 'bump' u=f(d.x-c*t),
# where vector d is the direction of travel (|d|=1).
# In this situation, we have v=-d/c*f(d.x-c*t).
c = 2000.     # Wave velocity in m/s
d = [1., 0.]  # or [0., 1.] - Other directions will not work due to boundary conditions

# f(s) = ( (1-(s-s0)/w)**2 )**pro_m for |s-s0|/w<1,
#      = 0                          otherwise.
#
# Half-width of the bump, should not be too small
w = 15 * dx
# Offset of the bump center from 0
s0 = 16 * dx
pro_m = 4

assert s0 - w > 0, "profile"
assert s0 + w < nx * dx, "profile"

def getProfile(x, t):
    a = (x[0] * d[0] + x[1] * d[1] - s0 - c * t) / w
    f = (1 - clip(a, minval=-1., maxval=1.) ** 2) ** pro_m
    return f

# Compute initial wavefield
rho = 1.  # Density of the medium in kg/m^3
u0 = getProfile(Solution(dom).getX(), t=0)
p = getProfile(Function(dom).getX(), t=0)
v0 = -p / c * d

print("Profile / v0 =", v0)
print("Profile / u0 =", u0)

# PDE coefficients and initial solution
m_M = Scalar(1. / (rho * c**2), Function(dom), expanded=True)
m_F = Tensor((1. / rho) * kronecker(dom.getDim()), Function(dom), expanded=True)
m_B = Tensor(-1. * kronecker(dom.getDim()), Function(dom), expanded=True)
dom.setPdeCoefficients(F=m_F, M=m_M, B=m_B)

dom.setInitialSolution(u=u0, v=v0, currentTime=0.)

# Position recorders along the line of travel
DPhones = 4

if abs(d[0]) > 0:
    recorderPositions = [(dx * i, dx * (nz // 2)) for i in range(2, nx - 2, DPhones)]
else:
    recorderPositions = [(dx * (nx // 2), dx * i) for i in range(2, nz - 2, DPhones)]

loc = Locator(Solution(dom), recorderPositions)
print(len(recorderPositions), "geophone(s)")
print("Recorder position(s) =", loc.getX())

# For the time integration loop
dt = 0.0001 * 10
timesteps = []
records = []

dtAdjust = 1.0
dom.calculateTimeIntegrationMatrices(timeStep=(0.0001 / dtAdjust))

loop_count = 31
cpp_u = None
cpp_v = None
save_wavefield_data = True

for k in range(loop_count):
    # Compute analytical profile
    u = getProfile(Solution(dom).getX(), t=dt * k)
    p = getProfile(Function(dom).getX(), t=dt * k)
    v = -p / c * d

    # Compute numerical wave
    if k > 0:
        dom.progressToTime(finalTime=dt * k)
        cpp_t = dom.getCurrentTime()
        cpp_u = dom.getU()
        cpp_v = dom.getV()
        
    # inf() and sup() should stay (mostly) the same as the profile is just
    # pushed forward
    print("Profile / u =", u)
    print("Profile / v[0] =", v[0])
    print("Profile / v[1] =", v[1])

    if (cpp_u is not None) and (cpp_v is not None):
        print("C++ / u =", cpp_u)
        print("C++ / v[0] =", cpp_v[0])
        print("C++ / v[1] =", cpp_v[1])

    # Save CSV files
    if save_wavefield_data:
        if k < 1:
            saveDataCSV("output/u%s.csv"%k, u=u0, x=u0.getFunctionSpace().getX())
            saveDataCSV("output/v%s.csv"%k, v=v0, x=v0.getFunctionSpace().getX())
        else:
            saveDataCSV("output/u%s.csv" % k, u=cpp_u, x=cpp_u.getFunctionSpace().getX())
            saveDataCSV("output/v%s.csv" % k, v=cpp_v, x=cpp_v.getFunctionSpace().getX())

        print("CSV files saved for step", k)

    timesteps.append(dt * k)
    
    if k < 1:
        records.append(loc(u))
    else:
        records.append(loc(cpp_u))

    print("Trace @ ", timesteps[-1], records[-1])
    print('k =', k, '/ Final time =', dt * k)

print("%d trace(s) at %s location(s)" % (len(timesteps), len(loc.getX())))
