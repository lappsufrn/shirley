
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#include <shirley/NodeFile.h>

namespace shirley {

dim_t NodeFile::createDenseDOFLabeling()
{
    const index_t UNSET_ID = -1, SET_ID = 1;

    // Get the global range of DOF IDs
    const IndexPair idRange(getGlobalDOFRange());

    // Distribute the range of DOF IDs
    IndexVector distribution(static_cast<unsigned long>(mpiInfo->size + 1));
    dim_t bufferLen = mpiInfo->setDistribution(idRange.first, idRange.second,
                                               &distribution[0]);

    IndexVector DOF_buffer(static_cast<unsigned long>(bufferLen));

    // Fill buffer by the UNSET_ID marker to check if nodes are defined
#pragma omp parallel for
    for (index_t n = 0; n < bufferLen; n++) {
        DOF_buffer[n] = UNSET_ID;
    }

    // Fill the buffer by sending portions around in a circle
#ifdef ESYS_MPI
    MPI_Status status;
    int dest = mpiInfo->mod_rank(mpiInfo->rank + 1);
    int source = mpiInfo->mod_rank(mpiInfo->rank - 1);
#endif

    int buffer_rank = mpiInfo->rank;

    for (int p = 0; p < mpiInfo->size; ++p) {
#ifdef ESYS_MPI
        if (p > 0) {  // The initial send can be skipped
            MPI_Sendrecv_replace(&DOF_buffer[0], bufferLen, MPI_DIM_T, dest,
                                 mpiInfo->counter(), source, mpiInfo->counter(),
                                 mpiInfo->comm, &status);
            mpiInfo->incCounter();
        }
#endif

        buffer_rank = mpiInfo->mod_rank(buffer_rank - 1);
        const index_t dof0 = distribution[buffer_rank];
        const index_t dof1 = distribution[buffer_rank + 1];

#pragma omp parallel for
        for (index_t n = 0; n < numNodes; n++) {
            const index_t k = globalDegreesOfFreedom[n];

            if ((dof0 <= k) && (k < dof1)) {
                DOF_buffer[k - dof0] = SET_ID;
            }
        }
    }

    // Count the entries in the buffer
    const dim_t myDOFs = distribution[mpiInfo->rank + 1] - distribution[mpiInfo->rank];
    dim_t myNewDOFs = 0;

    for (index_t i = 0; i < myDOFs; ++i) {
        if (DOF_buffer[i] == SET_ID) {
            DOF_buffer[i] = myNewDOFs;
            myNewDOFs++;
        }
    }

    IndexVector loc_offsets(mpiInfo->size, 0);
    IndexVector offsets(mpiInfo->size, 0);
    dim_t new_numGlobalDOFs;
    BoolVector set_new_DOF(static_cast<unsigned long>(numNodes));

#ifdef ESYS_MPI
    new_numGlobalDOFs = 0;
    loc_offsets[mpiInfo->rank] = myNewDOFs;

    MPI_Allreduce(&loc_offsets[0], &offsets[0], mpiInfo->size, MPI_DIM_T,
                  MPI_SUM, mpiInfo->comm);

    for (int i = 0; i < mpiInfo->size; ++i) {
        loc_offsets[i] = new_numGlobalDOFs;
        new_numGlobalDOFs += offsets[i];
    }
#else
    new_numGlobalDOFs = myNewDOFs;
#endif

//#pragma omp parallel
    {
//#pragma omp for
        for (index_t i = 0; i < myDOFs; ++i) {
            DOF_buffer[i] += loc_offsets[mpiInfo->rank];
        }

        // FIXME: This seems to fail randomly under OpenMP
//#pragma omp for
        for (index_t n = 0; n < numNodes; ++n) {
            set_new_DOF[n] = true;
        }
    }

    // Now entries are collected from the buffer again by sending them around
    // in a circle
#ifdef ESYS_MPI
    dest = mpiInfo->mod_rank(mpiInfo->rank + 1);
    source = mpiInfo->mod_rank(mpiInfo->rank - 1);
#endif

    buffer_rank = mpiInfo->rank;

    for (int p = 0; p < mpiInfo->size; ++p) {
        const index_t dof0 = distribution[buffer_rank];
        const index_t dof1 = distribution[buffer_rank + 1];

#pragma omp parallel for
        for (index_t n = 0; n < numNodes; n++) {
            const index_t k = globalDegreesOfFreedom[n];

            if (set_new_DOF[n] && (dof0 <= k) && (k < dof1)) {
                globalDegreesOfFreedom[n] = DOF_buffer[k - dof0];
                set_new_DOF[n] = false;
            }
        }

#ifdef ESYS_MPI
        if (p < mpiInfo->size - 1) {  // The last send can be skipped
            MPI_Sendrecv_replace(&DOF_buffer[0], bufferLen, MPI_DIM_T, dest,
                                 mpiInfo->counter(), source, mpiInfo->counter(),
                                 mpiInfo->comm, &status);
            mpiInfo->incCounter();
        }
#endif

        buffer_rank = mpiInfo->mod_rank(buffer_rank - 1);
    }

    return new_numGlobalDOFs;
}

dim_t NodeFile::createDenseNodeLabeling(IndexVector& nodeDistribution,
                                  const IndexVector& dofDistribution)
{
    const index_t UNSET_ID = -1, SET_ID = 1;
    const index_t myFirstDOF = dofDistribution[mpiInfo->rank];
    const index_t myLastDOF = dofDistribution[mpiInfo->rank + 1];

    // Find the range of node IDs controlled by me
    index_t min_id = escript::DataTypes::index_t_max();
    index_t max_id = escript::DataTypes::index_t_min();

#pragma omp parallel
    {
        index_t loc_min_id = min_id;
        index_t loc_max_id = max_id;

#pragma omp for
        for (index_t n = 0; n < numNodes; n++) {
            const index_t dof = globalDegreesOfFreedom[n];

            if ((myFirstDOF <= dof) && (dof < myLastDOF)) {
                loc_min_id = std::min(loc_min_id, Id[n]);
                loc_max_id = std::max(loc_max_id, Id[n]);
            }
        }

#pragma omp critical
        {
            min_id = std::min(loc_min_id, min_id);
            max_id = std::max(loc_max_id, max_id);
        }
    }

    dim_t myBufferLen = (max_id >= min_id) ? (max_id - min_id + 1) : 0;
    dim_t bufferLen;

#ifdef ESYS_MPI
    MPI_Allreduce(&myBufferLen, &bufferLen, 1, MPI_DIM_T, MPI_MAX,
                  mpiInfo->comm);
#else
    bufferLen = myBufferLen;
#endif

    const dim_t headerLen = 2;

    IndexVector Node_buffer(static_cast<unsigned long>(bufferLen + headerLen));

    // Mark and count the nodes in use
#pragma omp parallel
    {
#pragma omp for
        for (index_t i = 0; i < bufferLen + headerLen; i++) {
            Node_buffer[i] = UNSET_ID;
        }

#pragma omp for
        for (index_t n = 0; n < numNodes; n++) {
            globalNodesIndex[n] = -1;
            const index_t dof = globalDegreesOfFreedom[n];

            if ((myFirstDOF <= dof) && (dof < myLastDOF)) {
                Node_buffer[Id[n] - min_id + headerLen] = SET_ID;
            }
        }
    }

    dim_t myNewNumNodes = 0;

    for (index_t i = 0; i < myBufferLen; i++) {
        if (Node_buffer[headerLen + i] == SET_ID) {
            Node_buffer[headerLen + i] = myNewNumNodes;
            myNewNumNodes++;
        }
    }

    // Make the local number of nodes globally available
#ifdef ESYS_MPI
    MPI_Allgather(&myNewNumNodes, 1, MPI_DIM_T, &nodeDistribution[0], 1,
                  MPI_DIM_T, mpiInfo->comm);
#else
    nodeDistribution[0] = myNewNumNodes;
#endif

    dim_t globalNumNodes = 0;

    for (int p = 0; p < mpiInfo->size; ++p) {
        const dim_t itmp = nodeDistribution[p];
        nodeDistribution[p] = globalNumNodes;
        globalNumNodes += itmp;
    }

    nodeDistribution[mpiInfo->size] = globalNumNodes;

    // Offset node buffer
#pragma omp parallel for
    for (index_t i = 0; i < myBufferLen; i++) {
        Node_buffer[i + headerLen] += nodeDistribution[mpiInfo->rank];
    }

    // Now we send this buffer around to assign global node index
#ifdef ESYS_MPI
    int dest = mpiInfo->mod_rank(mpiInfo->rank + 1);
    int source = mpiInfo->mod_rank(mpiInfo->rank - 1);
#endif

    Node_buffer[0] = min_id;
    Node_buffer[1] = max_id;
    int buffer_rank = mpiInfo->rank;

    for (int p = 0; p < mpiInfo->size; ++p) {
        const index_t nodeID0 = Node_buffer[0];
        const index_t nodeID1 = Node_buffer[1];
        const index_t dof0 = dofDistribution[buffer_rank];
        const index_t dof1 = dofDistribution[buffer_rank + 1];

        if (nodeID0 <= nodeID1) {
#pragma omp parallel for
            for (index_t n = 0; n < numNodes; n++) {
                const index_t dof = globalDegreesOfFreedom[n];
                const index_t id = Id[n] - nodeID0;

                if ((dof0 <= dof) && (dof < dof1) &&
                    (id >= 0) && (id <= nodeID1 - nodeID0))
                    globalNodesIndex[n] = Node_buffer[id + headerLen];
            }
        }

#ifdef ESYS_MPI
        if (p < mpiInfo->size - 1) {  // The last send can be skipped
            MPI_Status status;
            MPI_Sendrecv_replace(&Node_buffer[0], bufferLen + headerLen, MPI_DIM_T,
                                 dest, mpiInfo->counter(), source,
                                 mpiInfo->counter(), mpiInfo->comm, &status);
            mpiInfo->incCounter();
        }
#endif

        buffer_rank = mpiInfo->mod_rank(buffer_rank - 1);
    }

    return globalNumNodes;
}

}  // namespace shirley
