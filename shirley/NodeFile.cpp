
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#include <shirley/Shirley.h>
#include <shirley/NodeFile.h>
#include <shirley/ShirleyException.h>

#include <escript/index.h>

#include <cstdlib>

namespace shirley {

static IndexPair getGlobalRange(dim_t n, const index_t* id,
                                const escript::JMPI& mpiInfo)
{
    IndexPair result(util::getMinMaxInt(1, n, id));

#ifdef ESYS_MPI
    index_t global_id_range[2];
    index_t id_range[2] = { -result.first, result.second };

    MPI_Allreduce(id_range, global_id_range, 2, MPI_DIM_T, MPI_MAX,
                  mpiInfo->comm);

    result.first = -global_id_range[0];
    result.second = global_id_range[1];
#endif

    if (result.second < result.first) {
        result.first = -1;
        result.second = 0;
    }

    return result;
}

/// Constructor.
/// Use NodeFile::allocTable() to allocate the node table (Id, Coordinates, etc)
NodeFile::NodeFile(int nDim, escript::JMPI mpiInfo) :
    numNodes(0),
    mpiInfo(mpiInfo),
    numDim(nDim),
    Id(nullptr),
    Tag(nullptr),
    globalDegreesOfFreedom(nullptr),
    Coordinates(nullptr),
    globalNodesIndex(nullptr),
    degreesOfFreedomId(nullptr),
    m_nodeType(nullptr),
    status(SHIRLEY_INITIAL_STATUS)
{
}

NodeFile::~NodeFile()
{
    freeTable();
}

void NodeFile::allocTable(dim_t new_numNodes)
{
    if (numNodes > 0) {
        freeTable();
    }

    Id = new index_t[new_numNodes];
    Tag = new int[new_numNodes];
    globalDegreesOfFreedom = new index_t[new_numNodes];
    Coordinates = new real_t[new_numNodes * numDim];
    globalNodesIndex = new index_t[new_numNodes];
    degreesOfFreedomId = new index_t[new_numNodes];
    m_nodeType = new u_char[new_numNodes];

    numNodes = new_numNodes;

#pragma omp parallel for
    for (index_t n = 0; n < numNodes; n++) {
        Id[n] = -1;
        Tag[n] = -1;
        globalDegreesOfFreedom[n] = -1;

        for (int i = 0; i < numDim; i++) {
            Coordinates[INDEX2(i, n, numDim)] = 0.0;
        }

        globalNodesIndex[n] = -1;
        degreesOfFreedomId[n] = -1;
        m_nodeType[n] = NodeType_Unknown;
    }
}

void NodeFile::freeTable()
{
    delete[] Id;
    delete[] Tag;
    delete[] globalDegreesOfFreedom;
    delete[] Coordinates;
    delete[] globalNodesIndex;
    delete[] degreesOfFreedomId;
    delete[] m_nodeType;

    numNodes = 0;
}

void NodeFile::print() const
{
    std::cout << "=== " << numDim << "D-Nodes:\n"
              << "number of nodes=" << numNodes
              << std::endl;
    std::cout << "Id,Tag,globalDegreesOfFreedom,degreesOfFreedom,"
                 "node,type,Coordinates"
              << std::endl;

    for (index_t n = 0; n < numNodes; n++) {
        std::cout << Id[n] << ","
                  << Tag[n] << ","
                  << globalDegreesOfFreedom[n] << ","
                  << degreesOfFreedomMapping.target[n] << ","
                  << nodesMapping.target[n] << ","
                  << m_nodeType[n]
                  << " ";

        std::cout.precision(15);
        std::cout.setf(std::ios::scientific, std::ios::floatfield);

        for (int i = 0; i < numDim; i++) {
            std::cout << Coordinates[INDEX2(i, n, numDim)] << " ";
        }

        std::cout << std::endl;
    }
}

/// sets tags to newTag where mask>0
void NodeFile::setTags(int newTag, const escript::Data& mask)
{
    if (1 != mask.getDataPointSize()) {
        throw escript::ValueError(
            "NodeFile::setTags(): number of components of mask must be 1");
    }
    else if ((mask.getNumDataPointsPerSample() != 1) ||
             (mask.getNumSamples() != numNodes)) {
        throw escript::ValueError(
            "NodeFile::setTags: illegal number of samples of mask Data object");
    }

    escript::DataTypes::real_t wantreal = 0.0;

#pragma omp parallel for
    for (index_t n = 0; n < numNodes; ++n) {
        if (mask.getSampleDataRO(n, wantreal)[0] > 0) {
            Tag[n] = newTag;
        }
    }

    updateTagList();
}

IndexPair NodeFile::getDOFRange() const
{
    IndexPair result(util::getMinMaxInt(1, numNodes, globalDegreesOfFreedom));

    if (result.second < result.first) {
        result.first = -1;
        result.second = 0;
    }

    return result;
}

IndexPair NodeFile::getGlobalIdRange() const
{
    return getGlobalRange(numNodes, Id, mpiInfo);
}

IndexPair NodeFile::getGlobalDOFRange() const
{
    return getGlobalRange(numNodes, globalDegreesOfFreedom, mpiInfo);
}

void NodeFile::assignMPIRankToDOFs(int* mpiRankOfDOF,
                                   const IndexVector& distribution)
{
    int p_min = mpiInfo->size, p_max = -1;

    // First we calculate the min and max DOF on this processor to reduce
    // costs for searching
    const IndexPair dofRange(getDOFRange());

    for (int p = 0; p < mpiInfo->size; ++p) {
        if (distribution[p] <= dofRange.first) {
            p_min = p;
        }

        if (distribution[p] <= dofRange.second) {
            p_max = p;
        }
    }

#pragma omp parallel for
    for (index_t n = 0; n < numNodes; ++n) {
        const index_t k = globalDegreesOfFreedom[n];

        for (int p = p_min; p <= p_max; ++p) {
            if (k < distribution[p + 1]) {
                mpiRankOfDOF[n] = p;
                break;
            }
        }
    }
}

void NodeFile::addNodeCapacity(dim_t numNodesToAdd) {
    if (numNodesToAdd < 0) {
        std::stringstream ss;
        ss << "NodeFile::addNodeCapacity(): number of nodes to add ("
           << numNodesToAdd << ") cannot be negative";
        throw ShirleyException(ss.str());
    }

    if (numNodesToAdd == 0) {
        return;
    }

    // Reallocates memory buffers
    dim_t new_numNodes = numNodes + numNodesToAdd;

    auto *new_Id = new index_t[new_numNodes];
    memcpy(new_Id, Id, numNodes * sizeof(index_t));
    delete[] Id;
    Id = new_Id;

    int *new_Tag = new int[new_numNodes];
    memcpy(new_Tag, Tag, numNodes * sizeof(int));
    delete[] Tag;
    Tag = new_Tag;

    auto *new_globalDegreesOfFreedom = new index_t[new_numNodes];
    memcpy(new_globalDegreesOfFreedom, globalDegreesOfFreedom, numNodes * sizeof(index_t));
    delete[] globalDegreesOfFreedom;
    globalDegreesOfFreedom = new_globalDegreesOfFreedom;

    auto *new_Coordinates = new real_t[new_numNodes * numDim];
    memcpy(new_Coordinates, Coordinates, numNodes * numDim * sizeof(real_t));
    delete[] Coordinates;
    Coordinates = new_Coordinates;

    auto *new_globalNodesIndex = new index_t[new_numNodes];
    memcpy(new_globalNodesIndex, globalNodesIndex, numNodes * sizeof(index_t));
    delete[] globalNodesIndex;
    globalNodesIndex = new_globalNodesIndex;

    auto *new_degreesOfFreedomId = new index_t[new_numNodes];
    memcpy(new_degreesOfFreedomId, degreesOfFreedomId, numNodes * sizeof(index_t));
    delete[] degreesOfFreedomId;
    degreesOfFreedomId = new_degreesOfFreedomId;

    auto *new_nodeType = new u_char[new_numNodes];
    memcpy(new_nodeType, m_nodeType, numNodes * sizeof(u_char));
    delete[] m_nodeType;
    m_nodeType = new_nodeType;

    // Initializes new memory buffers
#pragma omp parallel for
    for (index_t n = numNodes; n < new_numNodes; ++n) {
        Id[n] = -1;
        Tag[n] = 0;
        globalDegreesOfFreedom[n] = -1;

        for (int i = 0; i < numDim; ++i) {
            Coordinates[INDEX2(i, n, numDim)] = 0.0;
        }

        globalNodesIndex[n] = -1;
        degreesOfFreedomId[n] = -1;
        m_nodeType[n] = NodeType_Unknown;
    }

    numNodes = new_numNodes;
}

}  // namespace shirley
