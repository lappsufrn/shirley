
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#include <shirley/ShirleyDomain.h>
#include <shirley/Util.h>

namespace shirley {

void ShirleyDomain::resolveNodeIds()
{
    // Find the minimum and maximum node IDs used by elements
    index_t min_id = escript::DataTypes::index_t_max();
    index_t max_id = escript::DataTypes::index_t_min();

    IndexPair range(m_elements->getNodeRange());
    max_id = std::max(max_id, range.second);
    min_id = std::min(min_id, range.first);

    range = m_faceElements->getNodeRange();
    max_id = std::max(max_id, range.second);
    min_id = std::min(min_id, range.first);

    range = m_points->getNodeRange();
    max_id = std::max(max_id, range.second);
    min_id = std::min(min_id, range.first);

#ifdef SHIRLEY_TRACE
    index_t global_min_id, global_max_id;

#ifdef ESYS_MPI
    index_t id_range[2], global_id_range[2];
    id_range[0] = -min_id;
    id_range[1] =  max_id;

    MPI_Allreduce(id_range, global_id_range, 2, MPI_DIM_T, MPI_MAX, m_mpiInfo->comm);

    global_min_id = -global_id_range[0];
    global_max_id =  global_id_range[1];
#else
    global_min_id = min_id;
    global_max_id = max_id;
#endif

    printf("ShirleyDomain::resolveNodeIds(): "
           "node id range used by elements is %d:%d\n",
           global_min_id, global_max_id);
#endif

    if (min_id > max_id) {
        max_id = -1;
        min_id = 0;
    }

    // Allocate mappings for new local node labeling to global node labeling
    // (<newLocalToGlobalNodeLabels>) and global node labeling to the new local
    // node labeling; globalToNewLocalNodeLabels[i-min_id] is the new local ID
    // of global node i
    index_t len = (max_id >= min_id) ? (max_id - min_id + 1) : 0;

    // Mark in <usedMask> (a sparse array) the node IDs referred to by elements
    IntVector usedMask(len, -1);
    markNodes(usedMask, min_id);

    // Create a local labeling <newLocalToGlobalNodeLabels> (a dense array)
    // of the local nodes by packing the mask <usedMask>.
    // <newLocalToGlobalNodeLabels> will contain only the IDs (minus min_id)
    // of nodes referred to by elements.
    IndexVector newLocalToGlobalNodeLabels = util::packMask(usedMask);
    const dim_t new_numNodes = static_cast<dim_t>(newLocalToGlobalNodeLabels.size());

    usedMask.clear();

    // Invert the new labeling and shift the index <newLocalToGlobalNodeLabels>
    // to global node IDs. <globalToNewLocalNodeLabels> is a sparse array.
    IndexVector globalToNewLocalNodeLabels(static_cast<unsigned long>(len), -1);

#pragma omp parallel for
    for (index_t n = 0; n < new_numNodes; n++) {
#ifdef BOUNDS_CHECK
        ESYS_ASSERT(newLocalToGlobalNodeLabels[n] < len, "BOUNDS_CHECK");
        ESYS_ASSERT(newLocalToGlobalNodeLabels[n] >= 0,  "BOUNDS_CHECK");
#endif

        globalToNewLocalNodeLabels[newLocalToGlobalNodeLabels[n]] = n;
        newLocalToGlobalNodeLabels[n] += min_id;
    }

    // Create a new node file
    NodeFile* newNodeFile = new NodeFile(getDim(), m_mpiInfo);
    newNodeFile->allocTable(new_numNodes);

    if (len) {
        newNodeFile->gather_global(&newLocalToGlobalNodeLabels[0], m_nodes);
    }
    else {
        newNodeFile->gather_global(nullptr, m_nodes);
    }

    delete m_nodes;
    m_nodes = newNodeFile;

    // Relabel nodes of the elements
    relabelElementNodes(globalToNewLocalNodeLabels, min_id);
}

}  // namespace shirley
