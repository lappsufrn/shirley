
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#include <shirley/NodeFile.h>

#include <escript/index.h>

namespace shirley {

// Helper function
static void gatherEntries(dim_t n, const index_t* index,
                          index_t min_index, index_t max_index,
                          index_t* Id_out, const index_t* Id_in,
                          int* Tag_out, const int* Tag_in,
                          index_t* globalDegreesOfFreedom_out,
                          const index_t* globalDegreesOfFreedom_in,
                          int numDim, real_t* Coordinates_out,
                          const real_t* Coordinates_in,
                          u_char* nodeType_out, const u_char* nodeType_in)
{
    const dim_t range = max_index - min_index;
    const size_t numDim_size = numDim * sizeof(real_t);

#pragma omp parallel for
    for (index_t i = 0; i < n; i++) {
        const index_t k = index[i] - min_index;

        if ((k >= 0) && (k < range)) {
            Id_out[i] = Id_in[k];
            Tag_out[i] = Tag_in[k];
            globalDegreesOfFreedom_out[i] = globalDegreesOfFreedom_in[k];
            memcpy(&Coordinates_out[INDEX2(0, i, numDim)],
                   &Coordinates_in [INDEX2(0, k, numDim)], numDim_size);
            nodeType_out[i] = nodeType_in[k];
        }
    }
}

// Helper function
static void scatterEntries(dim_t n, const index_t* index,
                           index_t min_index, index_t max_index,
                           index_t* Id_out, const index_t* Id_in,
                           int* Tag_out, const int* Tag_in,
                           index_t* globalDegreesOfFreedom_out,
                           const index_t* globalDegreesOfFreedom_in,
                           int numDim, real_t* Coordinates_out,
                           const real_t* Coordinates_in,
                           u_char* nodeType_out, const u_char* nodeType_in)
{
    const dim_t range = max_index - min_index;
    const size_t numDim_size = numDim * sizeof(real_t);

#pragma omp parallel for
    for (index_t i = 0; i < n; i++) {
        const index_t k = index[i] - min_index;

        if ((k >= 0) && (k < range)) {
            Id_out[k] = Id_in[i];
            Tag_out[k] = Tag_in[i];
            globalDegreesOfFreedom_out[k] = globalDegreesOfFreedom_in[i];
            memcpy(&Coordinates_out[INDEX2(0, k, numDim)],
                   &Coordinates_in [INDEX2(0, i, numDim)], numDim_size);
            nodeType_out[k] = nodeType_in[i];
        }
    }
}

void NodeFile::gather(const index_t* index, const NodeFile* in)
{
    const IndexPair idRange(in->getGlobalIdRange());

    gatherEntries(numNodes, index, idRange.first, idRange.second, Id, in->Id,
        Tag, in->Tag, globalDegreesOfFreedom, in->globalDegreesOfFreedom,
        numDim, Coordinates, in->Coordinates, m_nodeType, in->m_nodeType);
}

void NodeFile::gather_global(const index_t* index, const NodeFile* in)
{
    // Get the global range of node IDs
    const IndexPair idRange(in->getGlobalIdRange());
    const index_t UNDEFINED_ID = idRange.first - 1;
    IndexVector distribution(static_cast<unsigned long>(in->mpiInfo->size + 1));

    // Distribute the range of node IDs
    dim_t buffer_len = mpiInfo->setDistribution(idRange.first, idRange.second,
                                                &distribution[0]);

    // Allocate buffers
    unsigned long buffer_len_ul = static_cast<unsigned long>(buffer_len);

    IndexVector Id_buffer(buffer_len_ul);
    IntVector Tag_buffer(buffer_len_ul);
    IndexVector globalDegreesOfFreedom_buffer(buffer_len_ul);
    RealVector Coordinates_buffer(static_cast<unsigned long>(buffer_len * numDim));
    std::vector<u_char> nodeType_buffer(buffer_len_ul);

    // Fill Id_buffer by the UNDEFINED_ID marker to check if nodes are
    // defined
#pragma omp parallel for
    for (index_t i = 0; i < buffer_len; i++) {
        Id_buffer[i] = UNDEFINED_ID;
    }

    // Fill the buffer by sending portions around in a circle
#ifdef ESYS_MPI
    MPI_Status status;
    int dest = mpiInfo->mod_rank(mpiInfo->rank + 1);
    int source = mpiInfo->mod_rank(mpiInfo->rank - 1);
#endif

    int buffer_rank = mpiInfo->rank;

    for (int p = 0; p < mpiInfo->size; ++p) {
#ifdef ESYS_MPI
        if (p > 0) {  // The initial send can be skipped
            MPI_Sendrecv_replace(&Id_buffer[0], buffer_len, MPI_DIM_T, dest,
                mpiInfo->counter(), source, mpiInfo->counter(),
                mpiInfo->comm, &status);
            MPI_Sendrecv_replace(&Tag_buffer[0], buffer_len, MPI_INT, dest,
                mpiInfo->counter() + 1, source,
                mpiInfo->counter() + 1, mpiInfo->comm, &status);
            MPI_Sendrecv_replace(&globalDegreesOfFreedom_buffer[0], buffer_len,
                MPI_DIM_T, dest, mpiInfo->counter() + 2, source,
                mpiInfo->counter() + 2, mpiInfo->comm, &status);
            MPI_Sendrecv_replace(&Coordinates_buffer[0], buffer_len * numDim,
                MPI_DOUBLE, dest, mpiInfo->counter() + 3, source,
                mpiInfo->counter() + 3, mpiInfo->comm, &status);
            MPI_Sendrecv_replace(&nodeType_buffer[0], buffer_len,
                MPI_UNSIGNED_CHAR, dest,
                mpiInfo->counter() + 4, source,
                mpiInfo->counter() + 4, mpiInfo->comm, &status);
            mpiInfo->incCounter(5);
        }
#endif

        buffer_rank = mpiInfo->mod_rank(buffer_rank - 1);

        scatterEntries(in->numNodes, in->Id, distribution[buffer_rank],
                       distribution[buffer_rank + 1], &Id_buffer[0], in->Id,
                       &Tag_buffer[0], in->Tag, &globalDegreesOfFreedom_buffer[0],
                       in->globalDegreesOfFreedom, numDim, &Coordinates_buffer[0],
                       in->Coordinates, &nodeType_buffer[0], in->m_nodeType);
    }

    // Now entries are collected from the buffer again by sending the entries
    // around in a circle
#ifdef ESYS_MPI
    dest = mpiInfo->mod_rank(mpiInfo->rank + 1);
    source = mpiInfo->mod_rank(mpiInfo->rank - 1);
#endif

    buffer_rank = mpiInfo->rank;

    for (int p = 0; p < mpiInfo->size; ++p) {
        gatherEntries(numNodes, index, distribution[buffer_rank],
                      distribution[buffer_rank + 1], Id, &Id_buffer[0],
                      Tag, &Tag_buffer[0], globalDegreesOfFreedom,
                      &globalDegreesOfFreedom_buffer[0], numDim,
                      Coordinates, &Coordinates_buffer[0],
                      m_nodeType, &nodeType_buffer[0]);

#ifdef ESYS_MPI
        if (p < mpiInfo->size - 1) {  // The last send can be skipped
            MPI_Sendrecv_replace(&Id_buffer[0], buffer_len, MPI_DIM_T, dest,
                mpiInfo->counter(), source,
                mpiInfo->counter(), mpiInfo->comm, &status);
            MPI_Sendrecv_replace(&Tag_buffer[0], buffer_len, MPI_INT, dest,
                mpiInfo->counter() + 1, source,
                mpiInfo->counter() + 1, mpiInfo->comm, &status);
            MPI_Sendrecv_replace(&globalDegreesOfFreedom_buffer[0], buffer_len,
                MPI_DIM_T, dest, mpiInfo->counter() + 2, source,
                mpiInfo->counter() + 2, mpiInfo->comm, &status);
            MPI_Sendrecv_replace(&Coordinates_buffer[0], buffer_len * numDim,
                MPI_DOUBLE, dest, mpiInfo->counter() + 3, source,
                mpiInfo->counter() + 3, mpiInfo->comm, &status);
            MPI_Sendrecv_replace(&nodeType_buffer[0], buffer_len,
                MPI_UNSIGNED_CHAR, dest,
                mpiInfo->counter() + 4, source,
                mpiInfo->counter() + 4, mpiInfo->comm, &status);
            mpiInfo->incCounter(5);
        }
#endif

        buffer_rank = mpiInfo->mod_rank(buffer_rank - 1);
    }

#if DOASSERT
    // Check if all nodes are set
    index_t err = -1;

#pragma omp parallel for
    for (index_t n = 0; n < numNodes; ++n) {
        if (Id[n] == UNDEFINED_ID) {
#pragma omp critical
            err = n;
        }
    }

    if (err >= 0) {
        std::stringstream ss;
        ss << "NodeFile::gather_global(): node id " << Id[err]
           << " at position " << err << " is referenced but not defined.";
        throw escript::AssertException(ss.str());
    }
#endif  // DOASSERT
}

}  // namespace shirley
