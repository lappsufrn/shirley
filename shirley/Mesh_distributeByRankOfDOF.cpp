
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#include <shirley/ShirleyDomain.h>

namespace shirley {

/// Redistributes the Nodes and Elements including overlap
/// according to the DOF distribution. It will create an element colouring
/// but will not create any mappings.
void ShirleyDomain::distributeByRankOfDOF(const IndexVector& dofDistribution)
{
    IntVector mpiRankOfDOF(static_cast<unsigned long>(m_nodes->getNumNodes()));
    m_nodes->assignMPIRankToDOFs(&mpiRankOfDOF[0], dofDistribution);

    // First, the elements are redistributed according to mpiRankOfDOF
    // at the input the Node tables refer to a the local labeling of the nodes
    // while at the output they refer to the global labeling which is rectified
    // in the next step
    m_elements->distributeByRankOfDOF(&mpiRankOfDOF[0], m_nodes->Id);
    m_faceElements->distributeByRankOfDOF(&mpiRankOfDOF[0], m_nodes->Id);
    m_points->distributeByRankOfDOF(&mpiRankOfDOF[0], m_nodes->Id);

    // This will replace the node file!
    resolveNodeIds();

    // Create a local labeling of the DOFs
    const IndexPair dofRange(m_nodes->getDOFRange());
    const dim_t len = dofRange.second - dofRange.first + 1;

    // Local mask for used nodes
    IndexVector localDOF_mask((unsigned long) len);
    IndexVector localDOF_map(static_cast<unsigned long>(m_nodes->getNumNodes()));

#pragma omp parallel for
    for (index_t i = 0; i < len; i++) {
        localDOF_mask[i] = -1;
    }

#pragma omp parallel for
    for (index_t n = 0; n < m_nodes->getNumNodes(); n++) {
        localDOF_map[n] = -1;
    }

#pragma omp parallel for
    for (index_t n = 0; n < m_nodes->getNumNodes(); n++) {
#ifdef BOUNDS_CHECK
        ESYS_ASSERT(m_nodes->globalDegreesOfFreedom[n] - dofRange.first < len, "BOUNDS_CHECK");
        ESYS_ASSERT(m_nodes->globalDegreesOfFreedom[n] - dofRange.first >= 0, "BOUNDS_CHECK");
#endif

        localDOF_mask[m_nodes->globalDegreesOfFreedom[n] - dofRange.first] = n;
    }

    dim_t numDOFs = 0;

    for (index_t i = 0; i < len; i++) {
        const index_t k = localDOF_mask[i];

        if (k >= 0) {
            localDOF_mask[i] = numDOFs;
            numDOFs++;
        }
    }

#pragma omp parallel for
    for (index_t n = 0; n < m_nodes->getNumNodes(); n++) {
        localDOF_map[n] = localDOF_mask[
            m_nodes->globalDegreesOfFreedom[n] - dofRange.first];
    }

    // Create element coloring
    createColoring(&localDOF_map[0]);
}

}  // namespace shirley
