
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#include <shirley/ElementFile.h>
#include <shirley/Util.h>

#include <escript/index.h>

namespace shirley {

/// Assigns colors to the mesh elements, so that any two elements sharing
/// a global node will have different colors. This allows processing
/// elements of identical color simultaneously without the risk of race
/// conditions
void ElementFile::createColoring(dim_t dofMap_numNodes, const index_t* dofMap)
{
    if (numElements < 1) {
        return;
    }

    //const IndexPair idRange(util::getMinMaxInt(1, dofMap.size(), &dofMap[0]));
    const IndexPair idRange(util::getMinMaxInt(1, dofMap_numNodes, dofMap));

    const dim_t idLen = idRange.second - idRange.first + 1;

#pragma omp parallel for
    for (index_t e = 0; e < numElements; e++) {
        Color[e] = -1;
    }

    dim_t numUncoloredElements = numElements;
    minColor = 0;
    maxColor = -1;
    IndexVector maskDOF((unsigned long) idLen);

    while (numUncoloredElements > 0) {
        // Initialize the mask marking nodes used by a color
#pragma omp parallel for
        for (index_t n = 0; n < idLen; n++) {
            maskDOF[n] = -1;
        }

        numUncoloredElements = 0;

        const int numTotalNodesPerElem = getNumTotalNodesPerElem();

        for (index_t e = 0; e < numElements; e++) {
            if (Color[e] < 0) {
                // Find out if element <e> is independent from the elements
                // already colored:
                bool independent = true;

                for (int n = 0; n < numTotalNodesPerElem; n++) {
#ifdef BOUNDS_CHECK
                    ESYS_ASSERT(Nodes[INDEX2(n, e, numTotalNodesPerElem)] >= 0,
                        "BOUNDS_CHECK");
                    ESYS_ASSERT(Nodes[INDEX2(n, e, numTotalNodesPerElem)] < dofMap_numNodes,
                        "BOUNDS_CHECK");
                    ESYS_ASSERT(dofMap[Nodes[INDEX2(n, e, numTotalNodesPerElem)]] - idRange.first >= 0,
                        "BOUNDS_CHECK");
                    ESYS_ASSERT(dofMap[Nodes[INDEX2(n, e, numTotalNodesPerElem)]] - idRange.first < idLen,
                        "BOUNDS_CHECK");
#endif

                    if (maskDOF[dofMap[Nodes[INDEX2(n, e, numTotalNodesPerElem)]] - idRange.first] > 0)
                    {
                        independent = false;
                        break;
                    }
                }

                // If <e> is independent a new color is assigned and the nodes
                // are marked as being used
                if (independent) {
                    for (int n = 0; n < numTotalNodesPerElem; n++) {
                        maskDOF[dofMap[Nodes[INDEX2(n, e, numTotalNodesPerElem)]] - idRange.first] = 1;
                    }

                    Color[e] = maxColor + 1;
                } else {
                    numUncoloredElements++;
                }
            }
        }  // for all elements

        maxColor++;
    }  // end of while loop
}

}  // namespace shirley
