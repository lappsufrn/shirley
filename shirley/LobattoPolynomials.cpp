
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#include <algorithm>

#include <cstring>
#include <cmath>

#include <shirley/LobattoPolynomials.h>

namespace shirley {

// In the paper "A Lobatto interpolation grid over the triangle"
// (doi:10.1093/imamat/hxh077), Lobatto polynomials of degree <i> [Lo_i(x)]
// are defined as the first derivative of Legendre polynomials of degree
// <i+1> [L_{i+1}(x)]. The roots of these Legendre polynomials can be used
// to compute the coordinates of a master grid over a triangle (or tetrahedron).

// L_3(x) = 1/2(5x^3 - 3x)
// Lo_2(x) = L_3'(x) = 1/2(15x^2 - 3)
static double LO_2_ROOTS[2] = {
    -0.4472135954999579392818347, 0.4472135954999579392818347 };

// L_4(x) = 1/8(35x^4 - 30x^2 + 3)
// Lo_3(x) = L_4'(x) = 1/2(35x^3 - 15x)
static double LO_3_ROOTS[3] = {
    -0.6546536707079771437982925, 0.0, 0.6546536707079771437982925 };

// L_5(x) = 1/8(63x^5 - 70x^3 + 15x)
// Lo_4(x) = L_5'(x) = 315/8(x^4) - 105/4(x^2) + 15/8
static double LO_4_ROOTS[4] = {
    -0.7650553239294646928510030, -0.2852315164806450963141510,
     0.2852315164806450963141510,  0.7650553239294646928510030 };

// L_6(x) = 1/16(231x^6 - 315x^4 + 105x^2 - 5)
// Lo_5(x) = L_6'(x) = 693/8(x^5) - 315/4(x^3) + 105/8(x)
static double LO_5_ROOTS[5] = {
    -0.8302238962785669298720322, -0.4688487934707142138037719,
     0.0,
     0.4688487934707142138037719,  0.8302238962785669298720322 };

// L_7(x) = 1/16(429x^7 - 693x^5 + 315x^3 - 35x)
// Lo_6(x) = L_7'(x) = 3003/16(x^6) - 3465/16(x^4) + 945/16(x^2) - 35/16
static double LO_6_ROOTS[6] = {
    -0.8717401485096066153374458, -0.5917001814331423021445107, -0.2092992179024788687686573,
     0.2092992179024788687686573,  0.5917001814331423021445107,  0.8717401485096066153374458 };

// L_8(x) = 1/128(6435x^8 - 12012x^6 + 6930x^4 - 1260x^2 + 35)
// Lo_7(x) = L_8'(x) = 6435/16(x^7) - 9009/16(x^5) + 3465/16(x^3) - 315/16(x)
static double LO_7_ROOTS[7] = {
    -0.8997579954114601573123452, -0.6771862795107377534458854, -0.3631174638261781587107521,
     0.0,
     0.3631174638261781587107521,  0.6771862795107377534458854,  0.8997579954114601573123452 };

// L_9(x) = 1/128(12155x^9 - 25740x^7 + 18018x^5 - 4620x^3 + 315x)
// Lo_8(x) = L_9'(x) = 109395/128(x^8) - 45045/32(x^6) + 45045/64(x^4) - 3465/32(x^2) + 315/128
static double LO_8_ROOTS[8] = {
    -0.9195339081664588138289326, -0.7387738651055050750031061, -0.4779249498104444956611750,
    -0.1652789576663870246262197,  0.1652789576663870246262197,  0.4779249498104444956611750,
     0.7387738651055050750031061,  0.9195339081664588138289326 };

// L_10(x) = 1/256(46189x^10 - 109395x^8 + 90090x^6 - 30030x^4 + 3465x^2 - 63)
// Lo_9(x) = L_10'(x) = 230945/128(x^9) - 109395/32(x^7) + 135135/64(x^5) - 15015/32(x^3) + 3465/128(x)
static double LO_9_ROOTS[9] = {
    -0.9340014304080591343322741, -0.7844834736631444186224178, -0.5652353269962050064709639,
    -0.2957581355869393914319115,
     0.0,
     0.2957581355869393914319115,  0.5652353269962050064709639,  0.7844834736631444186224178,
     0.9340014304080591343322741 };

// L_11(x) = 88179/256(x^11) - 230945/256(x^9) + 109395/128(x^7) - 45045/128(x^5) + 15015/256(x^3) - 693/256(x)
// Lo_10(x) = L_11'(x) = 969969/256(x^10) - 2078505/256(x^8) + 765765/128(x^6) - 225225/128(x^4) + 45045/256(x^2) - 693/256
static double LO_10_ROOTS[10] = {
    -0.9448992722228822234075801, -0.8192793216440066783486415, -0.6328761530318606776628830,
    -0.3995309409653489322643497, -0.1365529328549275548640618,  0.1365529328549275548640618,
     0.3995309409653489322642696,  0.6328761530318606776624048,  0.8192793216440066783486415,
     0.9448992722228822234075801 };

// <dest> must be able to hold at least <polyDegree> elements
void LobattoPolynomials::fillLobattoPolynomialRoots(
    int polyDegree, double *dest)
{
    if (dest == nullptr) {
        throw ShirleyException(
            "LobattoPolynomials::fillLobattoPolynomialRoots(): "
            "destination buffer cannot be null");
    }

    double *source = nullptr;

    switch (polyDegree) {
    case 2:
        source = LO_2_ROOTS;
        break;
    case 3:
        source = LO_3_ROOTS;
        break;
    case 4:
        source = LO_4_ROOTS;
        break;
    case 5:
        source = LO_5_ROOTS;
        break;
    case 6:
        source = LO_6_ROOTS;
        break;
    case 7:
        source = LO_7_ROOTS;
        break;
    case 8:
        source = LO_8_ROOTS;
        break;
    case 9:
        source = LO_9_ROOTS;
        break;
    case 10:
        source = LO_10_ROOTS;
        break;
    default:
        throw ShirleyException(
            "LobattoPolynomials::fillLobattoPolynomialRoots(): "
            "invalid polynomial degree");
    }

    memcpy(dest, source, polyDegree * sizeof(double));
}

// <dest> must be able to hold at least <polyDegree+1> elements
void LobattoPolynomials::fillNuArrayTri3(int polyDegree, double *dest)
{
    if (dest == nullptr) {
        throw ShirleyException(
            "LobattoPolynomials::fillNuArrayTri3(): "
            "destination buffer cannot be null");
    }

    dest[0] = 0.0;
    dest[polyDegree] = 1.0;

    std::vector<double> roots(polyDegree - 1);
    fillLobattoPolynomialRoots(polyDegree - 1, roots.data());

    for (std::size_t i = 0; i < roots.size(); ++i) {
        // See Eq. 2.7, pg. 8 of doi:10.1093/imamat/hxh077
        double &root = roots[i];
        dest[i + 1] = 0.5 * (1.0 + root);
    }
}

// <xi> and <eta> must be able to hold at least
// (1/2)*(<polyDegree+1>)*(<polyDegree+2>) elements each
void LobattoPolynomials::fillXiAndEtaArraysTri3(
    int polyDegree, double *xi, double *eta)
{
    std::vector<double> nu(polyDegree + 1);
    fillNuArrayTri3(polyDegree, nu.data());

    int k;
    int idx = 0;

    for (int i = 1; i <= polyDegree + 1; ++i) {
        for (int j = 1; j <= polyDegree + 2 - i; ++j) {
            // Bad equation: see Eq. 3.1, pg. 11 of doi:10.1093/imamat/hxh077
            // Good equation: see Eq. 1.13, pg. 3 of doi:10.1093/imamat/hxh111
            k = polyDegree + 3 - i - j;
            //xi[idx] = (1.0 + (2.0 * nu[j - 1]) - (nu[i - 1] + nu[k - 1])) / 3.0;  // Bad equation
            xi[idx] = (1.0 + (2.0 * nu[i - 1]) - (nu[j - 1] + nu[k - 1])) / 3.0;  // Good equation
            eta[idx] = (1.0 + (2.0 * nu[j - 1]) - (nu[i - 1] + nu[k - 1])) / 3.0;

            ++idx;
        }
    }
}

static long long FACTORIAL_TABLE[] = {
    1LL, 1LL, 2LL, 6LL,
    24LL, 120LL, 720LL, 5040LL,
    40320LL, 362880LL, 3628800LL, 39916800LL,
    479001600LL, 6227020800LL, 87178291200LL, 1307674368000LL
};  // 16 elements

static long long factorial(int n)
{
    if (n < 0) {
        throw ShirleyException(
            "LobattoPolynomials::factorial(): "
            "factorials can only be computed for positive integers");
    }

    if (n <= 15) {
        return FACTORIAL_TABLE[n];
    }

    long long result = FACTORIAL_TABLE[15];

    for (long long i = 16; i <= n; ++i) {
        result *= i;
    }

    return result;
}

inline
static double pow_of_minus_one(int exponent)
{
    return (exponent % 2 == 0) ? +1.0 : -1.0;
}

inline
static double pow_of_two(double base)
{
    if (base == 0.0) {
        return 0.0;
    }

    if (base == 1.0) {
        return 1.0;
    }

    if (base == 2.0) {
        return 4.0;
    }

    return base * base;
}

double LobattoPolynomials::calcAppellCoefficient(int k, int l, int i, int j)
{
    // See Eq. 1.22, pg. 8 of doi:10.1093/imamat/hxh077
    double result = pow_of_minus_one(i + j)
        * (factorial(k + j) / pow_of_two(factorial(i)))
        * (factorial(l + i) / pow_of_two(factorial(j)))
        * (static_cast<double>(factorial(k + l)) / factorial(k + l - i - j));

    return result;
}

double LobattoPolynomials::calcAppellPolynomial(
    int k, int l, double xi, double eta, bool roundResultTowardZero,
    double roundingTolerance)
{
    double result = 0.0;
    double coeff;

    // See Eq. 1.21, pg. 8 of doi:10.1093/imamat/hxh077
    for (int i = 0; i <= k + l; ++i) {
        for (int j = 0; j <= k + l - i; ++j) {
            coeff = calcAppellCoefficient(k, l, i, j);
            result += (coeff * pow(xi, i) * pow(eta, j));
        }
    }

    if (roundResultTowardZero && (fabs(result) <= roundingTolerance)) {
        result = 0.0;
    }

    return result;
}

void LobattoPolynomials::sortXiAndEtaPairs(
    int arraySize, const double *xi, const double *eta,
    std::vector<std::pair<double, double>>& dest)
{
    dest.clear();

    for (int i = 0; i < arraySize; ++i) {
        dest.emplace_back(xi[i], eta[i]);
    }

    struct {
        bool operator()(const std::pair<double, double>& pair1,
                        const std::pair<double, double>& pair2) const
        {
            if (pair1.first < pair2.first) {
                return true;
            }

            if (pair1.first > pair2.first) {
                return false;
            }

            if (pair1.second < pair2.second) {
                return true;
            }

            if (pair1.second > pair2.second) {
                return false;
            }

            return false;
        }
    } xiEtaPairLess;

    std::sort(dest.begin(), dest.end(), xiEtaPairLess);
}

static inline
double derivativeOfPow(double base, int exponent)
{
    if (exponent == 0) {
        // <exponent>*<base>^(<exponent>-1) is always zero
        return 0.0;
    }

    if (exponent == 1) {
        // <exponent>*<base>^(<exponent>-1) is always 1
        return 1.0;
    }

    if (exponent == 2) {
        // <exponent>*<base>^(<exponent>-1) is 2*base
        return 2.0 * base;
    }

    // The derivative of <base>^<exponent> with respect to <base> is
    // <exponent>*<base>^(<exponent>-1)
    return exponent * pow(base, exponent - 1);
}

double LobattoPolynomials::calcDerivativeOfAppellPolynomialWithRespectToXi(
    int k, int l, double xi, double eta, bool roundResultTowardZero,
    double roundingTolerance)
{
    double result = 0.0;
    double coeff;

    // See Eq. 1.21, pg. 8 of doi:10.1093/imamat/hxh077
    for (int i = 0; i <= k + l; ++i) {
        for (int j = 0; j <= k + l - i; ++j) {
            // When i==0, the derivative of xi^i will always be 0.0
            if (i == 0) { continue; }

            coeff = calcAppellCoefficient(k, l, i, j);
            result += (coeff * derivativeOfPow(xi, i) * pow(eta, j));
        }
    }

    if (roundResultTowardZero && (fabs(result) <= roundingTolerance)) {
        result = 0.0;
    }

    return result;
}

double LobattoPolynomials::calcDerivativeOfAppellPolynomialWithRespectToEta(
    int k, int l, double xi, double eta, bool roundResultTowardZero,
    double roundingTolerance)
{
    double result = 0.0;
    double coeff;

    // See Eq. 1.21, pg. 8 of doi:10.1093/imamat/hxh077
    for (int i = 0; i <= k + l; ++i) {
        for (int j = 0; j <= k + l - i; ++j) {
            // When j==0, the derivative of eta^j will always be 0.0
            if (j == 0) { continue; }

            coeff = calcAppellCoefficient(k, l, i, j);
            result += (coeff * pow(xi, i) * derivativeOfPow(eta, j));
        }
    }

    if (roundResultTowardZero && (fabs(result) <= roundingTolerance)) {
        result = 0.0;
    }

    return result;
}

// <dest> must be able to hold at least two elements
void LobattoPolynomials::calcDerivativeOfAppellPolynomialWithRespectToXiAndEta(
    int k, int l, double xi, double eta, double *dest,
    bool roundResultTowardZero, double roundingTolerance)
{
    dest[0] = 0.0;
    dest[1] = 0.0;

    double coeff;

    // See Eq. 1.21, pg. 8 of doi:10.1093/imamat/hxh077
    for (int i = 0; i <= k + l; ++i) {
        for (int j = 0; j <= k + l - i; ++j) {
            coeff = calcAppellCoefficient(k, l, i, j);

            // When i==0, the derivative of xi^i will always be 0.0
            if (i != 0) {
                dest[0] += (coeff * derivativeOfPow(xi, i) * pow(eta, j));
            }

            // When j==0, the derivative of eta^j will always be 0.0
            if (j != 0) {
                dest[1] += (coeff * pow(xi, i) * derivativeOfPow(eta, j));
            }
        }
    }

    if (roundResultTowardZero) {
        if (fabs(dest[0]) <= roundingTolerance) {
            dest[0] = 0.0;
        }

        if (fabs(dest[1]) <= roundingTolerance) {
            dest[1] = 0.0;
        }
    }
}

}  // namespace shirley
