
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#include <cmath>

#include <shirley/Ricker.h>


namespace shirley {

Ricker::Ricker(double f_dom, double t_dom) {
  // Sets up a Ricker wavelet wih dominant frequency `f_dom` and
  // center at time `t_dom`. If `t_dom` is not given an estimate
  // for suitable `t_dom` is calculated so f(0)~0.
  //
  // :note: maximum frequency is about 2 x the dominant frequency.
  m_drop = 18.0;
  m_f = f_dom;
  m_f_max = sqrt(7.0) * f_dom;
  m_s = M_PI * m_f;

  if (t_dom <= 0.0) {
      t_dom = sqrt(m_drop) / m_s;
  }

  m_t0 = t_dom;
}

double Ricker::getCenter() {
  // Return value of wavelet center
  return m_t0;
}

double Ricker::getTimeScale() {
  // Returns the time scale which is the inverse of the largest
  // frequency with a significant spectral component.
  return 1.0 / m_f_max;
}

double Ricker::getValue(double t) {
  // get value of wavelet at time `t`
  double e2 = pow(m_s * (t - m_t0), 2.0);
  return (1.0 - (2.0 * e2)) * exp(-e2);
}

double Ricker::getVelocity(double t) {
  // get the velocity f'(t) at time `t`
  double e2 = pow(m_s * (t - m_t0), 2.0);
  return (-3.0 + (2.0 * e2)) * exp(-e2) * 2.0 * pow(m_s, 2.0) * (t - m_t0);
}

double Ricker::getAcceleration(double t) {
  // get the acceleration f''(t) at time `t`
  double e2 = pow(m_s * (t - m_t0), 2.0);
  return 2.0 * pow(m_s, 2.0) * ((-4.0 * pow(e2, 2.0)) + (12.0 * e2) - 3.0)
    * exp(-e2);
}


}  // namespace shirley
