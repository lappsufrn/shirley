
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#include <shirley/Shirley.h>
#include <shirley/ShirleyDomain.h>
#include <shirley/ShirleyException.h>
#include <shirley/Assemble.h>
#include <shirley/LobattoPolynomials.h>
#include <shirley/VdmSystemsTri3.h>
#include <shirley/Ricker.h>

#include <escript/Data.h>
#include <escript/DataFactory.h>
#include <escript/FunctionSpaceFactory.h>

#include <escript/Random.h>

#include <utility>
#include <algorithm>

using escript::NotImplementedError;
using escript::ValueError;
using namespace escript;
using namespace escript::DataTypes;

namespace shirley {

// Define static members
ShirleyDomain::FunctionSpaceNamesMapType ShirleyDomain::m_functionSpaceTypeNames;

ShirleyDomain::ShirleyDomain(const std::string& name, int numDim,
                             escript::JMPI mpiInfo) :
    m_mpiInfo(mpiInfo),
    m_name(name),
    m_elements(nullptr),
    m_faceElements(nullptr),
    m_points(nullptr),
    m_numUniqueEdges(0),
    m_numUniqueFaces(0),
    m_inputSignal(nullptr),
    m_signalDelay(0.0),
    m_timeStep(0.0),
    m_currentTime(0.0)
{
    // Allocate node table
    m_nodes = new NodeFile(numDim, m_mpiInfo);

    setFunctionSpaceTypeNames();
}

ShirleyDomain::ShirleyDomain(const ShirleyDomain& in) :
    m_mpiInfo(in.m_mpiInfo),
    m_name(in.m_name),
    m_nodes(in.m_nodes),
    m_elements(in.m_elements),
    m_faceElements(in.m_faceElements),
    m_points(in.m_points),
    m_numUniqueEdges(0),
    m_numUniqueFaces(0),
    m_inputSignal(nullptr)
{
    setFunctionSpaceTypeNames();

    // FIXME We're not filling m_numUniqueEdges, m_numUniqueFaces and
    //  m_inputSignal, this constructor is most likely broken.
    throw ShirleyException(
        "ShirleyDomain::ctor(): please do not use the copy constructor, "
        "not all necessary data gets defined currently.");

}

ShirleyDomain::~ShirleyDomain()
{
    delete m_nodes;
    delete m_elements;
    delete m_faceElements;
    delete m_points;
    delete m_inputSignal;
}

void ShirleyDomain::setElements(ElementFile* elements)
{
    delete m_elements;
    m_elements = elements;
}

void ShirleyDomain::setFaceElements(ElementFile* elements)
{
    delete m_faceElements;
    m_faceElements = elements;
}

void ShirleyDomain::setPoints(ElementFile* elements)
{
    delete m_points;
    m_points = elements;
}

void ShirleyDomain::MPIBarrier() const
{
#ifdef ESYS_MPI
    MPI_Barrier(getMPIComm());
#endif
}

void ShirleyDomain::write(const std::string& fileName) const {
    throw NotImplementedError(
        "ShirleyDomain::write(): "
        "this member function is currently not implemented.");
}

void ShirleyDomain::Print_Mesh_Info(bool full) const {
    throw NotImplementedError(
        "ShirleyDomain::Print_Mesh_Info(): "
        "this member function is currently not implemented.");
}

void ShirleyDomain::dump(const std::string& fileName) const
{
#ifdef ESYS_HAVE_NETCDF
    throw NotImplementedError(
        "ShirleyDomain::dump(): "
        "this member function is currently not implemented.");
#else
    throw ShirleyException("ShirleyDomain::dump(): not configured with netCDF. "
                           "Please contact your installation manager.");
#endif  // ESYS_HAVE_NETCDF
}

int ShirleyDomain::getTagFromSampleNo(int functionSpaceType, index_t sampleNo)
    const
{
    switch (functionSpaceType) {
        case Nodes:
            return m_nodes->Tag[sampleNo];
        case Elements:
            return m_elements->Tag[sampleNo];
        case FaceElements:
            return m_faceElements->Tag[sampleNo];
        case Points:
            return m_points->Tag[sampleNo];
        case DegreesOfFreedom:
            throw ValueError("ShirleyDomain::getTagFromSampleNo(): "
                             "DegreesOfFreedom does not support tags");
        default:
            break;
    }

    std::stringstream ss;
    ss << "ShirleyDomain::getTagFromSampleNo(): "
          "invalid function space type ("
       << functionSpaceTypeAsString(functionSpaceType)
       << ") for domain " << getDescription();
    throw ValueError(ss.str());
}

const index_t* ShirleyDomain::borrowSampleReferenceIDs(int functionSpaceType)
    const
{
    switch (functionSpaceType) {
        case DegreesOfFreedom:
        case Nodes:
            return m_nodes->Id;
        case Elements:
        case ReducedElements:
            return m_elements->Id;
        case FaceElements:
        case ReducedFaceElements:
            return m_faceElements->Id;
        case Points:
            return m_points->Id;
        default:
            break;
    }

    std::stringstream msg;
    msg << "ShirleyDomain::borrowSampleReferenceIDs(): "
           "invalid function space type ("
        << functionSpaceTypeAsString(functionSpaceType) << ")";
    throw ShirleyException(msg.str());
}

bool ShirleyDomain::isValidFunctionSpaceType(int functionSpaceType) const
{
    FunctionSpaceNamesMapType::iterator loc;
    loc = m_functionSpaceTypeNames.find(functionSpaceType);
    return (loc != m_functionSpaceTypeNames.end());
}

std::string ShirleyDomain::getDescription() const
{
    return "ShirleyMesh";
}

std::string ShirleyDomain::functionSpaceTypeAsString(int functionSpaceType)
    const
{
    auto loc = m_functionSpaceTypeNames.find(functionSpaceType);

    if (loc == m_functionSpaceTypeNames.end()) {
        return "Invalid function space type code.";
    } else {
        return loc->second;
    }
}

void ShirleyDomain::setFunctionSpaceTypeNames()
{
    m_functionSpaceTypeNames.clear();

    m_functionSpaceTypeNames.insert(FunctionSpaceNamesMapType::value_type(
        DegreesOfFreedom, "Shirley_DegreesOfFreedom [Solution(domain)]"));
    // Currently unused
    m_functionSpaceTypeNames.insert(FunctionSpaceNamesMapType::value_type(
        ReducedDegreesOfFreedom, "Shirley_ReducedDegreesOfFreedom [ReducedSolution(domain)]"));

    m_functionSpaceTypeNames.insert(FunctionSpaceNamesMapType::value_type(
        Nodes, "Shirley_Nodes [ContinuousFunction(domain)]"));
    // Currently unused
    m_functionSpaceTypeNames.insert(FunctionSpaceNamesMapType::value_type(
        ReducedNodes, "Shirley_ReducedNodes [ReducedContinuousFunction(domain)]"));

    m_functionSpaceTypeNames.insert(FunctionSpaceNamesMapType::value_type(
        Elements, "Shirley_Elements [Function(domain)]"));
    // Currently unused
    m_functionSpaceTypeNames.insert(FunctionSpaceNamesMapType::value_type(
        ReducedElements, "Shirley_ReducedElements [ReducedFunction(domain)]"));

    m_functionSpaceTypeNames.insert(FunctionSpaceNamesMapType::value_type(
        FaceElements, "Shirley_FaceElements [FunctionOnBoundary(domain)]"));
    // Currently unused
    m_functionSpaceTypeNames.insert(FunctionSpaceNamesMapType::value_type(
        ReducedFaceElements, "Shirley_ReducedFaceElements [ReducedFunctionOnBoundary(domain)]"));

    m_functionSpaceTypeNames.insert(FunctionSpaceNamesMapType::value_type(
        Points, "Shirley_Points [DiracDeltaFunctions(domain)]"));
}

int ShirleyDomain::getContinuousFunctionCode() const
{
    return Nodes;
}

int ShirleyDomain::getReducedContinuousFunctionCode() const
{
    return ReducedNodes;
}

int ShirleyDomain::getFunctionCode() const
{
    return Elements;
}

int ShirleyDomain::getReducedFunctionCode() const
{
    return ReducedElements;
}

int ShirleyDomain::getFunctionOnBoundaryCode() const
{
    return FaceElements;
}

int ShirleyDomain::getReducedFunctionOnBoundaryCode() const
{
    return ReducedFaceElements;
}

int ShirleyDomain::getFunctionOnContactZeroCode() const
{
    throw NotImplementedError(
        "ShirleyDomain::getFunctionOnContactZeroCode(): "
        "this member function is currently not implemented.");
}

int ShirleyDomain::getReducedFunctionOnContactZeroCode() const
{
    throw NotImplementedError(
        "ShirleyDomain::getReducedFunctionOnContactZeroCode(): "
        "this member function is currently not implemented.");
}

int ShirleyDomain::getFunctionOnContactOneCode() const
{
    throw NotImplementedError(
        "ShirleyDomain::getFunctionOnContactOneCode(): "
        "this member function is currently not implemented.");
}

int ShirleyDomain::getReducedFunctionOnContactOneCode() const
{
    throw NotImplementedError(
        "ShirleyDomain::getReducedFunctionOnContactOneCode(): "
        "this member function is currently not implemented.");
}

int ShirleyDomain::getSolutionCode() const
{
    return DegreesOfFreedom;
}

int ShirleyDomain::getReducedSolutionCode() const
{
    return ReducedDegreesOfFreedom;
}

int ShirleyDomain::getDiracDeltaFunctionsCode() const
{
    return Points;
}

ShirleyDomain::StatusType ShirleyDomain::getStatus() const
{
    return m_nodes->status;
}

//
// Return the number of data points summed across all MPI processes
//
dim_t ShirleyDomain::getNumDataPointsGlobal() const
{
    return m_nodes->getGlobalNumNodes();
}

//
// Return the number of data points per sample and the number of samples
// needed to represent data on a part of the mesh.
//
std::pair<int, dim_t> ShirleyDomain::getDataShape(int functionSpaceCode) const
{
    int numDataPointsPerSample = 0;
    dim_t numSamples = 0;

    switch (functionSpaceCode) {
        case DegreesOfFreedom:
            if (m_nodes) {
                numDataPointsPerSample = 1;
                numSamples = m_nodes->getNumDegreesOfFreedom();
            }
            break;

        case Nodes:
            if (m_nodes) {
                numDataPointsPerSample = 1;
                numSamples = m_nodes->numNodes;
            }
            break;

        case Elements:
            if (m_elements) {
                numDataPointsPerSample = m_elements->getNumTotalNodesPerElem();
                numSamples = m_elements->numElements;
            }
            break;

        case FaceElements:
            if (m_faceElements) {
                numDataPointsPerSample = m_faceElements->getNumTotalNodesPerElem();
                numSamples = m_faceElements->numElements;
            }
            break;

        case Points:
            if (m_points) {
                numDataPointsPerSample = 1;
                numSamples = m_points->numElements;
            }

            // FIXME We shouldn't need this! Ideally, Dirac points should
            //  be loaded from the mesh file
            if (!m_diracPoints.empty()) {
                numDataPointsPerSample = 1;
                numSamples += m_diracPoints.size();
            }

            break;

        default:
            std::stringstream ss;
            ss << "ShirleyDomain::getDataShape(): "
                  "invalid function space type ("
               << functionSpaceTypeAsString(functionSpaceCode)
               << ") for domain " << getDescription();
            throw ValueError(ss.str());
    }

    return std::make_pair(numDataPointsPerSample, numSamples);
}

//
// Copies the locations of data points into <dest>.
//
void ShirleyDomain::setToX(escript::Data& dest) const
{
    if (*dest.getFunctionSpace().getDomain() != *this) {
        throw ValueError(
            "ShirleyDomain::setToX(): illegal domain of <dest>");
    }

    if (!dest.isExpanded()) {
        throw ShirleyException(
            "ShirleyDomain::setToX(): expanded Data object expected in <dest>");
    }

    int functionSpaceType = dest.getFunctionSpace().getTypeCode();

    if (functionSpaceType == Nodes) {
        std::stringstream ss;
        ss << "ShirleyDomain::setToX(): "
              "this member function is currently not implemented "
              "for function space type "
           << functionSpaceTypeAsString(functionSpaceType);
        throw NotImplementedError(ss.str());
    } else {
        escript::Data tmp_data =
            escript::Vector(0., continuousFunction(*this), true);
        Assemble_NodeCoordinates(m_nodes, tmp_data);

        // This is then interpolated onto dest:
        interpolateOnDomain(dest, tmp_data);
    }
}

void ShirleyDomain::setTagMap(const std::string& name, int tag)
{
    m_tagMap[name] = tag;
}

int ShirleyDomain::getTag(const std::string& name) const
{
    auto it = m_tagMap.find(name);

    if (it == m_tagMap.end()) {
        std::stringstream ss;
        ss << "ShirleyDomain::getTag(): unknown tag name " << name;
        throw ValueError(ss.str());
    }

    return it->second;
}

bool ShirleyDomain::isValidTagName(const std::string& name) const
{
    return (m_tagMap.count(name) > 0);
}

std::string ShirleyDomain::showTagNames() const
{
    std::stringstream ss;
    auto it = m_tagMap.begin();

    while (it != m_tagMap.end()) {
        ss << it->first;
        ++it;

        if (it != m_tagMap.end()) {
            ss << ", ";
        }
    }

    return ss.str();
}

//
// Sets the location of nodes
//
void ShirleyDomain::setNewX(const escript::Data& newX)
{
    if (*newX.getFunctionSpace().getDomain() != *this) {
        throw ValueError(
            "ShirleyDomain::setNewX(): illegal domain of new point locations");
    }

    if (newX.getFunctionSpace() == continuousFunction(*this)) {
        throw NotImplementedError(
            "ShirleyDomain::setNewX(): "
            "this member function is currently not implemented.");
    } else {
        throw ValueError(
            "ShirleyDomain::setNewX(): as of Escript version 3.3, setNewX() "
            "only accepts ContinuousFunction arguments. Please interpolate.");
    }
}

//
// Interpolates data between different function spaces
//
void ShirleyDomain::interpolateOnDomain(escript::Data& target,
                                        const escript::Data& source) const
{
    if (*source.getFunctionSpace().getDomain() != *this) {
        throw ValueError(
            "ShirleyDomain::interpolateOnDomain(): "
            "illegal domain of interpolation source");
    }

    if (*target.getFunctionSpace().getDomain() != *this) {
        throw ValueError(
            "ShirleyDomain::interpolateOnDomain(): "
            "illegal domain of interpolation target");
    }

    // Check target and source
    const int numComps = target.getDataPointSize();

    if (numComps != source.getDataPointSize()) {
        throw ValueError(
            "ShirleyDomain::interpolateOnDomain(): "
            "number of components of input and output Data do not match");
    }
    else if (!target.actsExpanded()) {
        throw ValueError(
            "ShirleyDomain::interpolateOnDomain(): "
            "expanded Data object is expected for output data");
    }
    else if (source.isComplex() != target.isComplex()) {
        throw ValueError(
            "ShirleyDomain::interpolateOnDomain(): "
            "complexity of input and output Data must match");
    }

    const int inFs = source.getFunctionSpace().getTypeCode();
    const int outFs = target.getFunctionSpace().getTypeCode();

    if (inFs == Nodes) {
        if (!source.numSamplesEqual(1, m_nodes->getNumNodes())) {
            throw ValueError(
                "ShirleyDomain::interpolateOnDomain(): "
                "illegal number of samples of input Data object");
        }
    }

    target.requireWrite();

    switch (inFs) {
        case Nodes:  // Interpolating from ContinuousFunction()...
            switch (outFs) {
                case Elements:  // ... to Function()
                    {
                    const int numTotalNodesPerElem = m_elements->getNumTotalNodesPerElem();
                    const real_t zero = 0.0;
                    const size_t numComps_size = numComps * sizeof(real_t);

//#pragma omp parallel for
                    for (int el = 0; el < m_elements->numElements; ++el) {
                        const int startOffset = m_elements->getVertexOffset(el, 0);
                        index_t nodeId, sourceArrIdx;

                        real_t* targetArr = target.getSampleDataRW(el, zero);
                        int targetArrIdx = 0;

                        for (int nn = 0; nn < numTotalNodesPerElem; ++nn) {
                            // FIXME Use borrowTargetNodes()?
                            //  See dudley::Assemble_interpolate()
                            nodeId = m_elements->Nodes[startOffset + nn];
                            sourceArrIdx = nodeId;

                            // Copy node coordinates to target
                            memcpy(&targetArr[targetArrIdx],
                                   source.getSampleDataRO(sourceArrIdx, zero),
                                   numComps_size);
                            targetArrIdx += numComps;
                        }
                    }

                    return;
                    }  // case Elements

                    break;

                case DegreesOfFreedom:  // ... to Solution()
                    {
                    // Copied from dudley::Assemble_CopyNodalData()
                    const dim_t numTarget = m_nodes->getNumDegreesOfFreedom();
                    const index_t* map = m_nodes->borrowDegreesOfFreedomTarget();
                    const real_t zero = 0.0;
                    const size_t numComps_size = numComps * sizeof(real_t);

//#pragma omp parallel for
                    for (index_t n = 0; n < numTarget; ++n) {
                        memcpy(target.getSampleDataRW(n, zero),
                               source.getSampleDataRO(map[n], zero),
                               numComps_size);
                    }

                    return;
                    }  // case DegreesOfFreedom

                    break;

                default:
                    break;
            }

            break;

        case DegreesOfFreedom:  // Interpolating from Solution()...
            switch (outFs) {
                case Elements:  // ... to Function()
                    {
                    // We must use borrowTargetDegreesOfFreedom()!
                    // See dudley::Assemble_interpolate()
                    const int numTotalNodesPerElem = m_elements->getNumTotalNodesPerElem();
                    const real_t zero = 0.0;
                    const size_t numComps_size = numComps * sizeof(real_t);
                    const index_t* targetDof = m_nodes->borrowTargetDegreesOfFreedom();

//#pragma omp parallel for
                    for (int el = 0; el < m_elements->numElements; ++el) {
                        const int startOffset = m_elements->getVertexOffset(el, 0);
                        index_t nodeId, sourceArrIdx;

                        real_t* targetArr = target.getSampleDataRW(el, zero);
                        int targetArrIdx = 0;

                        for (int nn = 0; nn < numTotalNodesPerElem; ++nn) {
                            nodeId = m_elements->Nodes[startOffset + nn];
                            index_t realNodeId = targetDof[nodeId];
                            sourceArrIdx = realNodeId;

                            // Copy node coordinates to target
                            memcpy(&targetArr[targetArrIdx],
                                   source.getSampleDataRO(sourceArrIdx, zero),
                                   numComps_size);
                            targetArrIdx += numComps;
                        }
                    }

                    return;
                    }  // case Elements

                    break;

                case Nodes:  // ... to ContinuousFunction()
                    {
                    // Copied from dudley::Assemble_CopyNodalData()
                    // (without the paso::Coupler part)
                    const index_t numOut = m_nodes->getNumNodes();
                    const index_t* targetDof = m_nodes->borrowTargetDegreesOfFreedom();
                    const real_t zero = 0.0;
                    const size_t numComps_size = numComps * sizeof(real_t);

//#pragma omp parallel for
                    for (index_t n = 0; n < numOut; n++) {
                        const index_t realNodeId = targetDof[n];
                        memcpy(target.getSampleDataRW(n, zero),
                               source.getSampleDataRO(realNodeId, zero),
                               numComps_size);
                    }
                    }

                    break;

                default:
                    break;
            }

        case Elements:  // Interpolating from Function()...
            switch (outFs) {
                case DegreesOfFreedom:  // ... to Solution()
                    {
                    interpolateFromFunctionToSolution(target, source);

                    return;
                    }

                    break;

                default:
                    break;
            }

        default:
            break;
    }

    std::stringstream ss;
    ss << "ShirleyDomain::interpolateOnDomain(): Shirley does not "
          "know how to interpolate from function space type "
       << functionSpaceTypeAsString(inFs) << " to type "
       << functionSpaceTypeAsString(outFs);
    throw ValueError(ss.str());
}

void ShirleyDomain::interpolateFromFunctionToSolution(
    escript::Data& target, const escript::Data& source) const
{
    const int numTotalNodesPerElem = m_elements->getNumTotalNodesPerElem();

    // FIXME Avoid using *readyPtr()
    const escript::DataReady_ptr &inReadyPtr = source.borrowReadyPtr();
    const DataTypes::RealVectorType &inVector = inReadyPtr->getVectorRO();

    const DataTypes::ShapeType& inShape = source.getDataPointShape();
    const DataTypes::ShapeType& outShape = target.getDataPointShape();

    const unsigned int inDpRank = source.getDataPointRank();
    const unsigned int outDpRank = target.getDataPointRank();

    // We must gather how many times each node ID is referenced,
    // and the sum of function values associated to it.

    // Key = node ID
    // Value = vector of pairs, where:
    //   Vector index = shape index
    //   Pair:
    //     Member 'first' = how many times the node ID is referenced
    //     Member 'second' = sum of function values associated to the node ID
    std::unordered_map<index_t, std::vector<std::pair<int, real_t>>>
        nodeIdQuantitySumFuncValMap;

    // ATTENTION: The map above is wasteful, we should probably
    //  go for a different way to keep track of shared nodes.
    //  It also can't be written to in simultaneous threads.

    for (int el = 0; el < m_elements->numElements; ++el) {
        const int startOffset = m_elements->getVertexOffset(el, 0);
        index_t nodeId;

        for (int nn = 0; nn < numTotalNodesPerElem; ++nn) {
            nodeId = m_elements->Nodes[startOffset + nn];
            const long offset = inReadyPtr->getPointOffset(el, nn);

            auto iter = nodeIdQuantitySumFuncValMap.find(nodeId);

            if (inDpRank == 0) {
                real_t funcVal = inVector[offset];

                if (iter == nodeIdQuantitySumFuncValMap.end()) {
                    // Create a new, single-element vector
                    nodeIdQuantitySumFuncValMap[nodeId] =
                        std::vector<std::pair<int, real_t>>(
                            1, std::make_pair(1, funcVal));
                }
                else {
                    // Update the current single-element
                    // vector
                    std::vector<std::pair<int, real_t>>
                        &curVector = iter->second;
                    curVector[0].first = curVector[0].first + 1;
                    curVector[0].second = curVector[0].second + funcVal;
                }
            }
            else if (inDpRank == 1) {
                std::vector<std::pair<int, real_t>> *newVector = nullptr;
                std::vector<std::pair<int, real_t>> *curVector = nullptr;

                if (iter == nodeIdQuantitySumFuncValMap.end()) {
                    // Create a new vector. It will be added
                    // to with as many elements as the first
                    // shape dimension
                    nodeIdQuantitySumFuncValMap[nodeId] =
                        std::vector<std::pair<int, real_t>>();
                    newVector = &nodeIdQuantitySumFuncValMap[nodeId];
                }
                else {
                    // Fetch the current vector
                    curVector = &iter->second;
                }

                for (int j = 0; j < inShape[0]; ++j) {
                    real_t funcVal = inVector[offset + j];

                    if (newVector != nullptr) {
                        // Insert a new element in the
                        // new vector
                        newVector->push_back(std::make_pair(1, funcVal));
                    }
                    else {
                        // Update an element in the
                        // current vector
                        (*curVector)[j].first = (*curVector)[j].first + 1;
                        (*curVector)[j].second = (*curVector)[j].second + funcVal;
                    }
                }
            }
            else {
                std::stringstream ss;
                ss << "ShirleyDomain::interpolateOnDomain(): "
                      "invalid data point rank for <source> = "
                   << inDpRank;
                throw ShirleyException(ss.str());
            }
        }
    }

    // Write the average of function values for each node ID
    // in the target instance
    const dim_t numTarget = m_nodes->getNumDegreesOfFreedom();
    const index_t* map = m_nodes->borrowDegreesOfFreedomTarget();

//#pragma omp parallel for
    for (index_t n = 0; n < numTarget; ++n) {
        real_t *outVector = target.getSampleDataRW(n, 0.0);

        const auto &iter = nodeIdQuantitySumFuncValMap.find(map[n]);
        std::vector<std::pair<int, real_t>> &curVector = iter->second;

        if (iter != nodeIdQuantitySumFuncValMap.end()) {
            if (outDpRank == 0) {
                std::pair<int, real_t> &curPair = curVector[0];
                outVector[0] = curPair.second / ((double) curPair.first);
            }
            else if (outDpRank == 1) {
                for (int j = 0; j < outShape[0]; ++j) {
                    std::pair<int, real_t> &curPair = curVector[j];
                    outVector[j] = curPair.second / ((double) curPair.first);
                }
            }
            else {
                std::stringstream ss;
                ss << "ShirleyDomain::interpolateOnDomain(): "
                      "invalid data point rank for <target> = "
                   << outDpRank;
                throw ShirleyException(ss.str());
            }
        }
    }
}

bool ShirleyDomain::probeInterpolationOnDomain(int functionSpaceType_source,
                                               int functionSpaceType_target)
    const
{
    switch (functionSpaceType_source) {
        case Nodes:  // From ContinuousFunction()...
            if ( // ... to Function()
                (functionSpaceType_target == Elements) ||
                 // ...  to Solution()
                (functionSpaceType_target == DegreesOfFreedom)) {
                return true;
            }

            break;

        case DegreesOfFreedom:  // From Solution()...
            if ( // ... to Function()
                (functionSpaceType_target == Elements) ||
                 // ... to ContinuousFunction()
                (functionSpaceType_target == Nodes)) {
                return true;
            }

            break;

        case Elements:  // From Function()...
            if ( // ... to Solution()
                functionSpaceType_target == DegreesOfFreedom) {
                return true;
            }

            break;
    }

    std::stringstream ss;
    ss << "ShirleyDomain::probeInterpolationOnDomain(): Shirley does "
          "not know how to interpolate from function space type "
       << functionSpaceTypeAsString(functionSpaceType_source)
       << " to type "
       << functionSpaceTypeAsString(functionSpaceType_target);
    throw ValueError(ss.str());
}

signed char ShirleyDomain::preferredInterpolationOnDomain(
    int functionSpaceType_source, int functionSpaceType_target) const
{
    if (probeInterpolationOnDomain(functionSpaceType_source,
                                   functionSpaceType_target)) {
        return +1;
    }

    if (probeInterpolationOnDomain(functionSpaceType_target,
                                   functionSpaceType_source)) {
        return -1;
    }

    return 0;
}

bool ShirleyDomain::commonFunctionSpace(const IntVector& fs,
                                        int& resultCode) const
{
   /*
    The idea is to use equivalence classes (i.e. types which can be
    interpolated back and forth):
    class 0: DOF <-> Nodes
    class 1: ReducedDOF <-> ReducedNodes
    class 2: Points
    class 3: Elements
    class 4: ReducedElements

    There is also a set of lines. Interpolation is possible down a line but not
    between lines.
    class 0 and 1 belong to all lines so aren't considered.
    line 0: class 2
    line 1: class 3,4

    For classes with multiple members (eg class 1) we have vars to record if
    there is at least one instance. e.g. hasNodes is true if we have at least
    one instance of Nodes.
    */

    if (fs.empty()) {
        return false;
    }

    std::vector<bool> hasClass(7, false);
    std::vector<int> hasLine(3, 0);
    bool hasNodes = false;
    bool hasRedNodes = false;

    for (size_t i=0; i<fs.size(); ++i) {
        switch (fs[i]) {
            case Nodes: hasNodes=true; // fall through
            case DegreesOfFreedom:
                hasClass[0]=true;
                break;
            case ReducedNodes: hasRedNodes=true; // fall through
            case ReducedDegreesOfFreedom:
                hasClass[1]=true;
                break;
            case Points:
                hasLine[0]=1;
                hasClass[2]=true;
                break;
            case Elements:
                hasClass[3]=true;
                hasLine[1]=1;
                break;
            case ReducedElements:
                hasClass[4]=true;
                hasLine[1]=1;
                break;
            default:
                return false;
        }
    }
    int numLines= hasLine[0] + hasLine[1];

    // fail if we have more than one leaf group
    // = there are at least two branches we can't interpolate between
    if (numLines > 1)
        return false;
    else if (numLines==1) {
        // we have points
        if (hasLine[0] == 1)
            resultCode=Points;
        else if (hasLine[1] == 1) {
            if (hasClass[4])
                resultCode=ReducedElements;
            else
                resultCode=Elements;
        }
    } else { // numLines==0
        if (hasClass[1])
            // something from class 1
            resultCode=(hasRedNodes ? ReducedNodes : ReducedDegreesOfFreedom);
        else // something from class 0
        resultCode=(hasNodes ? Nodes : DegreesOfFreedom);
    }
    return true;
}

//
// Interpolates data to other domain
//
void ShirleyDomain::interpolateAcross(escript::Data& /* target */,
                                      const escript::Data& /* source */) const
{
    throw NotImplementedError(
        "ShirleyDomain::interpolateAcross(): "
        "Shirley does not allow interpolation across domains.");
}

bool ShirleyDomain::probeInterpolationAcross(
    int functionSpaceType_source,
    const AbstractDomain& targetDomain,
    int functionSpaceType_target) const
{
    return false;
}

//
// return the normal vectors at the location of data points as a Data object
//
void ShirleyDomain::setToNormal(escript::Data& out) const
{
    if (*out.getFunctionSpace().getDomain() != *this) {
        throw ValueError(
            "ShirleyDomain::setToNormal(): illegal domain of normal locations");
    }

    throw NotImplementedError(
        "ShirleyDomain::setToNormal(): "
        "this member function is currently not implemented.");
}

//
// Returns the size of elements
//
void ShirleyDomain::setToSize(escript::Data& out) const
{
    switch (out.getFunctionSpace().getTypeCode()) {
        case Elements:
        case ReducedElements:
            Assemble_getSize(m_nodes, m_elements, out);
            break;

        default:
            std::stringstream ss;
            ss << "ShirleyDomain::setToSize(): Shirley does not know "
                  "anything about function space type "
               << functionSpaceTypeAsString(out.getFunctionSpace().getTypeCode());
            throw ValueError(ss.str());
    }
}

inline static
bool valuesAreClose(double x, double y, double tolerance)
{
    return fabs(x - y) <= tolerance;
}

//
// Calculates the gradient of arg
//
void ShirleyDomain::setToGradient(escript::Data& grad,
                                  const escript::Data& arg) const {
    if (*arg.getFunctionSpace().getDomain() != *this) {
        throw ValueError(
            "ShirleyDomain::setToGradient(): illegal domain of <arg>");
    }

    if (*grad.getFunctionSpace().getDomain() != *this) {
        throw ValueError(
            "ShirleyDomain::setToGradient(): illegal domain of <grad>");
    }

    if (grad.isComplex() != arg.isComplex()) {
        throw ValueError(
            "ShirleyDomain::setToGradient(): complexity of input and output must match");
    }

    int inFs = arg.getFunctionSpace().getTypeCode();
    int outFs = grad.getFunctionSpace().getTypeCode();

    switch (outFs) {
        case Elements:
            {
            switch (inFs) {
                case Elements:
                    calc2DGradientFromElements(grad, arg, m_elements);
                    return;

                case DegreesOfFreedom:
                    calc2DGradientFromDegreesOfFreedom(grad, arg, m_elements);
                    return;

                default:
                    {
                    std::stringstream ss;
                    ss << "ShirleyDomain::setToGradient(): Shirley does not "
                          "know how to compute gradients from "
                       << functionSpaceTypeAsString(inFs)
                       << " FunctionSpace to Elements FunctionSpace() ";
                    throw ValueError(ss.str());
                    }
            }
            }
            break;

        default:
            std::stringstream ss;
            ss << "ShirleyDomain::setToGradient(): Shirley does not know "
                  "anything about function space type "
               << functionSpaceTypeAsString(inFs);
            throw ValueError(ss.str());
    }
}

void ShirleyDomain::calc2DGradientFromElements(
    escript::Data& grad, const escript::Data& arg, ElementFile *elements) const
{
    if (elements->numDim != 2) {
        throw ValueError(
            "ShirleyDomain::calc2DGradientOnElements(): "
            "gradients on Data instances based on Elements "
            "can only be computed for 2-D domains");
    }

    grad.requireWrite();

    const int numTotalNodesPerElem = elements->getNumTotalNodesPerElem();
    const int polyDegree = elements->m_order;

    std::vector<double> stdXi(numTotalNodesPerElem);
    std::vector<double> stdEta(numTotalNodesPerElem);
    LobattoPolynomials::fillXiAndEtaArraysTri3(
        polyDegree, stdXi.data(), stdEta.data());

    const DataTypes::ShapeType& inShape = arg.getDataPointShape();
    const DataTypes::ShapeType& outShape = grad.getDataPointShape();

    const int numValues = DataTypes::noValues(inShape);
    const unsigned int inDpRank = arg.getDataPointRank();
    const unsigned int outDpRank = grad.getDataPointRank();

    // FIXME Avoid using *readyPtr()
    const escript::DataReady_ptr &inReadyPtr = arg.borrowReadyPtr();
    const DataTypes::RealVectorType &inVector = inReadyPtr->getVectorRO();

#pragma omp parallel for
    for (int el = 0; el < elements->numElements; ++el) {
        // Get the (x,y) coordinates of the vertex nodes for this element
        real_t x[3], y[3];

        index_t nodeId = elements->Nodes[elements->getVertexOffset(el, 0)];
        index_t arrayIdx = nodeId;
        x[0] = m_nodes->Coordinates[INDEX2(0, arrayIdx, m_nodes->numDim)];
        y[0] = m_nodes->Coordinates[INDEX2(1, arrayIdx, m_nodes->numDim)];

        nodeId = elements->Nodes[elements->getVertexOffset(el, 1)];
        arrayIdx = nodeId;
        x[1] = m_nodes->Coordinates[INDEX2(0, arrayIdx, m_nodes->numDim)];
        y[1] = m_nodes->Coordinates[INDEX2(1, arrayIdx, m_nodes->numDim)];

        nodeId = elements->Nodes[elements->getVertexOffset(el, 2)];
        arrayIdx = nodeId;
        x[2] = m_nodes->Coordinates[INDEX2(0, arrayIdx, m_nodes->numDim)];
        y[2] = m_nodes->Coordinates[INDEX2(1, arrayIdx, m_nodes->numDim)];

        // Compute (x,y) and (ξ,η) coordinates for all nodes of this element
        std::vector<double> nodeX(numTotalNodesPerElem);
        std::vector<double> nodeY(numTotalNodesPerElem);
        std::vector<double> nodeXi(numTotalNodesPerElem);
        std::vector<double> nodeEta(numTotalNodesPerElem);

        const double triangleArea = LobattoPolynomials::calcTriangleArea(
            x[0], x[1], x[2], y[0], y[1], y[2]);

        for (int nn = 0; nn < numTotalNodesPerElem; ++nn) {
            // Get the (x,y) coordinates of the current node
            nodeId = elements->Nodes[
                elements->getVertexOffset(el, 0) + nn];
            arrayIdx = nodeId;
            nodeX[nn] = m_nodes->Coordinates[INDEX2(0, arrayIdx, m_nodes->numDim)];
            nodeY[nn] = m_nodes->Coordinates[INDEX2(1, arrayIdx, m_nodes->numDim)];

            // Compute the (ξ,η) coordinates of the current node.
            // Partial derivatives ∂Ψ_i/∂ξ and ∂Ψ_i/∂η will be calculated over them.
            bool roundResultTowardZero = false;
            nodeXi[nn] = LobattoPolynomials::calcXiFromGeneralToStandardTriangle(
                nodeX[nn], nodeY[nn], x[0], x[2], y[0], y[2], triangleArea, roundResultTowardZero);
            nodeEta[nn] = LobattoPolynomials::calcEtaFromGeneralToStandardTriangle(
                nodeX[nn], nodeY[nn], x[0], x[1], y[0], y[1], triangleArea, roundResultTowardZero);
        }

        // Traverse all nodes (vertex & SEM nodes) of this element
        // computing the gradient at each of them.
        //
        // In matrix notation, the partial derivatives of cardinal node
        // interpolation functions Ψ_i with respect to ξ and η can be written
        // as:
        //
        //   [ ∂Ψ_i/∂ξ] = [ ∂x/∂ξ ∂y/∂ξ] . [ ∂Ψ_i/∂x]
        //   [ ∂Ψ_i/∂η]   [ ∂x/∂η ∂y/∂η]   [ ∂Ψ_i/∂y]
        //
        // If ∂Ψ_i/∂ξ and ∂Ψ_i/∂η are known, and if ∂x/∂ξ, ∂y/∂ξ, ∂x/∂η and
        // ∂y/∂η are known as well, then we can achieve our goal of finding
        // ∂Ψ_i/∂x and ∂Ψ_i/∂y (which is essential to the objective of this
        // function) by writing ("[...]^-1" means "the inverse matrix of
        // [...]"):
        //
        //   [ ∂Ψ_i/∂x ] = [ ∂x/∂ξ ∂y/∂ξ ]^-1 . [ ∂Ψ_i/∂ξ ]
        //   [ ∂Ψ_i/∂y ]   [ ∂x/∂η ∂y/∂η ]      [ ∂Ψ_i/∂η ]
        //
        // Our Ψ_i functions are simply a sum of Appell polynomials (χ_{kl})
        // multiplied by a set of known coefficients (b^i_{kl}), and the
        // expression of Appell polynomials makes them straightforward to
        // differentiate exactly (please see the implementation of
        // calcDerivativeOfAppellPolynomialWithRespectToXi() and
        // calcDerivativeOfAppellPolynomialWithRespectToEta() for reference).
        // Therefore, the values of ∂Ψ_i/∂ξ and ∂Ψ_i/∂η are taken care of.
        // They need to be computed for every node of every element.
        //
        // Now we proceed to compute ∂x/∂ξ, ∂y/∂ξ, ∂x/∂η and ∂y/∂η. Let us
        // define the N_1(ξ,η), N_2(ξ,η) and N_3(ξ,η) nodal shape functions
        // over the standard right triangle:
        //
        //   N_1(ξ,η) = 1 - ξ - η
        //   N_2(ξ,η) = ξ
        //   N_3(ξ,η) = η
        //
        // Given a pair of (ξ,η) coordinates over the standard right triangle,
        // these nodal shape functions allow us to compute the corresponding
        // x- and y-coordinates over a general triangle, the vertices of which
        // are given by (x_1, y_1), (x_2, y_2) and (x_3, y_3), through the
        // expressions below (please see "Quadrature Formulas in Two
        // Dimensions", by Shaozhong Deng of UNC at Charlotte, for reference):
        //
        //   x = P(ξ,η) = x_1*N_1(ξ,η) + x_2*N_2(ξ,η) + x_3*N_3(ξ,η)
        //   y = Q(ξ,η) = y_1*N_1(ξ,η) + y_2*N_2(ξ,η) + y_3*N_3(ξ,η)
        //
        // Now we can find ∂x/∂ξ, ∂y/∂ξ, ∂x/∂η and ∂y/∂η by differentiating
        // P(ξ,η) and Q(ξ,η) accordingly:
        //
        //   ∂x/∂ξ = x_2 - x_1     ∂y/∂ξ = y_2 - y_1
        //   ∂x/∂η = x_3 - x_1     ∂y/∂η = y_3 - y_1
        //
        // This makes the computation of [ ∂x/∂ξ ∂y/∂ξ ]^-1
        //                               [ ∂x/∂η ∂y/∂η ]
        // as simple as inverting a 2x2 matrix, which is done by computing
        // <a>, <b>, <c>, <d> and <determinant> below. These values need
        // to be calculated only once for each element, and can be reused
        // for every node belonging to that element.
        //
        // With that, we finally have all the values we need to compute
        // ∂Ψ_i/∂x and ∂Ψ_i/∂y at each element. By combining those values
        // with the value of the function of interest at each node
        // (supplied in <arg>), we are able to find the partial derivatives
        // of the function of interest at each node as well.

        const double a = x[1] - x[0];  // ∂x/∂ξ = x_2 - x_1
        const double b = y[1] - y[0];  // ∂y/∂ξ = y_2 - y_1
        const double c = x[2] - x[0];  // ∂x/∂η = x_3 - x_1
        const double d = y[2] - y[0];  // ∂y/∂η = y_3 - y_1
        const double determinant = (a * d) - (b * c);

        const double inv_a = (1.0 / determinant) *  d;
        const double inv_b = (1.0 / determinant) * -b;
        const double inv_c = (1.0 / determinant) * -c;
        const double inv_d = (1.0 / determinant) *  a;

        std::vector<double> sum_dpsi_dxi(numValues);
        std::vector<double> sum_dpsi_deta(numValues);
        std::vector<double> psiCoeffs(numTotalNodesPerElem);

        for (int q = 0; q < numTotalNodesPerElem; ++q) {
            std::fill(sum_dpsi_dxi.begin(), sum_dpsi_dxi.end(), 0.0);
            std::fill(sum_dpsi_deta.begin(), sum_dpsi_deta.end(), 0.0);

            // Compute partial derivatives ∂χ_{kl}/∂ξ and ∂χ_{kl}/∂η in
            // advance for all polynomial combinations of the current SEM
            // node of this element. The values computed here will still
            // need to be combined later with the proper coefficients
            // (see Eq. 3.2, pg. 12 of doi:10.1093/imamat/hxh077)
            std::vector<double> dchi_i_dxi_deta_table(
                2 * (polyDegree + 1) * (polyDegree + 1), 0.0);
            bool roundResultTowardZero = false;

            for (int k = 0; k <= polyDegree; ++k) {
                for (int l = 0; l <= polyDegree - k; ++l) {
                    LobattoPolynomials::
                    calcDerivativeOfAppellPolynomialWithRespectToXiAndEta(
                        k, l, nodeXi[q], nodeEta[q],
                        &dchi_i_dxi_deta_table[
                            (k * (polyDegree + 1) * 2) + (l * 2)],
                            roundResultTowardZero);
                }
            }

            // Traverse cardinal node interpolation functions Ψ_i over the
            // current node. The sum of Ψ_i over each specific node is
            // described at Eq. 1.5, pg. 2 of doi:10.1093/imamat/hxh077
            for (int p = 0; p < numTotalNodesPerElem; ++p) {
                // Get the coefficients for Ψ_i. These are the b^i_{kl}
                // values in Eq. 3.2, pg. 12 of doi:10.1093/imamat/hxh077
                VdmSystemsTri3::fillVdmCoefficients(
                    polyDegree, p + 1, psiCoeffs.data());
                int idx = 0;

                double dchi_i_dxi_deta[2] = { 0.0, 0.0 };
                double sum_dpsi_i_dxi = 0.0;
                double sum_dpsi_i_deta = 0.0;

                // The traversal of k and l is described at Eq. 3.2, pg. 12 of
                // doi:10.1093/imamat/hxh077
                for (int k = 0; k <= polyDegree; ++k) {
                    for (int l = 0; l <= polyDegree - k; ++l) {
                        // Compute ∂Ψ_i/∂ξ (ie. the sum of
                        // b^i_{kl} * ∂χ_{kl}/∂ξ) and ∂Ψ_i/∂η (ie.
                        // the sum of b^i_{kl} * ∂χ_{kl}/∂η).
                        //LobattoPolynomials::
                        //calcDerivativeOfAppellPolynomialWithRespectToXiAndEta(
                        //    k, l, nodeXi[q], nodeEta[q], dchi_i_dxi_deta, false);
                        dchi_i_dxi_deta[0] = dchi_i_dxi_deta_table[
                            (k * (polyDegree + 1) * 2) + (l * 2) + 0];
                        dchi_i_dxi_deta[1] = dchi_i_dxi_deta_table[
                            (k * (polyDegree + 1) * 2) + (l * 2) + 1];

                        // Accumulates partial results
                        sum_dpsi_i_dxi += (psiCoeffs[idx] * dchi_i_dxi_deta[0]);
                        sum_dpsi_i_deta += (psiCoeffs[idx] * dchi_i_dxi_deta[1]);

                        ++idx;
                    }
                }

                // funcVal is the f_i term in Eq. 1.5, pg. 2 of
                // doi:10.1093/imamat/hxh077. It is the value of the
                // function of interest, the gradient of which must be
                // computed; the values for this function are supplied
                // in <arg>
                int stdXiEtaIndex = -1;

                for (int j = 0; j < numTotalNodesPerElem; ++j) {
                    if (valuesAreClose(nodeXi[j], stdXi[p], 1E-6) &&
                            valuesAreClose(nodeEta[j], stdEta[p], 1E-6)) {
                        stdXiEtaIndex = j;
                        break;
                    }
                }

                if (stdXiEtaIndex == -1) {
                    std::stringstream ss;
                    ss << "ShirleyDomain::calc2DGradientOnElements(): "
                          "couldn't find index for standard xi/eta => "
                       << "element = " << el << ", q = " << q
                       << ", p = " << p;
                    throw ShirleyException(ss.str());
                }

                const long offset = inReadyPtr->getPointOffset(el, stdXiEtaIndex);

                if (inDpRank == 0) {
                    real_t funcVal = inVector[offset];
                    sum_dpsi_dxi[0] += sum_dpsi_i_dxi * funcVal;
                    sum_dpsi_deta[0] += sum_dpsi_i_deta * funcVal;
                }
                else if (inDpRank == 1) {
                    for (int j = 0; j < inShape[0]; ++j) {
                        real_t funcVal = inVector[offset + j];
                        sum_dpsi_dxi[j] += sum_dpsi_i_dxi * funcVal;
                        sum_dpsi_deta[j] += sum_dpsi_i_deta * funcVal;
                    }
                }
                else {
                    std::stringstream ss;
                    ss << "ShirleyDomain::calc2DGradientOnElements(): "
                          "invalid data point rank for <arg> = "
                       << inDpRank;
                    throw ShirleyException(ss.str());
                }
            }  // for (int p = 0; ...

            real_t *outVector = grad.getSampleDataRW(0, 0.0);
            const long offset = grad.getDataOffset(el, q);

            if (outDpRank == 1) {
                double df_dx = (inv_a * sum_dpsi_dxi[0]) + (inv_b * sum_dpsi_deta[0]);
                double df_dy = (inv_c * sum_dpsi_dxi[0]) + (inv_d * sum_dpsi_deta[0]);

                if (valuesAreClose(df_dx, 0.0, 1E-14)) {
                    df_dx = 0.0;
                }

                if (valuesAreClose(df_dy, 0.0, 1E-14)) {
                    df_dy = 0.0;
                }

                outVector[offset + 0] = df_dx;
                outVector[offset + 1] = df_dy;
            }
            else if (outDpRank == 2) {
                for (int j = 0; j < outShape[0]; ++j) {
                    double df_dx = (inv_a * sum_dpsi_dxi[j]) + (inv_b * sum_dpsi_deta[j]);
                    double df_dy = (inv_c * sum_dpsi_dxi[j]) + (inv_d * sum_dpsi_deta[j]);

                    if (valuesAreClose(df_dx, 0.0, 1E-14)) {
                        df_dx = 0.0;
                    }

                    if (valuesAreClose(df_dy, 0.0, 1E-14)) {
                        df_dy = 0.0;
                    }

                    outVector[offset + DataTypes::getRelIndex(outShape, j, 0)] = df_dx;
                    outVector[offset + DataTypes::getRelIndex(outShape, j, 1)] = df_dy;
                }
            }
            else {
                std::stringstream ss;
                ss << "ShirleyDomain::calc2DGradientOnElements(): "
                      "invalid data point rank for <grad> = "
                   << outDpRank;
                throw ShirleyException(ss.str());
            }
        }  // for (int q = 0; ...
    }  // for (int el = 0; ...
}

void ShirleyDomain::calc2DGradientFromDegreesOfFreedom(
    escript::Data& grad, const escript::Data& arg, ElementFile *elements) const
{
    if (elements->numDim != 2) {
        throw ValueError(
            "ShirleyDomain::calc2DGradientOnDegreesOfFreedom(): "
            "gradients on Data instances based on Elements "
            "can only be computed for 2-D domains");
    }

    grad.requireWrite();

    const int numTotalNodesPerElem = elements->getNumTotalNodesPerElem();
    const int polyDegree = elements->m_order;

    std::vector<double> stdXi(numTotalNodesPerElem);
    std::vector<double> stdEta(numTotalNodesPerElem);
    LobattoPolynomials::fillXiAndEtaArraysTri3(
        polyDegree, stdXi.data(), stdEta.data());

    const DataTypes::ShapeType& inShape = arg.getDataPointShape();
    const DataTypes::ShapeType& outShape = grad.getDataPointShape();

    const int numValues = DataTypes::noValues(inShape);
    const unsigned int inDpRank = arg.getDataPointRank();
    const unsigned int outDpRank = grad.getDataPointRank();

    // FIXME Avoid using *readyPtr()
    const escript::DataReady_ptr &inReadyPtr = arg.borrowReadyPtr();
    const DataTypes::RealVectorType &inVector = inReadyPtr->getVectorRO();

    const index_t* targetDof = m_nodes->borrowTargetDegreesOfFreedom();

#pragma omp parallel for
    for (int el = 0; el < elements->numElements; ++el) {
        // Get the (x,y) coordinates of the vertex nodes for this element
        real_t x[3], y[3];

        index_t nodeId = elements->Nodes[elements->getVertexOffset(el, 0)];
        index_t arrayIdx = nodeId;
        x[0] = m_nodes->Coordinates[INDEX2(0, arrayIdx, m_nodes->numDim)];
        y[0] = m_nodes->Coordinates[INDEX2(1, arrayIdx, m_nodes->numDim)];

        nodeId = elements->Nodes[elements->getVertexOffset(el, 1)];
        arrayIdx = nodeId;
        x[1] = m_nodes->Coordinates[INDEX2(0, arrayIdx, m_nodes->numDim)];
        y[1] = m_nodes->Coordinates[INDEX2(1, arrayIdx, m_nodes->numDim)];

        nodeId = elements->Nodes[elements->getVertexOffset(el, 2)];
        arrayIdx = nodeId;
        x[2] = m_nodes->Coordinates[INDEX2(0, arrayIdx, m_nodes->numDim)];
        y[2] = m_nodes->Coordinates[INDEX2(1, arrayIdx, m_nodes->numDim)];

        // Compute (x,y) and (ξ,η) coordinates for all nodes of this element
        std::vector<double> nodeX(numTotalNodesPerElem);
        std::vector<double> nodeY(numTotalNodesPerElem);
        std::vector<double> nodeXi(numTotalNodesPerElem);
        std::vector<double> nodeEta(numTotalNodesPerElem);

        const double triangleArea = LobattoPolynomials::calcTriangleArea(
            x[0], x[1], x[2], y[0], y[1], y[2]);

        for (int nn = 0; nn < numTotalNodesPerElem; ++nn) {
            // Get the (x,y) coordinates of the current node
            nodeId = elements->Nodes[
                elements->getVertexOffset(el, 0) + nn];
            arrayIdx = nodeId;
            nodeX[nn] = m_nodes->Coordinates[INDEX2(0, arrayIdx, m_nodes->numDim)];
            nodeY[nn] = m_nodes->Coordinates[INDEX2(1, arrayIdx, m_nodes->numDim)];

            // Compute the (ξ,η) coordinates of the current node.
            // Partial derivatives ∂Ψ_i/∂ξ and ∂Ψ_i/∂η will be calculated over them.
            bool roundResultTowardZero = false;
            nodeXi[nn] = LobattoPolynomials::calcXiFromGeneralToStandardTriangle(
                nodeX[nn], nodeY[nn], x[0], x[2], y[0], y[2], triangleArea, roundResultTowardZero);
            nodeEta[nn] = LobattoPolynomials::calcEtaFromGeneralToStandardTriangle(
                nodeX[nn], nodeY[nn], x[0], x[1], y[0], y[1], triangleArea, roundResultTowardZero);
        }

        // Traverse all nodes (vertex & SEM nodes) of this element
        // computing the gradient at each of them.
        //
        // In matrix notation, the partial derivatives of cardinal node
        // interpolation functions Ψ_i with respect to ξ and η can be written
        // as:
        //
        //   [ ∂Ψ_i/∂ξ] = [ ∂x/∂ξ ∂y/∂ξ] . [ ∂Ψ_i/∂x]
        //   [ ∂Ψ_i/∂η]   [ ∂x/∂η ∂y/∂η]   [ ∂Ψ_i/∂y]
        //
        // If ∂Ψ_i/∂ξ and ∂Ψ_i/∂η are known, and if ∂x/∂ξ, ∂y/∂ξ, ∂x/∂η and
        // ∂y/∂η are known as well, then we can achieve our goal of finding
        // ∂Ψ_i/∂x and ∂Ψ_i/∂y (which is essential to the objective of this
        // function) by writing ("[...]^-1" means "the inverse matrix of
        // [...]"):
        //
        //   [ ∂Ψ_i/∂x ] = [ ∂x/∂ξ ∂y/∂ξ ]^-1 . [ ∂Ψ_i/∂ξ ]
        //   [ ∂Ψ_i/∂y ]   [ ∂x/∂η ∂y/∂η ]      [ ∂Ψ_i/∂η ]
        //
        // Our Ψ_i functions are simply a sum of Appell polynomials (χ_{kl})
        // multiplied by a set of known coefficients (b^i_{kl}), and the
        // expression of Appell polynomials makes them straightforward to
        // differentiate exactly (please see the implementation of
        // calcDerivativeOfAppellPolynomialWithRespectToXi() and
        // calcDerivativeOfAppellPolynomialWithRespectToEta() for reference).
        // Therefore, the values of ∂Ψ_i/∂ξ and ∂Ψ_i/∂η are taken care of.
        // They need to be computed for every node of every element.
        //
        // Now we proceed to compute ∂x/∂ξ, ∂y/∂ξ, ∂x/∂η and ∂y/∂η. Let us
        // define the N_1(ξ,η), N_2(ξ,η) and N_3(ξ,η) nodal shape functions
        // over the standard right triangle:
        //
        //   N_1(ξ,η) = 1 - ξ - η
        //   N_2(ξ,η) = ξ
        //   N_3(ξ,η) = η
        //
        // Given a pair of (ξ,η) coordinates over the standard right triangle,
        // these nodal shape functions allow us to compute the corresponding
        // x- and y-coordinates over a general triangle, the vertices of which
        // are given by (x_1, y_1), (x_2, y_2) and (x_3, y_3), through the
        // expressions below (please see "Quadrature Formulas in Two
        // Dimensions", by Shaozhong Deng of UNC at Charlotte, for reference):
        //
        //   x = P(ξ,η) = x_1*N_1(ξ,η) + x_2*N_2(ξ,η) + x_3*N_3(ξ,η)
        //   y = Q(ξ,η) = y_1*N_1(ξ,η) + y_2*N_2(ξ,η) + y_3*N_3(ξ,η)
        //
        // Now we can find ∂x/∂ξ, ∂y/∂ξ, ∂x/∂η and ∂y/∂η by differentiating
        // P(ξ,η) and Q(ξ,η) accordingly:
        //
        //   ∂x/∂ξ = x_2 - x_1     ∂y/∂ξ = y_2 - y_1
        //   ∂x/∂η = x_3 - x_1     ∂y/∂η = y_3 - y_1
        //
        // This makes the computation of [ ∂x/∂ξ ∂y/∂ξ ]^-1
        //                               [ ∂x/∂η ∂y/∂η ]
        // as simple as inverting a 2x2 matrix, which is done by computing
        // <a>, <b>, <c>, <d> and <determinant> below. These values need
        // to be calculated only once for each element, and can be reused
        // for every node belonging to that element.
        //
        // With that, we finally have all the values we need to compute
        // ∂Ψ_i/∂x and ∂Ψ_i/∂y at each element. By combining those values
        // with the value of the function of interest at each node
        // (supplied in <arg>), we are able to find the partial derivatives
        // of the function of interest at each node as well.

        const double a = x[1] - x[0];  // ∂x/∂ξ = x_2 - x_1
        const double b = y[1] - y[0];  // ∂y/∂ξ = y_2 - y_1
        const double c = x[2] - x[0];  // ∂x/∂η = x_3 - x_1
        const double d = y[2] - y[0];  // ∂y/∂η = y_3 - y_1
        const double determinant = (a * d) - (b * c);

        const double inv_a = (1.0 / determinant) *  d;
        const double inv_b = (1.0 / determinant) * -b;
        const double inv_c = (1.0 / determinant) * -c;
        const double inv_d = (1.0 / determinant) *  a;

        std::vector<double> sum_dpsi_dxi(numValues);
        std::vector<double> sum_dpsi_deta(numValues);
        std::vector<double> psiCoeffs(numTotalNodesPerElem);

        for (int q = 0; q < numTotalNodesPerElem; ++q) {
            std::fill(sum_dpsi_dxi.begin(), sum_dpsi_dxi.end(), 0.0);
            std::fill(sum_dpsi_deta.begin(), sum_dpsi_deta.end(), 0.0);

            // Compute partial derivatives ∂χ_{kl}/∂ξ and ∂χ_{kl}/∂η in
            // advance for all polynomial combinations of the current SEM
            // node of this element. The values computed here will still
            // need to be combined later with the proper coefficients
            // (see Eq. 3.2, pg. 12 of doi:10.1093/imamat/hxh077)
            std::vector<double> dchi_i_dxi_deta_table(
                2 * (polyDegree + 1) * (polyDegree + 1), 0.0);
            bool roundResultTowardZero = false;

            for (int k = 0; k <= polyDegree; ++k) {
                for (int l = 0; l <= polyDegree - k; ++l) {
                    LobattoPolynomials::
                    calcDerivativeOfAppellPolynomialWithRespectToXiAndEta(
                        k, l, nodeXi[q], nodeEta[q],
                        &dchi_i_dxi_deta_table[
                            (k * (polyDegree + 1) * 2) + (l * 2)],
                            roundResultTowardZero);
                }
            }

            // Traverse cardinal node interpolation functions Ψ_i over the
            // current node. The sum of Ψ_i over each specific node is
            // described at Eq. 1.5, pg. 2 of doi:10.1093/imamat/hxh077
            for (int p = 0; p < numTotalNodesPerElem; ++p) {
                // Get the coefficients for Ψ_i. These are the b^i_{kl}
                // values in Eq. 3.2, pg. 12 of doi:10.1093/imamat/hxh077
                VdmSystemsTri3::fillVdmCoefficients(
                    polyDegree, p + 1, psiCoeffs.data());
                int idx = 0;

                double dchi_i_dxi_deta[2] = { 0.0, 0.0 };
                double sum_dpsi_i_dxi = 0.0;
                double sum_dpsi_i_deta = 0.0;

                // The traversal of k and l is described at Eq. 3.2, pg. 12 of
                // doi:10.1093/imamat/hxh077
                for (int k = 0; k <= polyDegree; ++k) {
                    for (int l = 0; l <= polyDegree - k; ++l) {
                        // Compute ∂Ψ_i/∂ξ (ie. the sum of
                        // b^i_{kl} * ∂χ_{kl}/∂ξ) and ∂Ψ_i/∂η (ie.
                        // the sum of b^i_{kl} * ∂χ_{kl}/∂η).
                        //LobattoPolynomials::
                        //calcDerivativeOfAppellPolynomialWithRespectToXiAndEta(
                        //    k, l, nodeXi[q], nodeEta[q], dchi_i_dxi_deta, false);
                        dchi_i_dxi_deta[0] = dchi_i_dxi_deta_table[
                            (k * (polyDegree + 1) * 2) + (l * 2) + 0];
                        dchi_i_dxi_deta[1] = dchi_i_dxi_deta_table[
                            (k * (polyDegree + 1) * 2) + (l * 2) + 1];

                        // Accumulates partial results
                        sum_dpsi_i_dxi += (psiCoeffs[idx] * dchi_i_dxi_deta[0]);
                        sum_dpsi_i_deta += (psiCoeffs[idx] * dchi_i_dxi_deta[1]);

                        ++idx;
                    }
                }

                // funcVal is the f_i term in Eq. 1.5, pg. 2 of
                // doi:10.1093/imamat/hxh077. It is the value of the
                // function of interest, the gradient of which must be
                // computed; the values for this function are supplied
                // in <arg>
                int stdXiEtaIndex = -1;

                for (int j = 0; j < numTotalNodesPerElem; ++j) {
                    if (valuesAreClose(nodeXi[j], stdXi[p], 1E-6) &&
                            valuesAreClose(nodeEta[j], stdEta[p], 1E-6)) {
                        stdXiEtaIndex = j;
                        break;
                    }
                }

                if (stdXiEtaIndex == -1) {
                    std::stringstream ss;
                    ss << "ShirleyDomain::calc2DGradientOnDegreesOfFreedom(): "
                          "couldn't find index for standard xi/eta => "
                       << "element = " << el << ", q = " << q
                       << ", p = " << p;
                    throw ShirleyException(ss.str());
                }

                nodeId = elements->Nodes[
                    elements->getVertexOffset(el, 0) + stdXiEtaIndex];
                index_t realNodeId = targetDof[nodeId];

                const long offset = inReadyPtr->getPointOffset(realNodeId, 0);

                if (inDpRank == 0) {
                    real_t funcVal = inVector[offset];
                    sum_dpsi_dxi[0] += sum_dpsi_i_dxi * funcVal;
                    sum_dpsi_deta[0] += sum_dpsi_i_deta * funcVal;
                }
                else if (inDpRank == 1) {
                    for (int j = 0; j < inShape[0]; ++j) {
                        real_t funcVal = inVector[offset + j];
                        sum_dpsi_dxi[j] += sum_dpsi_i_dxi * funcVal;
                        sum_dpsi_deta[j] += sum_dpsi_i_deta * funcVal;
                    }
                }
                else {
                    std::stringstream ss;
                    ss << "ShirleyDomain::calc2DGradientOnDegreesOfFreedom(): "
                          "invalid data point rank for <arg> = "
                       << inDpRank;
                    throw ShirleyException(ss.str());
                }
            }  // for (int i = 0; ...

            real_t *outVector = grad.getSampleDataRW(0, 0.0);
            const long offset = grad.getDataOffset(el, q);

            if (outDpRank == 1) {
                double df_dx = (inv_a * sum_dpsi_dxi[0]) + (inv_b * sum_dpsi_deta[0]);
                double df_dy = (inv_c * sum_dpsi_dxi[0]) + (inv_d * sum_dpsi_deta[0]);

                if (valuesAreClose(df_dx, 0.0, 1E-14)) {
                    df_dx = 0.0;
                }

                if (valuesAreClose(df_dy, 0.0, 1E-14)) {
                    df_dy = 0.0;
                }

                outVector[offset + 0] = df_dx;
                outVector[offset + 1] = df_dy;
            }
            else if (outDpRank == 2) {
                for (int j = 0; j < outShape[0]; ++j) {
                    double df_dx = (inv_a * sum_dpsi_dxi[j]) + (inv_b * sum_dpsi_deta[j]);
                    double df_dy = (inv_c * sum_dpsi_dxi[j]) + (inv_d * sum_dpsi_deta[j]);

                    if (valuesAreClose(df_dx, 0.0, 1E-14)) {
                        df_dx = 0.0;
                    }

                    if (valuesAreClose(df_dy, 0.0, 1E-14)) {
                        df_dy = 0.0;
                    }

                    outVector[offset + DataTypes::getRelIndex(outShape, j, 0)] = df_dx;
                    outVector[offset + DataTypes::getRelIndex(outShape, j, 1)] = df_dy;
                }
            }
            else {
                std::stringstream ss;
                ss << "ShirleyDomain::calc2DGradientOnDegreesOfFreedom(): "
                      "invalid data point rank for <grad> = "
                   << outDpRank;
                throw ShirleyException(ss.str());
            }
        }  // for (int nn = 0; ...
    }  // for (int el = 0; ...
}

void ShirleyDomain::fillJacobians()
{
    // For each element, compute the Jacobian matrix, its determinant
    // and its inverse. The result will be stored in a vector.
    ElementFile *elements = m_elements;

    m_elemIndexJacobian.clear();

    for (int el = 0; el < elements->numElements; ++el) {
        // Get the (x,y) coordinates of the vertex nodes for this element
        real_t x[3], y[3];

        index_t nodeId = elements->Nodes[elements->getVertexOffset(el, 0)];
        index_t arrayIdx = nodeId;
        x[0] = m_nodes->Coordinates[INDEX2(0, arrayIdx, m_nodes->numDim)];
        y[0] = m_nodes->Coordinates[INDEX2(1, arrayIdx, m_nodes->numDim)];

        nodeId = elements->Nodes[elements->getVertexOffset(el, 1)];
        arrayIdx = nodeId;
        x[1] = m_nodes->Coordinates[INDEX2(0, arrayIdx, m_nodes->numDim)];
        y[1] = m_nodes->Coordinates[INDEX2(1, arrayIdx, m_nodes->numDim)];

        nodeId = elements->Nodes[elements->getVertexOffset(el, 2)];
        arrayIdx = nodeId;
        x[2] = m_nodes->Coordinates[INDEX2(0, arrayIdx, m_nodes->numDim)];
        y[2] = m_nodes->Coordinates[INDEX2(1, arrayIdx, m_nodes->numDim)];

        // Calculate and store the Jacobian matrix for this element
        m_elemIndexJacobian.emplace_back(x[0], y[0], x[1], y[1], x[2], y[2]);
    }
}

void ShirleyDomain::fillCanonicalNodeDerivatives()
{
    const int numTotalNodesPerElem = m_elements->getNumTotalNodesPerElem();
    const int polyDegree = m_elements->m_order;

    std::vector<double> stdXi(numTotalNodesPerElem);
    std::vector<double> stdEta(numTotalNodesPerElem);
    LobattoPolynomials::fillXiAndEtaArraysTri3(
        polyDegree, stdXi.data(), stdEta.data());

    m_canonNodeDerivatives = std::vector<NodeDerivatives2d>(numTotalNodesPerElem);

    // Traverse canonical order of (ξ,η) coordinates
    for (int p = 0; p < numTotalNodesPerElem; ++p) {
        double xi = stdXi[p];
        double eta = stdEta[p];

        // Compute partial derivatives ∂χ_{kl}/∂ξ and ∂χ_{kl}/∂η in
        // advance for all polynomial combinations of the current (ξ,η)
        // coordinates. The values computed here will still need
        // to be combined later with the proper coefficients
        // (see Eq. 3.2, pg. 12 of doi:10.1093/imamat/hxh077)
        std::vector<double> dchi_i_dxi_deta_table(
            2 * (polyDegree + 1) * (polyDegree + 1), 0.0);
        bool roundResultTowardZero = false;

        for (int k = 0; k <= polyDegree; ++k) {
            for (int l = 0; l <= polyDegree - k; ++l) {
                LobattoPolynomials::
                calcDerivativeOfAppellPolynomialWithRespectToXiAndEta(
                    k, l, xi, eta,
                    &dchi_i_dxi_deta_table[
                        (k * (polyDegree + 1) * 2) + (l * 2)],
                        roundResultTowardZero);
            }
        }

        // Traverse cardinal node interpolation functions Ψ_i over the
        // current (ξ,η) coordinates. The sum of Ψ_i over each specific
        // node is described at Eq. 1.5, pg. 2 of doi:10.1093/imamat/hxh077.
        std::vector<double> psiCoeffs(numTotalNodesPerElem);

        NodeDerivatives2d &nodeDerivs = m_canonNodeDerivatives[p];
        nodeDerivs.m_dpsi_dxi.resize(numTotalNodesPerElem);
        nodeDerivs.m_dpsi_deta.resize(numTotalNodesPerElem);

        for (int q = 0; q < numTotalNodesPerElem; ++q) {
            // Get the coefficients for Ψ_i. These are the b^i_{kl}
            // values in Eq. 3.2, pg. 12 of doi:10.1093/imamat/hxh077
            VdmSystemsTri3::fillVdmCoefficients(
                polyDegree, q + 1, psiCoeffs.data());
            int idx = 0;

            double dchi_i_dxi_deta[2] = { 0.0, 0.0 };
            double sum_dpsi_i_dxi = 0.0;
            double sum_dpsi_i_deta = 0.0;

            // The traversal of k and l is described at Eq. 3.2, pg. 12 of
            // doi:10.1093/imamat/hxh077
            for (int k = 0; k <= polyDegree; ++k) {
                for (int l = 0; l <= polyDegree - k; ++l) {
                    // Compute ∂Ψ_i/∂ξ (i.e. the sum of
                    // b^i_{kl} * ∂χ_{kl}/∂ξ) and ∂Ψ_i/∂η (i.e.
                    // the sum of b^i_{kl} * ∂χ_{kl}/∂η).
                    //LobattoPolynomials::
                    //calcDerivativeOfAppellPolynomialWithRespectToXiAndEta(
                    //    k, l, xi, eta, dchi_i_dxi_deta, false);
                    dchi_i_dxi_deta[0] = dchi_i_dxi_deta_table[
                        (k * (polyDegree + 1) * 2) + (l * 2) + 0];
                    dchi_i_dxi_deta[1] = dchi_i_dxi_deta_table[
                        (k * (polyDegree + 1) * 2) + (l * 2) + 1];

                    // Accumulates partial results
                    sum_dpsi_i_dxi += (psiCoeffs[idx] * dchi_i_dxi_deta[0]);
                    sum_dpsi_i_deta += (psiCoeffs[idx] * dchi_i_dxi_deta[1]);

                    ++idx;
                }
            }

            nodeDerivs.m_dpsi_dxi[q] = sum_dpsi_i_dxi;
            nodeDerivs.m_dpsi_deta[q] = sum_dpsi_i_deta;
        }  // for (int q = ...
    }  // for (int p = ...
}

inline
static double calcEdgeLength(double xDist, double yDist)
{
    return std::sqrt((xDist * xDist) + (yDist * yDist));
}

void ShirleyDomain::fillXiEtaCoordinates()
{
    // For each element, compute the (ξ,η) coordinates corresponding
    // to the (x,y) coordinates of each SEM node. The result will be
    // stored in a vector.
    ElementFile *elements = m_elements;
    const int numTotalNodesPerElem = elements->getNumTotalNodesPerElem();

    m_elemIndexXiEtaCoords.clear();

    // DEBUG
    double minArea = std::numeric_limits<double>::max();
    double maxArea = std::numeric_limits<double>::lowest();
    double totalArea = 0.0;
    std::vector<double> elemAreas;

    double minEdgeLen = std::numeric_limits<double>::max();
    double maxEdgeLen = std::numeric_limits<double>::lowest();
    double totalEdgeLen = 0.0;

    double mean_minEdgeLen = std::numeric_limits<double>::max();
    double mean_maxEdgeLen = std::numeric_limits<double>::lowest();
    double mean_totalEdgeLen = 0.0;
    std::vector<double> mean_elemEdgeLengths;

    double max_minEdgeLen = std::numeric_limits<double>::max();
    double max_maxEdgeLen = std::numeric_limits<double>::lowest();
    double max_totalEdgeLen = 0.0;
    std::vector<double> max_elemEdgeLengths;
    // DEBUG

    for (int el = 0; el < elements->numElements; ++el) {
        // Get the (x,y) coordinates of the vertex nodes for this element
        real_t x[3], y[3];

        index_t nodeId = elements->Nodes[elements->getVertexOffset(el, 0)];
        index_t arrayIdx = nodeId;
        x[0] = m_nodes->Coordinates[INDEX2(0, arrayIdx, m_nodes->numDim)];
        y[0] = m_nodes->Coordinates[INDEX2(1, arrayIdx, m_nodes->numDim)];

        nodeId = elements->Nodes[elements->getVertexOffset(el, 1)];
        arrayIdx = nodeId;
        x[1] = m_nodes->Coordinates[INDEX2(0, arrayIdx, m_nodes->numDim)];
        y[1] = m_nodes->Coordinates[INDEX2(1, arrayIdx, m_nodes->numDim)];

        nodeId = elements->Nodes[elements->getVertexOffset(el, 2)];
        arrayIdx = nodeId;
        x[2] = m_nodes->Coordinates[INDEX2(0, arrayIdx, m_nodes->numDim)];
        y[2] = m_nodes->Coordinates[INDEX2(1, arrayIdx, m_nodes->numDim)];

        // Compute (ξ,η) coordinates for all nodes of this element
        const double triangleArea = LobattoPolynomials::calcTriangleArea(
            x[0], x[1], x[2], y[0], y[1], y[2]);

        std::vector<real_t> xiEtaCoords(numTotalNodesPerElem * 2);

        for (int p = 0; p < numTotalNodesPerElem; ++p) {
            // Get the (x,y) coordinates of the current node
            nodeId = elements->Nodes[elements->getVertexOffset(el, 0) + p];
            arrayIdx = nodeId;
            double nodeX = m_nodes->Coordinates[INDEX2(0, arrayIdx, m_nodes->numDim)];
            double nodeY = m_nodes->Coordinates[INDEX2(1, arrayIdx, m_nodes->numDim)];

            // Compute and store the (ξ,η) coordinates of the current node
            bool roundResultTowardZero = false;
            double nodeXi = LobattoPolynomials::calcXiFromGeneralToStandardTriangle(
                nodeX, nodeY, x[0], x[2], y[0], y[2], triangleArea, roundResultTowardZero);
            double nodeEta = LobattoPolynomials::calcEtaFromGeneralToStandardTriangle(
                nodeX, nodeY, x[0], x[1], y[0], y[1], triangleArea, roundResultTowardZero);

            xiEtaCoords[(p * 2) + 0] = nodeXi;
            xiEtaCoords[(p * 2) + 1] = nodeEta;
        }

        m_elemIndexXiEtaCoords.push_back(xiEtaCoords);

        // DEBUG
        minArea = std::min(triangleArea, minArea);
        maxArea = std::max(triangleArea, maxArea);
        totalArea += triangleArea;
        elemAreas.push_back(triangleArea);

        double edgeLen_1 = calcEdgeLength(x[0] - x[1], y[0] - y[1]);
        minEdgeLen = std::min(edgeLen_1, minEdgeLen);
        maxEdgeLen = std::max(edgeLen_1, maxEdgeLen);
        totalEdgeLen += edgeLen_1;
        double edgeLen_2 = calcEdgeLength(x[0] - x[2], y[0] - y[2]);
        minEdgeLen = std::min(edgeLen_2, minEdgeLen);
        maxEdgeLen = std::max(edgeLen_2, maxEdgeLen);
        totalEdgeLen += edgeLen_2;
        double edgeLen_3 = calcEdgeLength(x[1] - x[2], y[1] - y[2]);
        minEdgeLen = std::min(edgeLen_3, minEdgeLen);
        maxEdgeLen = std::max(edgeLen_3, maxEdgeLen);
        totalEdgeLen += edgeLen_3;

        double cur_meanEdgeLen = (edgeLen_1 + edgeLen_2 + edgeLen_3) / 3.0;
        mean_minEdgeLen = std::min(cur_meanEdgeLen, mean_minEdgeLen);
        mean_maxEdgeLen = std::max(cur_meanEdgeLen, mean_maxEdgeLen);
        mean_totalEdgeLen += cur_meanEdgeLen;
        mean_elemEdgeLengths.push_back(cur_meanEdgeLen);

        double cur_maxEdgeLen = std::max({ edgeLen_1, edgeLen_2, edgeLen_3 });
        max_minEdgeLen = std::min(cur_maxEdgeLen, max_minEdgeLen);
        max_maxEdgeLen = std::max(cur_maxEdgeLen, max_maxEdgeLen);
        max_totalEdgeLen += cur_maxEdgeLen;
        max_elemEdgeLengths.push_back(cur_maxEdgeLen);
        // DEBUG
    }  // for (int el = ...

    // DEBUG
    // Compute standard deviation in element area
    // and area quarters
    double mean_area = totalArea / (double) elements->numElements;
    double std_dev_area = 0.0;

    double area_range = maxArea - minArea;
    int bin_count = 4;
    double bin_size = area_range / (double) bin_count;
    std::map<int, int> area_bins;

    double mean_meanEdgeLen = mean_totalEdgeLen / (double) elements->numElements;
    double max_meanEdgeLen = max_totalEdgeLen / (double) elements->numElements;
    double std_dev_meanEdgeLen = 0.0;
    double std_dev_maxEdgeLen = 0.0;

    for (int el = 0; el < elements->numElements; ++el) {
        const double &elemArea = elemAreas[el];
        std_dev_area += std::pow(elemArea - mean_area, 2.0);

        if (elemArea <= minArea) {
            area_bins[0] = area_bins[0] + 1;
        }
        else if (elemArea >= maxArea) {
            area_bins[bin_count - 1] = area_bins[bin_count - 1] + 1;
        }
        else {
            for (int i = 1; i <= bin_count; ++i) {
                if (elemArea < minArea + (bin_size * (double) i)) {
                    area_bins[i - 1] = area_bins[i - 1] + 1;
                    break;
                }
            }
        }

        const double &elemMeanEdgeLen = mean_elemEdgeLengths[el];
        std_dev_meanEdgeLen += std::pow(elemMeanEdgeLen - mean_meanEdgeLen, 2.0);

        const double &elemMaxEdgeLen = max_elemEdgeLengths[el];
        std_dev_maxEdgeLen += std::pow(elemMaxEdgeLen - max_meanEdgeLen, 2.0);
    }

    std_dev_area = std::sqrt(std_dev_area / (double) (elements->numElements - 1));
    std_dev_meanEdgeLen = std::sqrt(std_dev_meanEdgeLen / (double) (elements->numElements - 1));
    std_dev_maxEdgeLen = std::sqrt(std_dev_maxEdgeLen / (double) (elements->numElements - 1));
    // DEBUG

    // DEBUG
    std::cout << "Total area = " << totalArea << std::endl;
    std::cout << "Min. elem. area = " << minArea << std::endl;
    std::cout << "Max. elem. area = " << maxArea << std::endl;
    std::cout << "Elem. area range = " << area_range << std::endl;
    std::cout << "Mean elem. area = " << mean_area << std::endl;
    std::cout << "Std. dev. in elem. area = " << std_dev_area << std::endl;
    std::cout << "Min/max elem. area ratio = "
              << (maxArea / minArea) << std::endl;

    std::cout << "=====" << std::endl;
    for (int i = 0; i < bin_count; ++i) {
        std::cout << "Elems. in bin " << (i + 1) << " = "
                  << area_bins[i] << std::endl;
    }
    std::cout << "=====" << std::endl;

    std::cout << "Total edge length = " << totalEdgeLen << std::endl;
    std::cout << "Min. edge length = " << minEdgeLen << std::endl;
    std::cout << "Max. edge length = " << maxEdgeLen << std::endl;
    std::cout << "Edge length range = " << (maxEdgeLen - minEdgeLen) << std::endl;
    std::cout << "=====" << std::endl;

    std::cout << "(Mean) Total edge length = " << mean_totalEdgeLen << std::endl;
    std::cout << "(Mean) Min. edge length = " << mean_minEdgeLen << std::endl;
    std::cout << "(Mean) Max. edge length = " << mean_maxEdgeLen << std::endl;
    std::cout << "(Mean) Edge length range = " << (mean_maxEdgeLen - mean_minEdgeLen) << std::endl;
    std::cout << "(Mean) Mean edge length = "
              << (mean_totalEdgeLen / (double) elements->numElements) << std::endl;
    std::cout << "(Mean) Std. dev. in edge length = " << std_dev_meanEdgeLen << std::endl;
    std::cout << "(Mean) Min/max edge length ratio = "
              << (mean_maxEdgeLen / mean_minEdgeLen) << std::endl;
    std::cout << "=====" << std::endl;

    std::cout << "(Max) Total edge length = " << max_totalEdgeLen << std::endl;
    std::cout << "(Max) Min. edge length = " << max_minEdgeLen << std::endl;
    std::cout << "(Max) Max. edge length = " << max_maxEdgeLen << std::endl;
    std::cout << "(Max) Edge length range = " << (max_maxEdgeLen - max_minEdgeLen) << std::endl;
    std::cout << "(Max) Mean edge length = "
              << (max_totalEdgeLen / (double) elements->numElements) << std::endl;
    std::cout << "(Max) Std. dev. in edge length = " << std_dev_maxEdgeLen << std::endl;
    std::cout << "(Max) Min/max edge length ratio = "
              << (max_maxEdgeLen / max_minEdgeLen) << std::endl;
    std::cout << "=====" << std::endl;
    // DEBUG
}

void ShirleyDomain::fillXiEtaTraversal()
{
    // For each element, determine the order at which its SEM nodes
    // should be traversed as to comply with the canonical order for
    // (ξ,η) coordinates. The result will be stored in a vector.
    ElementFile *elements = m_elements;
    const int numTotalNodesPerElem = elements->getNumTotalNodesPerElem();
    const int polyDegree = elements->m_order;

    std::vector<double> stdXi(numTotalNodesPerElem);
    std::vector<double> stdEta(numTotalNodesPerElem);
    LobattoPolynomials::fillXiAndEtaArraysTri3(
        polyDegree, stdXi.data(), stdEta.data());

    m_elemIndexXiEtaTraversal.clear();

    for (int el = 0; el < elements->numElements; ++el) {
        // Get the (ξ,η) coordinates of all SEM nodes of this element
        std::vector<real_t> &xiEtaCoords = m_elemIndexXiEtaCoords[el];

        std::vector<int> xiEtaTraversal(numTotalNodesPerElem);

        for (int p = 0; p < numTotalNodesPerElem; ++p) {
            // Find out which SEM node of the element corresponds to
            // the current canonical (ξ,η) coordinates
            int stdXiEtaIndex = -1;
            double nodeXi, nodeEta;

            for (int j = 0; j < numTotalNodesPerElem; ++j) {
                nodeXi = xiEtaCoords[(j * 2) + 0];
                nodeEta = xiEtaCoords[(j * 2) + 1];

                if (valuesAreClose(nodeXi, stdXi[p], 1E-6) &&
                        valuesAreClose(nodeEta, stdEta[p], 1E-6)) {
                    stdXiEtaIndex = j;
                    break;
                }
            }

            if (stdXiEtaIndex == -1) {
                std::stringstream ss;
                ss << "ShirleyDomain::fillXiEtaTraversal(): "
                      "couldn't find index for standard xi/eta => "
                   << "e = " << el << ", p = " << p;
                throw ShirleyException(ss.str());
            }

            xiEtaTraversal[p] = stdXiEtaIndex;
        }  // for (int p = ...

        m_elemIndexXiEtaTraversal.push_back(xiEtaTraversal);
    }  // for (int el = ...
}

//
// Calculates the integral of a function defined on arg
//
void ShirleyDomain::setToIntegrals(RealVector& integrals,
                                   const escript::Data& arg) const
{
    setToIntegralsWorker<real_t>(integrals, arg);
}

void ShirleyDomain::setToIntegrals(ComplexVector& integrals,
                                   const escript::Data& arg) const
{
    setToIntegralsWorker<cplx_t>(integrals, arg);
}

template<typename Scalar>
void ShirleyDomain::setToIntegralsWorker(std::vector<Scalar>& integrals,
                                         const escript::Data& arg) const
{
    if (*arg.getFunctionSpace().getDomain() != *this) {
        throw ValueError(
            "ShirleyDomain::setToIntegralsWorker(): "
            "illegal domain of integration kernel");
    }

    switch (arg.getFunctionSpace().getTypeCode()) {
        case Elements:
            {
            unsigned int dpRank = arg.getDataPointRank();

            if (dpRank != 0) {
                std::stringstream ss;
                ss << "ShirleyDomain::setToIntegralsWorker(): "
                      "invalid data point rank for <arg> = " << dpRank;
                throw ShirleyException(ss.str());
            }

            if (m_elemIndexJacobian.empty()) {
                std::stringstream ss;
                ss << "ShirleyDomain::setToIntegralsWorker(): "
                      "the Jacobian vector for elements is empty. "
                      "Have you called ShirleyDomain::fillJacobians() yet?";
                throw ShirleyException(ss.str());
            }

            ElementFile *elements = m_elements;
            const int numTotalNodesPerElem = elements->getNumTotalNodesPerElem();

            const int polyDegree = elements->m_order;

            std::vector<double> stdXi(numTotalNodesPerElem);
            std::vector<double> stdEta(numTotalNodesPerElem);
            LobattoPolynomials::fillXiAndEtaArraysTri3(
                polyDegree, stdXi.data(), stdEta.data());

            integrals[0] = 0.0;

            const escript::DataReady_ptr &argReadyPtr = arg.borrowReadyPtr();
            const DataTypes::RealVectorType &argVector = argReadyPtr->getVectorRO();

            for (int el = 0; el < elements->numElements; ++el) {
                // Get the (x,y) coordinates of the vertex nodes for this element
                real_t x[3], y[3];

                index_t nodeId = elements->Nodes[elements->getVertexOffset(el, 0)];
                index_t arrayIdx = nodeId;
                x[0] = m_nodes->Coordinates[INDEX2(0, arrayIdx, m_nodes->numDim)];
                y[0] = m_nodes->Coordinates[INDEX2(1, arrayIdx, m_nodes->numDim)];

                nodeId = elements->Nodes[elements->getVertexOffset(el, 1)];
                arrayIdx = nodeId;
                x[1] = m_nodes->Coordinates[INDEX2(0, arrayIdx, m_nodes->numDim)];
                y[1] = m_nodes->Coordinates[INDEX2(1, arrayIdx, m_nodes->numDim)];

                nodeId = elements->Nodes[elements->getVertexOffset(el, 2)];
                arrayIdx = nodeId;
                x[2] = m_nodes->Coordinates[INDEX2(0, arrayIdx, m_nodes->numDim)];
                y[2] = m_nodes->Coordinates[INDEX2(1, arrayIdx, m_nodes->numDim)];

                // Compute (x,y) and (ξ,η) coordinates for all nodes of this element
                std::vector<double> nodeX(numTotalNodesPerElem);
                std::vector<double> nodeY(numTotalNodesPerElem);
                std::vector<double> nodeXi(numTotalNodesPerElem);
                std::vector<double> nodeEta(numTotalNodesPerElem);

                const double triangleArea = LobattoPolynomials::calcTriangleArea(
                    x[0], x[1], x[2], y[0], y[1], y[2]);

                for (int p = 0; p < numTotalNodesPerElem; ++p) {
                    // Get the (x,y) coordinates of the current node
                    nodeId = elements->Nodes[
                        elements->getVertexOffset(el, 0) + p];
                    arrayIdx = nodeId;
                    nodeX[p] = m_nodes->Coordinates[INDEX2(0, arrayIdx, m_nodes->numDim)];
                    nodeY[p] = m_nodes->Coordinates[INDEX2(1, arrayIdx, m_nodes->numDim)];

                    // Compute the (ξ,η) coordinates of the current node.
                    // Partial derivatives ∂Ψ_i/∂ξ and ∂Ψ_i/∂η will be calculated over them.
                    bool roundResultTowardZero = false;
                    nodeXi[p] = LobattoPolynomials::calcXiFromGeneralToStandardTriangle(
                        nodeX[p], nodeY[p], x[0], x[2], y[0], y[2], triangleArea, roundResultTowardZero);
                    nodeEta[p] = LobattoPolynomials::calcEtaFromGeneralToStandardTriangle(
                        nodeX[p], nodeY[p], x[0], x[1], y[0], y[1], triangleArea, roundResultTowardZero);
                }

                // Read determinant of the Jacobian matrix for this element
                const Jacobian2d& jac = m_elemIndexJacobian[el];
                double jac_det = jac.m_determinant;

                std::vector<double> psiCoeffs(numTotalNodesPerElem);

                for (int p = 0; p < numTotalNodesPerElem; ++p) {
                    // Finds the proper index for the (ξ,η) coordinates of the
                    // node in relation to the "standard" (ξ,η) coordinates.
                    int stdXiEtaIndex = -1;

                    for (int j = 0; j < numTotalNodesPerElem; ++j) {
                        if (valuesAreClose(nodeXi[j], stdXi[p], 1E-6) &&
                                valuesAreClose(nodeEta[j], stdEta[p], 1E-6)) {
                            stdXiEtaIndex = j;
                            break;
                        }
                    }

                    if (stdXiEtaIndex == -1) {
                        std::stringstream ss;
                        ss << "ShirleyDomain::setToIntegralsWorker(): "
                              "couldn't find index for standard xi/eta => "
                           << "element = " << el << ", ordinal node = " << p;
                        throw ShirleyException(ss.str());
                    }

                    // Get the coefficients for Ψ_i. These are the b^i_{kl}
                    // values in Eq. 3.2, pg. 12 of doi:10.1093/imamat/hxh077.
                    VdmSystemsTri3::fillVdmCoefficients(
                        polyDegree, p + 1, psiCoeffs.data());

                    // Eq 3.9, pg. 14 of doi:10.1093/imamat/hxh077
                    double integrationWeight = 0.5 * psiCoeffs[0];

                    long offset = argReadyPtr->getPointOffset(el, stdXiEtaIndex);
                    real_t funcVal = argVector[offset];
                    real_t integralValue = jac_det * integrationWeight * funcVal;
                    integrals[0] += integralValue;
                }  // for (int nn = ...
            }  // for (int el = ...
            }  // case Elements

            break;

        default:
            std::stringstream ss;
            ss << "ShirleyDomain::setToIntegrals(): Shirley does not know "
                  "anything about function space type "
               << functionSpaceTypeAsString(arg.getFunctionSpace().getTypeCode());
            throw ValueError(ss.str());
    }
}

int ShirleyDomain::getSystemMatrixTypeId(const boost::python::object& options)
    const
{
    throw NotImplementedError(
        "ShirleyDomain::getSystemMatrixTypeId(): "
        "this member function is currently not implemented.");
}

int ShirleyDomain::getTransportTypeId(int solver, int preconditioner,
                                      int package, bool symmetry) const
{
#ifdef ESYS_HAVE_PASO
    return paso::TransportProblem::getTypeId(solver, preconditioner, package,
                                             symmetry, getMPI());
#else
    throw ShirleyException(
        "ShirleyDomain::getTransportTypeId(): transport solvers require "
        "Paso, but Shirley was not compiled with Paso support!");
#endif
}

//
// Returns true if data on functionSpaceCode is considered as being cell centered
//
bool ShirleyDomain::isCellOriented(int functionSpaceCode) const
{
    throw NotImplementedError(
        "ShirleyDomain::isCellOriented(): "
        "this member function is currently not implemented.");
}

bool ShirleyDomain::ownSample(int fsCode, index_t id) const
{
#ifdef ESYS_MPI
    if (getMPISize() > 1) {
        if (fsType == Nodes || fsType == Elements) {
            const index_t myFirstNode = m_nodeDistribution[getMPIRank()];
            const index_t myLastNode = m_nodeDistribution[getMPIRank()+1];
            const index_t k = m_nodeId[id];
            return (myFirstNode <= k && k < myLastNode);
        } else {
            throw ShirleyException(
                "ShirleyDomain::ownSample(): unsupported function space type");
        }
    }
#endif
    return true;
}

//
// Adds linear PDE of second order into a given stiffness matrix and
// right hand side
//
void ShirleyDomain::addPDEToSystem(
    escript::AbstractSystemMatrix& mat, escript::Data& rhs,
    const escript::Data& A, const escript::Data& B, const escript::Data& C,
    const escript::Data& D, const escript::Data& X, const escript::Data& Y,
    const escript::Data& d, const escript::Data& y,
    const escript::Data& d_contact, const escript::Data& y_contact,
    const escript::Data& d_dirac, const escript::Data& y_dirac) const
{
    throw NotImplementedError(
        "ShirleyDomain::addPDEToSystem(): "
        "this member function is currently not implemented.");
}

void ShirleyDomain::addPDEToLumpedSystem(escript::Data& mat,
                                         const escript::Data& D,
                                         const escript::Data& d,
                                         const escript::Data& d_dirac,
                                         bool useHRZ) const
{
    throw NotImplementedError(
        "ShirleyDomain::addPDEToLumpedSystem(): "
        "this member function is currently not implemented.");
}

//
// Adds linear PDE of second order into the right hand side only
//
void ShirleyDomain::addPDEToRHS(escript::Data& rhs, const escript::Data& X,
      const escript::Data& Y, const escript::Data& y,
      const escript::Data& y_contact, const escript::Data& y_dirac) const
{
    throw NotImplementedError(
        "ShirleyDomain::addPDEToRHS(): "
        "this member function is currently not implemented.");
}

//
// Adds PDE of second order into a transport problem
//
void ShirleyDomain::addPDEToTransportProblem(
    escript::AbstractTransportProblem& tp, escript::Data& source,
    const escript::Data& M, const escript::Data& A, const escript::Data& B,
    const escript::Data& C, const escript::Data& D, const escript::Data& X,
    const escript::Data& Y, const escript::Data& d, const escript::Data& y,
    const escript::Data& d_contact, const escript::Data& y_contact,
    const escript::Data& d_dirac, const escript::Data& y_dirac) const
{
#ifdef ESYS_HAVE_PASO
    throw NotImplementedError(
        "ShirleyDomain::addPDEToTransportProblem(): "
        "this member function is currently not implemented.");
#else
    throw ShirleyException(
        "ShirleyDomain::addPDEToTransportProblem(): transport problems "
        "require the Paso library, which is not available.");
#endif
}

//
// Creates a stiffness matrix and initializes it with zeros
//
escript::ASM_ptr ShirleyDomain::newSystemMatrix(int row_blocksize,
    const escript::FunctionSpace& row_functionspace,
    int column_blocksize,
    const escript::FunctionSpace& column_functionspace,
    int systemMatrixType) const
{
    throw NotImplementedError(
        "ShirleyDomain::newSystemMatrix(): "
        "this member function is currently not implemented.");
}

//
// Creates a TransportProblem
//
escript::ATP_ptr ShirleyDomain::newTransportProblem(int blocksize,
    const escript::FunctionSpace& functionSpace, int type) const
{
    // Is the domain right?
    if (*functionSpace.getDomain() != *this) {
        throw ValueError(
            "ShirleyDomain::newTransportProblem(): domain of function "
            "space does not match the domain of transport problem generator.");
    }

#ifdef ESYS_HAVE_PASO
    throw NotImplementedError(
        "ShirleyDomain::newTransportProblem(): "
        "this member function is currently not implemented.");
#else
    throw ShirleyException(
        "ShirleyDomain::newTransportProblem(): transport problems require "
        "the Paso library, which is not available.");
#endif
}

escript::Data ShirleyDomain::getX() const
{
    return continuousFunction(*this).getX();
}

escript::Data ShirleyDomain::getNormal() const
{
    return functionOnBoundary(*this).getNormal();
}

escript::Data ShirleyDomain::getSize() const
{
    return escript::function(*this).getSize();
}

bool ShirleyDomain::operator==(const AbstractDomain& other) const
{
    const ShirleyDomain* temp = dynamic_cast<const ShirleyDomain*>(&other);

    if (temp) {
        return ((m_nodes == temp->m_nodes) &&
                (m_elements == temp->m_elements) &&
                (m_faceElements == temp->m_faceElements) &&
                (m_points == temp->m_points));
    }

    return false;
}

bool ShirleyDomain::operator!=(const AbstractDomain& other) const
{
    return !(operator==(other));
}

void ShirleyDomain::setTags(int functionSpaceType, int newTag,
                            const escript::Data& mask) const
{
    switch (functionSpaceType) {
        case Nodes:
            m_nodes->setTags(newTag, mask);
            break;
        case Elements:
            m_elements->setTags(newTag, mask);
            break;
        case FaceElements:
            m_faceElements->setTags(newTag, mask);
            break;
        case Points:
            m_points->setTags(newTag, mask);
            break;
        case DegreesOfFreedom:
            throw ValueError("ShirleyDomain::setTags(): "
                             "DegreesOfFreedom does not support tags");
        default:
            std::stringstream ss;
            ss << "ShirleyDomain::setTags(): Shirley does not know "
                  "anything about function space type "
               << functionSpaceTypeAsString(functionSpaceType);
            throw ValueError(ss.str());
    }
}

int ShirleyDomain::getNumberOfTagsInUse(int functionSpaceCode) const
{
    switch (functionSpaceCode) {
        case Nodes:
            return m_nodes->tagsInUse.size();
        case Elements:
            return m_elements->tagsInUse.size();
        case FaceElements:
            return m_faceElements->tagsInUse.size();
        case Points:
            return m_points->tagsInUse.size();
        case DegreesOfFreedom:
            throw ValueError("ShirleyDomain::getNumberOfTagsInUse(): "
                             "DegreesOfFreedom does not support tags");
        default:
            std::stringstream ss;
            ss << "ShirleyDomain::getNumberOfTagsInUse(): Shirley does not know "
                  "anything about function space type "
               << functionSpaceTypeAsString(functionSpaceCode);
            throw ValueError(ss.str());
    }
}

const int* ShirleyDomain::borrowListOfTagsInUse(int functionSpaceCode) const
{
    switch (functionSpaceCode) {
        case Nodes:
            return m_nodes->tagsInUse.empty() ?
                nullptr : &m_nodes->tagsInUse[0];
        case Elements:
            return m_elements->tagsInUse.empty() ?
                nullptr : &m_elements->tagsInUse[0];
        case FaceElements:
            return m_faceElements->tagsInUse.empty() ?
                nullptr : &m_faceElements->tagsInUse[0];
        case Points:
            return m_points->tagsInUse.empty() ?
                nullptr : &m_points->tagsInUse[0];
        case DegreesOfFreedom:
            throw ValueError("ShirleyDomain::borrowListOfTagsInUse(): "
                             "DegreesOfFreedom does not support tags");
        default:
            std::stringstream ss;
            ss << "ShirleyDomain::borrowListOfTagsInUse(): Shirley does not "
                  "know anything about function space type "
               << functionSpaceTypeAsString(functionSpaceCode);
            throw ValueError(ss.str());
    }
}

bool ShirleyDomain::canTag(int functionSpaceCode) const
{
    switch (functionSpaceCode) {
        case Nodes:
        case Elements:
        case FaceElements:
        case Points:
            return true;
        default:
            return false;
    }
}

int ShirleyDomain::getApproximationOrder(int functionSpaceCode) const
{
    throw NotImplementedError(
        "ShirleyDomain::getApproximationOrder(): "
        "this member function is currently not implemented.");
}

escript::Data ShirleyDomain::randomFill(
    const escript::DataTypes::ShapeType& shape,
    const escript::FunctionSpace& what, long seed,
    const boost::python::tuple& filter) const
{
    escript::Data towipe(0, shape, what, true);

    // Since we just made this object, no sharing is possible and we don't
    // need to check for exclusive write
    DataTypes::RealVectorType& dv(towipe.getExpandedVectorReference());
    escript::randomFillArray(seed, &dv[0], static_cast<size_t>(dv.size()));

    return towipe;
}

/// Prepares the mesh for further use
void ShirleyDomain::prepare(bool optimize)
{
    // First step is to distribute the elements according to a global
    // distribution of DOF
    IndexVector distribution(static_cast<unsigned long>(m_mpiInfo->size + 1));

    // First we create dense labeling for the DOFs
    dim_t newGlobalNumDOFs = m_nodes->createDenseDOFLabeling();

    // Create a distribution of the global DOFs and determine the MPI rank
    // controlling the DOFs on this processor
    m_mpiInfo->setDistribution(0, newGlobalNumDOFs - 1, &distribution[0]);

    // Now the mesh is re-distributed according to the distribution vector
    // this will redistribute the Nodes and Elements including overlap and
    // will create an element colouring but will not create any mappings
    // (see later in this function)
    distributeByRankOfDOF(distribution);

    // At this stage we are able to start an optimization of the DOF
    // distribution using ParMetis. On return distribution is altered and
    // new DOF IDs have been assigned
    if (optimize && (m_mpiInfo->size > 1)) {
        optimizeDOFDistribution(distribution);
        distributeByRankOfDOF(distribution);
    }

    // The local labelling of the degrees of freedom is optimized
    if (optimize) {
        optimizeDOFLabeling(distribution);
    }

    // Rearrange elements with the aim of bringing elements closer to memory
    // locations of the nodes (distributed shared memory!):
    // ATTENTION: ShirleyDomain::optimizeElementOrdering() calls
    // ElementFile::optimizeOrdering(), which should *NOT* be used!
    //optimizeElementOrdering();

    // Create the global indices
    IndexVector nodeDistribution(static_cast<unsigned long>(m_mpiInfo->size + 1));

    m_nodes->createDenseNodeLabeling(nodeDistribution, distribution);

    // Create the missing mappings
    createMappings(distribution, nodeDistribution);

    updateTagList();
}

/// Tries to reduce the number of colours for all element files
void ShirleyDomain::createColoring(const index_t* node_localDOF_map)
{
    m_elements->createColoring(m_nodes->getNumNodes(), node_localDOF_map);
    m_faceElements->createColoring(m_nodes->getNumNodes(), node_localDOF_map);
    m_points->createColoring(m_nodes->getNumNodes(), node_localDOF_map);
}

/// Redistributes elements to minimize communication during assemblage
void ShirleyDomain::optimizeElementOrdering()
{
    // ATTENTION: ElementFile::optimizeOrdering() should *NOT* be used!
    m_elements->optimizeOrdering();
    m_faceElements->optimizeOrdering();
    m_points->optimizeOrdering();
}

void ShirleyDomain::createMappings(const IndexVector& dofDist,
                                   const IndexVector& nodeDist)
{
    m_nodes->createNodeMappings(dofDist, nodeDist);

#ifdef ESYS_HAVE_TRILINOS
    // TODO?: the following block should probably go into prepare() but
    // Domain::load() only calls createMappings() which is why it's here...
    // Make sure trilinos distribution graph is available for matrix building
    // and interpolation
    const index_t numTargets = m_nodes->getNumDegreesOfFreedomTargets();
    const index_t* target = m_nodes->borrowTargetDegreesOfFreedom();
    boost::scoped_array<IndexList> indexList(new IndexList[numTargets]);

#pragma omp parallel
    {
        // Insert contributions from element matrices into columns in
        // index list
        IndexList_insertElements(indexList.get(), m_elements, target);
        IndexList_insertElements(indexList.get(), m_faceElements, target);
        IndexList_insertElements(indexList.get(), m_points, target);
    }

    m_nodes->createTrilinosGraph(indexList.get());
#endif
}

void ShirleyDomain::markNodes(IntVector& mask, index_t offset) const
{
    m_elements->markNodes(mask, offset);
    m_faceElements->markNodes(mask, offset);
    m_points->markNodes(mask, offset);
}

void ShirleyDomain::relabelElementNodes(const IndexVector& newNodes,
                                        index_t offset)
{
    m_elements->relabelNodes(newNodes, offset);
    m_faceElements->relabelNodes(newNodes, offset);
    m_points->relabelNodes(newNodes, offset);
}

/// Regenerates list of tags in use for node file and element files
void ShirleyDomain::updateTagList()
{
    m_nodes->updateTagList();
    m_elements->updateTagList();
    m_faceElements->updateTagList();
    m_points->updateTagList();
}

dim_t ShirleyDomain::findNode(const double *coords) const
{
    // Traverses elements to find out which vertex node is closest to
    // the coordinates supplied by the caller. SEM nodes located along
    // element edges or inside element faces will *NOT* be checked.
    double x_c = coords[0];
    double y_c = coords[1];

    const dim_t NOT_MINE = -1;
    dim_t closest = NOT_MINE;
    double minDist = std::numeric_limits<double>::max();

    for (int el = 0; el < m_elements->numElements; ++el) {
        // First vertex node for this element
        index_t nodeId = m_elements->Nodes[m_elements->getVertexOffset(el, 0)];
        index_t arrayIdx = nodeId;
        real_t x = m_nodes->Coordinates[INDEX2(0, arrayIdx, m_nodes->numDim)];
        real_t y = m_nodes->Coordinates[INDEX2(1, arrayIdx, m_nodes->numDim)];

        real_t xDist = x_c - x;
        real_t yDist = y_c - y;
        real_t curDist = std::sqrt((xDist * xDist) + (yDist * yDist));

        if (curDist < minDist) {
            minDist = curDist;
            closest = nodeId;
        }

        // Second vertex node for this element
        nodeId = m_elements->Nodes[m_elements->getVertexOffset(el, 1)];
        arrayIdx = nodeId;
        x = m_nodes->Coordinates[INDEX2(0, arrayIdx, m_nodes->numDim)];
        y = m_nodes->Coordinates[INDEX2(1, arrayIdx, m_nodes->numDim)];

        xDist = x_c - x;
        yDist = y_c - y;
        curDist = std::sqrt((xDist * xDist) + (yDist * yDist));

        if (curDist < minDist) {
            minDist = curDist;
            closest = nodeId;
        }

        // Third vertex node for this element
        nodeId = m_elements->Nodes[m_elements->getVertexOffset(el, 2)];
        arrayIdx = nodeId;
        x = m_nodes->Coordinates[INDEX2(0, arrayIdx, m_nodes->numDim)];
        y = m_nodes->Coordinates[INDEX2(1, arrayIdx, m_nodes->numDim)];

        xDist = x_c - x;
        yDist = y_c - y;
        curDist = std::sqrt((xDist * xDist) + (yDist * yDist));

        if (curDist < minDist) {
            minDist = curDist;
            closest = nodeId;
        }
    }

    // If this happens, we've let a Dirac point slip through, which is awful
    if (closest == NOT_MINE) {
        throw ShirleyException("ShirleyDomain::findNode(): "
                               "Unable to map appropriate dirac point to a node");
    }

    index_t arrayIdx = closest;
    real_t x = m_nodes->Coordinates[INDEX2(0, arrayIdx, m_nodes->numDim)];
    real_t y = m_nodes->Coordinates[INDEX2(1, arrayIdx, m_nodes->numDim)];
    std::cout << "Node closest to (" << x_c << "," << y_c << ")"
      << " has ID " << closest
      << " and is located at (" << x << "," << y << ")" << std::endl;

    return closest;
}

void ShirleyDomain::addPoints(
    const std::vector<double>& coords, const std::vector<int>& tags)
{
    for (size_t i = 0; i < tags.size(); i++) {
        dim_t node = findNode(&coords[i * m_nodes->numDim]);

        if (node >= 0) {
            m_diracPointNodeIDs.push_back(borrowSampleReferenceIDs(Nodes)[node]);
            DiracPoint dp;
            dp.node = node; //local
            dp.tag = tags[i];
            m_diracPoints.push_back(dp);
        }
        else if (m_mpiInfo->size == 1) {
            throw ShirleyException(
                "ShirleyDomain::addPoints(): Dirac point unmapped");
        }
    }
}

void ShirleyDomain::clearInputSignal()
{
    delete m_inputSignal;
    m_inputSignal = nullptr;
}

void ShirleyDomain::setRickerSignal(
    double f_dom, double t_dom, double signalDelay)
{
    if (m_inputSignal != nullptr) {
        clearInputSignal();
    }

    m_inputSignal = new Ricker(f_dom, t_dom);
    m_signalDelay = signalDelay;
}

void ShirleyDomain::setPdeCoefficients(const escript::Data& M,
    const escript::Data& B, const escript::Data& D,
    const escript::Data& E, const escript::Data& F)
{
    std::cout << "ShirleyDomain::setPdeCoefficients()" << std::endl;

    if (!M.isEmpty()) {
        // For the acoustic wave, M should be a Scalar() defined on Function()
        if (M.getDataPointRank() != 0) {
            throw ShirleyException(
                "ShirleyDomain::setPdeCoefficients(): "
                "M coefficient must have rank 0 (i.e. Scalar)");
        }

        if (M.getFunctionSpace().getTypeCode() != Elements) {
            throw ShirleyException(
                "ShirleyDomain::setPdeCoefficients(): "
                "M coefficient must be defined on the Function() "
                "function space, please interpolate");
        }
    }

    if (!B.isEmpty()) {
        // For the acoustic wave, B should be a Tensor() defined on Function()
        if (B.getDataPointRank() != 2) {
            throw ShirleyException(
                "ShirleyDomain::setPdeCoefficients(): "
                "B coefficient must have rank 2 (i.e. Tensor)");
        }

        // FIXME Check for correct shape

        if (B.getFunctionSpace().getTypeCode() != Elements) {
            throw ShirleyException(
                "ShirleyDomain::setPdeCoefficients(): "
                "B coefficient must be defined on the Function() "
                "function space, please interpolate");
        }
    }

    if (!D.isEmpty()) {
        // For the acoustic wave, D should be a Scalar() defined on Function()
        if (D.getDataPointRank() != 0) {
            throw ShirleyException(
                "ShirleyDomain::setPdeCoefficients(): "
                "D coefficient must have rank 0 (i.e. Scalar)");
        }

        if (D.getFunctionSpace().getTypeCode() != Elements) {
            throw ShirleyException(
                "ShirleyDomain::setPdeCoefficients(): "
                "D coefficient must be defined on the Function() "
                "function space, please interpolate");
        }
    }

    if (!E.isEmpty()) {
        // For the acoustic wave, E should be a Tensor() defined on Function()
        if (E.getDataPointRank() != 2) {
            throw ShirleyException(
                "ShirleyDomain::setPdeCoefficients(): "
                "E coefficient must have rank 2 (i.e. Tensor)");
        }

        // FIXME Check for correct shape

        if (E.getFunctionSpace().getTypeCode() != Elements) {
            throw ShirleyException(
                "ShirleyDomain::setPdeCoefficients(): "
                "E coefficient must be defined on the Function() "
                "function space, please interpolate");
        }
    }

    if (!F.isEmpty()) {
        // For the acoustic wave, F should be a Tensor() defined on Function()
        if (F.getDataPointRank() != 2) {
            throw ShirleyException(
                "ShirleyDomain::setPdeCoefficients(): "
                "F coefficient must have rank 2 (i.e. Tensor)");
        }

        // FIXME Check for correct shape

        if (F.getFunctionSpace().getTypeCode() != Elements) {
            throw ShirleyException(
                "ShirleyDomain::setPdeCoefficients(): "
                "F coefficient must be defined on the Function() "
                "function space, please interpolate");
        }
    }

    m_M = escript::Data();

    if (!M.isEmpty()) {
        m_M.copy(M);
    }

    m_B = escript::Data();

    if (!B.isEmpty()) {
        m_B.copy(B);
    }

    m_D = escript::Data();

    if (!D.isEmpty()) {
        m_D.copy(D);
    }

    m_E = escript::Data();

    if (!E.isEmpty()) {
        m_E.copy(E);
    }

    m_F = escript::Data();

    if (!F.isEmpty()) {
        m_F.copy(F);
    }
}

void ShirleyDomain::setInitialSolution(
    const escript::Data& u, const escript::Data& v, double currentTime)
{
    bool uEmpty = u.isEmpty();

    if (!uEmpty && (u.getFunctionSpace().getTypeCode() != DegreesOfFreedom)) {
        throw ShirleyException(
            "ShirleyDomain::setInitialSolution(): "
            "u must be defined on the Solution() "
            "function space, please interpolate");
    }

    bool vEmpty = v.isEmpty();

    if (!vEmpty && (v.getFunctionSpace().getTypeCode() != Elements)) {
        throw ShirleyException(
            "ShirleyDomain::setInitialSolution(): "
            "v must be defined on the Function() "
            "function space, please interpolate");
    }

    if (uEmpty) {
        m_u = escript::Scalar(0., escript::solution(*this), true);
    }
    else {
        m_u = escript::Data();
        m_u.copy(u);
        m_u.expand();
    }

    if (vEmpty) {
        m_v = escript::Vector(0., escript::function(*this), true);
    }
    else {
        m_v = escript::Data();
        m_v.copy(v);
        m_v.expand();
    }

    m_currentTime = currentTime;
}

escript::Data ShirleyDomain::getU()
{
    return m_u;
}

escript::Data ShirleyDomain::getV()
{
    return m_v;
}

double ShirleyDomain::getCurrentTime()
{
    return m_currentTime;
}

void ShirleyDomain::calculateTimeIntegrationMatrices(double timeStep)
{
    std::cout << "ShirleyDomain::calculateTimeIntegrationMatrices()" << std::endl;

    if (timeStep <= 0.0) {
        throw ShirleyException(
            "ShirleyDomain::calculateTimeIntegrationMatrices(): "
            "time step must be greater than zero");
    }

    m_timeStep = timeStep;

    if (m_M.isEmpty()) {
        std::stringstream ss;
        ss << "ShirleyDomain::calculateTimeIntegrationMatrices(): "
              "M matrix shouldn't be empty! Try setting it as "
              "a Scalar().";
        throw ShirleyException(ss.str());
    }

    ElementFile *elements = m_elements;
    const int polyDegree = elements->m_order;
    const int numTotalNodesPerElem = elements->getNumTotalNodesPerElem();

    // Precompute integration weights
    std::vector<double> integrationWeights(numTotalNodesPerElem);
    double *integrationWeights_ptr = integrationWeights.data();

    for (int q = 0; q < numTotalNodesPerElem; ++q) {
        integrationWeights_ptr[q] = VdmSystemsTri3::
            getIntegrationWeight(polyDegree, q + 1);
    }

    // Matrix \bar{M}_{ν}
    m_M_bar = escript::Scalar(0., escript::solution(*this), true);
    m_M_bar.requireWrite();

    const real_t *m_ro = &m_M.getDataRO(0.0)[0];
    const index_t* targetDof = m_nodes->borrowTargetDegreesOfFreedom();

    //DEBUG
    double min_m = std::numeric_limits<double>::max();
    double max_m = std::numeric_limits<double>::lowest();
    double min_det = std::numeric_limits<double>::max();
    double max_det = std::numeric_limits<double>::lowest();
    double min_wq = std::numeric_limits<double>::max();
    double max_wq = std::numeric_limits<double>::lowest();
    //DEBUG

    for (int el = 0; el < elements->numElements; ++el) {
        const Jacobian2d& jac = m_elemIndexJacobian[el];
        double jac_det = jac.m_determinant;

        // Read value of M for this element
        real_t matM_value = m_ro[m_M.getDataOffset(el, 0)];

        const std::vector<int> &xiEtaTraversal =
            m_elemIndexXiEtaTraversal[el];
        const int *xiEtaTraversal_ptr = xiEtaTraversal.data();

        //DEBUG
        min_m = std::min(min_m, matM_value);
        max_m = std::max(max_m, matM_value);
        min_det = std::min(min_det, jac_det);
        max_det = std::max(max_det, jac_det);
        //DEBUG

        // Traverse SEM nodes
        for (int q = 0; q < numTotalNodesPerElem; ++q) {
            const int stdXiEtaIndexQ = xiEtaTraversal_ptr[q];

            // See Eq. 3.9, pg. 14 of doi:10.1093/imamat/hxh077
            const double integrationWeight = integrationWeights_ptr[q];

            //DEBUG
            min_wq = std::min(min_wq, integrationWeight);
            max_wq = std::max(max_wq, integrationWeight);
            //DEBUG

            // Accumulate value of \bar{M}_{ν} for this degree of freedom
            double M_bar_value = matM_value * jac_det * integrationWeight;

            index_t nodeId = elements->Nodes[
                elements->getVertexOffset(el, 0) + stdXiEtaIndexQ];
            index_t realNodeId = targetDof[nodeId];
            m_M_bar.getSampleDataRW(realNodeId, 0.0)[0] += M_bar_value;
        }  // for (int q = ...
    }  // for (int el = ...

    m_M_bar_inv = escript::Scalar(1., escript::solution(*this), true);
    m_M_bar_inv /= m_M_bar;

    // DEBUG
    std::cout << "M   => " << min_m   << " , " << max_m   << std::endl;
    std::cout << "det => " << min_det << " , " << max_det << std::endl;
    std::cout << "wq  => " << min_wq  << " , " << max_wq  << std::endl;
    // DEBUG

    std::cout << "m_M_bar     => " << m_M_bar.toString() << std::endl;
    std::cout << "m_M_bar_inv => " << m_M_bar_inv.toString() << std::endl;

    // Matrix \bar{D}_{ν}
    m_D_bar = escript::Scalar(0., escript::solution(*this), true);
    m_D_bar.requireWrite();

    bool d_is_empty = m_D.isEmpty();
    const real_t *d_ro = d_is_empty ?
        nullptr : &m_D.getDataRO(0.0)[0];

    for (int el = 0; el < elements->numElements; ++el) {
        const Jacobian2d& jac = m_elemIndexJacobian[el];
        double jac_det = jac.m_determinant;

        // Read value of D for this element
        real_t matD_value = 0.0;

        if (!d_is_empty) {
            matD_value = d_ro[m_D.getDataOffset(el, 0)];
        }

        const std::vector<int> &xiEtaTraversal =
            m_elemIndexXiEtaTraversal[el];
        const int *xiEtaTraversal_ptr = xiEtaTraversal.data();

        // Traverse SEM nodes
        for (int q = 0; q < numTotalNodesPerElem; ++q) {
            const int stdXiEtaIndexQ = xiEtaTraversal_ptr[q];

            // See Eq. 3.9, pg. 14 of doi:10.1093/imamat/hxh077
            const double integrationWeight = integrationWeights_ptr[q];

            // Accumulate value of \bar{D}_{ν} for this degree of freedom
            double D_bar_value = matD_value * jac_det * integrationWeight;

            index_t nodeId = elements->Nodes[
                elements->getVertexOffset(el, 0) + stdXiEtaIndexQ];
            index_t realNodeId = targetDof[nodeId];
            m_D_bar.getSampleDataRW(realNodeId, 0.0)[0] += D_bar_value;
        }  // for (int q = ...
    }  // for (int el = ...

    std::cout << "m_D_bar (1) => " << m_D_bar.toString() << std::endl;

    m_D_bar /= m_M_bar;  // \bar{D}_{ν} must be multiplied by \bar{M}_{ν}^-1

    std::cout << "m_D_bar (2) => " << m_D_bar.toString() << std::endl;
}

const int DPOI_TYPE_UNKNOWN  = 1;
const int DPOI_TYPE_EXPANDED = 2;
const int DPOI_TYPE_CONSTANT = 4;
const int DPOI_TYPE_TAGGED   = 8;
const int DPOI_TYPE_EMPTY    = 16;

struct DataPointOffsetInfo {
    int dpoiType;
    DataTypes::RealVectorType::size_type numValues;
    int numDataPointsPerSample;
};

struct DataPointOffsetInfo getDataPointOffsetInfo(const escript::Data &data)
{
    DataPointOffsetInfo result;

    if (data.isExpanded()) {
        result.dpoiType = DPOI_TYPE_EXPANDED;
    }
    else if (data.isConstant()) {
        result.dpoiType = DPOI_TYPE_CONSTANT;
    }
    else if (data.isTagged()) {
        result.dpoiType = DPOI_TYPE_TAGGED;
    }
    else if (data.isEmpty()) {
        result.dpoiType = DPOI_TYPE_EMPTY;
    }
    else {
        result.dpoiType = DPOI_TYPE_UNKNOWN;
    }

    result.numValues = data.getNoValues();
    result.numDataPointsPerSample = data.getNumDataPointsPerSample();

    return result;
}

static inline
DataTypes::RealVectorType::size_type calcDataOffset(
    const DataPointOffsetInfo &dpoInfo, int sampleNo, int dataPointNo,
    escript::Data *data_ptr)
{
    switch (dpoInfo.dpoiType) {
        case DPOI_TYPE_EXPANDED:
            // Copied from DataExpanded::getPointOffset()
            // for performance reasons
            return ((sampleNo * dpoInfo.numDataPointsPerSample) + dataPointNo)
                   * dpoInfo.numValues;

        case DPOI_TYPE_CONSTANT:
            // Copied from DataConstant::getPointOffset()
            // for performance reasons
            return 0;

        case DPOI_TYPE_TAGGED:
            return data_ptr->getDataOffset(sampleNo, dataPointNo);

        default:
            throw ShirleyException(
                "ShirleyDomain::calcDataOffset(): "
                "DataPointOffsetInfo::dpoiType not supported.");
    }
}

void ShirleyDomain::progressToTime(double finalTime)
{
    if (finalTime <= m_currentTime) {
        std::stringstream ss;
        ss << "ShirleyDomain::progressToTime(): final time ("
           << finalTime << ") must be greater than current time ("
           << m_currentTime << ")";
        throw ShirleyException(ss.str());
    }

    double dtMax = m_timeStep;
    double dt = finalTime - m_currentTime;
    int dtSteps = std::max(static_cast<int>(std::ceil(dt / dtMax)), 1);
    double t;

    ElementFile *elements = m_elements;
    const int numTotalNodesPerElem = elements->getNumTotalNodesPerElem();
    const int polyDegree = elements->m_order;

    std::vector<double> stdXi(numTotalNodesPerElem);
    std::vector<double> stdEta(numTotalNodesPerElem);
    LobattoPolynomials::fillXiAndEtaArraysTri3(
        polyDegree, stdXi.data(), stdEta.data());

    const index_t* targetDof = m_nodes->borrowTargetDegreesOfFreedom();

    // Matrix E
    if (m_E.isEmpty()) {
        // Having E being always non-empty allows for some performance
        // optimizations
        m_E = escript::Tensor(0., escript::function(*this), true);
    }

    const real_t* matE_ro = &m_E.getDataRO(0.0)[0];
    const DataTypes::ShapeType matE_shape = m_E.getDataPointShape();

    // Other matrices
    const DataTypes::ShapeType& matF_shape = m_F.getDataPointShape();
    const DataTypes::ShapeType& matB_shape = m_B.getDataPointShape();

    const real_t* F_ro = &m_F.getDataRO(0.0)[0];
    const real_t* B_ro = &m_B.getDataRO(0.0)[0];
    const real_t* M_bar_inv_ro = &m_M_bar_inv.getDataRO(0.0)[0];

    // Current U
    escript::Data currU = escript::Data();
    currU.copy(m_u);
    currU.expand();

    // Current V
    escript::Data currV = escript::Data();
    currV.copy(m_v);
    currV.expand();

    // Predictor values
    escript::Data uPred = escript::Scalar(0., escript::solution(*this), true);
    escript::Data vPred = escript::Vector(0., escript::function(*this), true);

    // Corrector values
    escript::Data uCorr = escript::Scalar(0., escript::solution(*this), true);
    escript::Data vCorr = escript::Vector(0., escript::function(*this), true);

    // Scalar values
    escript::Data timeStep_mul_D_bar =
        escript::Scalar(m_timeStep, escript::solution(*this), true);
    timeStep_mul_D_bar *= m_D_bar;

    const real_t *ts_mul_D_bar_ro = &timeStep_mul_D_bar.getDataRO(0.0)[0];

    // DataTypes::getRelIndex() is expensive in tight loops, so we
    // precompute some values to speed things up a bit
    long matE_relIndex[2][2];
    long matF_relIndex[2][2];
    long matB_relIndex[2][2];

    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            matE_relIndex[i][j] = DataTypes::getRelIndex(matE_shape, i, j);
            matF_relIndex[i][j] = DataTypes::getRelIndex(matF_shape, i, j);
            matB_relIndex[i][j] = DataTypes::getRelIndex(matB_shape, i, j);
        }
    }

    // Precompute integration weights
    std::vector<double> integrationWeights(numTotalNodesPerElem);
    double *integrationWeights_ptr = integrationWeights.data();

    for (int q = 0; q < numTotalNodesPerElem; ++q) {
        integrationWeights_ptr[q] = VdmSystemsTri3::
            getIntegrationWeight(polyDegree, q + 1);
    }

    const double kronecker_2d[2][2] = { { 1.0, 0.0 }, { 0.0, 1.0 } };

    const double timeStep_ld = m_timeStep;

    // Repackage raw node derivatives for quick access
    struct rawNodeDerivatives2d {
        const double* dpsi_dxi;
        const double* dpsi_deta;
    };

    std::vector<rawNodeDerivatives2d> rawCanonNodeDerivatives(numTotalNodesPerElem);
    rawNodeDerivatives2d *rawCanonNodeDerivatives_ptr = rawCanonNodeDerivatives.data();

    for (int p = 0; p < numTotalNodesPerElem; ++p) {
        const NodeDerivatives2d &nodeDerivs = m_canonNodeDerivatives[p];
        rawCanonNodeDerivatives_ptr[p].dpsi_dxi = nodeDerivs.m_dpsi_dxi.data();
        rawCanonNodeDerivatives_ptr[p].dpsi_deta = nodeDerivs.m_dpsi_deta.data();
    }

    int u_numSamples = currU.getNumSamples();
    int u_numDataPointsPerSample = currU.getNumDataPointsPerSample();

    int v_numSamples = currV.getNumSamples();
    int v_numDataPointsPerSample = currV.getNumDataPointsPerSample();

    shirley::Jacobian2d *elemIndexJacobian_ptr =
        m_elemIndexJacobian.data();
    std::vector<int> *elemIndexXiEtaTraversal_ptr =
        m_elemIndexXiEtaTraversal.data();

    // Work pointers
    escript::Data *baseV;
    escript::Data *changeV;
    escript::Data *newV;

    escript::Data *baseU;
    escript::Data *changeU;
    escript::Data *newU;

    const real_t *baseV_ro;
    const real_t *changeV_ro;
    real_t *newV_rw;

    const real_t *changeU_ro;
    real_t* newU_rw;

    real_t *currV_rw;
    const real_t *vCorr_ro;
    real_t *currU_rw;
    const real_t *uCorr_ro;

#define USE_DPO_INFO

#ifdef USE_DPO_INFO
    DataPointOffsetInfo dpoInfo_U = getDataPointOffsetInfo(currU);
    DataPointOffsetInfo dpoInfo_V = getDataPointOffsetInfo(currV);
    DataPointOffsetInfo dpoInfo_B = getDataPointOffsetInfo(m_B);
    DataPointOffsetInfo dpoInfo_E = getDataPointOffsetInfo(m_E);
    DataPointOffsetInfo dpoInfo_F = getDataPointOffsetInfo(m_F);
    DataPointOffsetInfo dpoInfo_M_bar_inv = getDataPointOffsetInfo(m_M_bar_inv);
    DataPointOffsetInfo dpoInfo_timeStep_mul_D_bar =
        getDataPointOffsetInfo(timeStep_mul_D_bar);
#endif

    // Heun's method
#pragma omp parallel
    for (int i1 = 0; i1 < dtSteps; ++i1) {
        // Phase 0 => predictor
        // Phase 1 => corrector
        for (int phase = 0; phase < 2; ++phase) {
            // Update v.
#pragma omp single
{
            t = m_currentTime + (i1 * m_timeStep) + (phase * m_timeStep);

            if (phase == 0) {  // Predictor phase
                baseV = &currV;
                changeV = &currV;
                newV = &vPred;

                changeU = &currU;
            }
            else {             // Corrector phase
                baseV = &currV;
                changeV = &vPred;
                newV = &vCorr;

                changeU = &uPred;
            }

            baseV_ro = &baseV->getDataRO(0.0)[0];
            changeV_ro = (changeV != baseV) ?
                &changeV->getDataRO(0.0)[0] : baseV_ro;
            newV_rw = newV->getSampleDataRW(0, 0.0);

            changeU_ro = &changeU->getDataRO(0.0)[0];
}  // #pragma omp single

#pragma omp for
            for (int el = 0; el < elements->numElements; ++el) {
                const Jacobian2d& jac = elemIndexJacobian_ptr[el];
                const double jac_inv[2][2] =
                    { { jac.m_inv_a, jac.m_inv_b },
                      { jac.m_inv_c, jac.m_inv_d } };

                const std::vector<int> &xiEtaTraversal =
                    elemIndexXiEtaTraversal_ptr[el];
                const int *xiEtaTraversal_ptr = xiEtaTraversal.data();

                const long vertexOffset_0 = elements->getVertexOffset(el, 0);

                // Precompute offsets for SEM nodes / integration points
                std::vector<long> changeU_offsets(numTotalNodesPerElem);
                long *changeU_offsets_ptr = changeU_offsets.data();
                std::vector<real_t> funcValues(numTotalNodesPerElem);
                real_t *funcValues_ptr = funcValues.data();

                for (int q = 0; q < numTotalNodesPerElem; ++q) {
                    const int stdXiEtaIndexQ = xiEtaTraversal_ptr[q];

                    const index_t nodeId = elements->Nodes[
                        vertexOffset_0 + stdXiEtaIndexQ];
                    const index_t realNodeId = targetDof[nodeId];

#ifdef USE_DPO_INFO
                    const long changeU_offset = calcDataOffset(
                        dpoInfo_U, realNodeId, 0, changeU);
#else
                    const long changeU_offset =
                        changeU->getDataOffset(realNodeId, 0);
#endif

                    changeU_offsets_ptr[q] = changeU_offset;
                    funcValues_ptr[q] = changeU_ro[changeU_offset];
                }

#ifdef USE_DPO_INFO
                const long matE_offset = calcDataOffset(dpoInfo_E, el, 0, &m_E);
                const long F_offset = calcDataOffset(dpoInfo_F, el, 0, &m_F);
#else
                const long matE_offset = m_E.getDataOffset(el, 0);
                const long F_offset = m_F.getDataOffset(el, 0);
#endif

                real_t fk_values[2][4];

                for (int k = 0; k < 2; ++k) {
                    // jHat=0, j=0
                    real_t F_val = F_ro[F_offset + matF_relIndex[k][0]];
                    real_t K_val = jac_inv[0][0];
                    fk_values[k][0] = F_val * K_val;

                    // jHat=0, j=1
                    F_val = F_ro[F_offset + matF_relIndex[k][1]];
                    K_val = jac_inv[1][0];
                    fk_values[k][1] = F_val * K_val;

                    // jHat=1, j=0
                    F_val = F_ro[F_offset + matF_relIndex[k][0]];
                    K_val = jac_inv[0][1];
                    fk_values[k][2] = F_val * K_val;

                    // jHat=1, j=1
                    F_val = F_ro[F_offset + matF_relIndex[k][1]];
                    K_val = jac_inv[1][1];
                    fk_values[k][3] = F_val * K_val;
                }

                // Computes Σ[l](δ_{kl} + h*E^{(e)}_{kl}) * V^{(e)}_{lp}
                for (int p = 0; p < numTotalNodesPerElem; ++p) {
                    const int stdXiEtaIndexP = xiEtaTraversal_ptr[p];

#ifdef USE_DPO_INFO
                    const long baseV_offset = calcDataOffset(
                        dpoInfo_V, el, stdXiEtaIndexP, baseV);
                    const long changeV_offset = (changeV != baseV) ?
                        calcDataOffset(dpoInfo_V, el, stdXiEtaIndexP, changeV) :
                        baseV_offset;

                    const long newV_offset = calcDataOffset(
                        dpoInfo_V, el, stdXiEtaIndexP, newV);
#else
                    const long baseV_offset = baseV->getDataOffset(
                        el, stdXiEtaIndexP);
                    const long changeV_offset = (changeV != baseV) ?
                        changeV->getDataOffset(el, stdXiEtaIndexP) :
                        baseV_offset;

                    const long newV_offset = newV->getDataOffset(el, stdXiEtaIndexP);
#endif

                    // k=0, l=0
                    double kronecker = kronecker_2d[0][0];  // δ_{k,l}
                    double hE_value = m_timeStep * matE_ro[
                        matE_offset + matE_relIndex[0][0]];  // h*E^{(e)}_{k,l}
                    double aa = (kronecker * baseV_ro[baseV_offset + 0])
                        + (hE_value * changeV_ro[changeV_offset + 0]);

                    // k=0, l=1
                    kronecker = kronecker_2d[0][1];  // δ_{k,l}
                    hE_value = m_timeStep * matE_ro[
                        matE_offset + matE_relIndex[0][1]];  // h*E^{(e)}_{k,l}
                    aa += (kronecker * baseV_ro[baseV_offset + 1])
                        + (hE_value * changeV_ro[changeV_offset + 1]);

                    newV_rw[newV_offset + 0] = aa;  // End k=0

                    // k=1, l=0
                    kronecker = kronecker_2d[1][0];  // δ_{k,l}
                    hE_value = m_timeStep * matE_ro[
                        matE_offset + matE_relIndex[1][0]];  // h*E^{(e)}_{k,l}
                    aa = (kronecker * baseV_ro[baseV_offset + 0])
                        + (hE_value * changeV_ro[changeV_offset + 0]);

                    // k=1, l=1
                    kronecker = kronecker_2d[1][1];  // δ_{k,l}
                    hE_value = m_timeStep * matE_ro[
                        matE_offset + matE_relIndex[1][1]];  // h*E^{(e)}_{k,l}
                    aa += (kronecker * baseV_ro[baseV_offset + 1])
                        + (hE_value * changeV_ro[changeV_offset + 1]);

                    newV_rw[newV_offset + 1] = aa;  // End k=1
                }  // for int p = ...

                // Computes h*F^{(e)}_{kj}*K^{(e)}_{jĵ}*\hat{N}_{q,ĵ}*U_{μ(q,e)}
                for (int k = 0; k < 2; ++k) {
                    // Traverse SEM nodes
                    for (int p = 0; p < numTotalNodesPerElem; ++p) {
                        const int stdXiEtaIndexP = xiEtaTraversal_ptr[p];

#ifdef USE_DPO_INFO
                        const long newV_offset = calcDataOffset(
                            dpoInfo_V, el, stdXiEtaIndexP, newV);
#else
                        const long newV_offset = newV->getDataOffset(
                            el, stdXiEtaIndexP);
#endif

                        const rawNodeDerivatives2d rawNodeDerivs =
                            rawCanonNodeDerivatives_ptr[p];
                        const double *dpsi_dxi = rawNodeDerivs.dpsi_dxi;
                        const double *dpsi_deta = rawNodeDerivs.dpsi_deta;

                        // Traverse integration points
                        for (int q = 0; q < numTotalNodesPerElem; ++q) {
                            const real_t funcVal = funcValues_ptr[q];

                            const double g_qpjHat[2] = { dpsi_dxi[q], dpsi_deta[q] };

                            // jHat=0
                            double g_qpjHat_val = g_qpjHat[0];
                            double derivative = g_qpjHat_val * funcVal;

                            // jHat=0, j=0
                            real_t bb = fk_values[k][0] * derivative;

                            // jHat=0, j=1
                            bb += fk_values[k][1] * derivative;

                            // jHat=1
                            g_qpjHat_val = g_qpjHat[1];
                            derivative = g_qpjHat_val * funcVal;

                            // jHat=1, j=0
                            bb += fk_values[k][2] * derivative;

                            // jHat=1, j=1
                            bb += fk_values[k][3] * derivative;

                            // Accumulate <h*bb> to V^{(e)}_{kp}
                            newV_rw[newV_offset + k] += (m_timeStep * bb);
                        }  // for int q = ...
                    }  // for int p = ...
                }  // for int k = ...
            }  // for (int el = ...

#pragma omp single
{
            // Update u.
            // First part: (1.0 + h*(\bar{M}_{ν}^-1*\bar{D}_{ν})) * U_{ν}
            if (phase == 0) {  // Predictor phase
                baseU = &currU;
                changeU = &currU;
                newU = &uPred;
            }
            else {             // Corrector phase
                baseU = &currU;
                changeU = &uPred;
                newU = &uCorr;
            }

            newU->copy(*baseU);  // Compute 1.0 * U_{ν}
            // ATTENTION; The line below crashes under OpenMP, as it
            //  calls Data::exclusiveWrite() and that function cannot
            //  be called within a multi-threaded section. The
            //  remaining factors of the computation are performed
            //  a little further below
            //*newU += (timeStep_mul_D_bar * *changeU);

            newU_rw = newU->getSampleDataRW(0, 0.0);
}  // #pragma omp single

            // Add h*(\bar{M}_{ν}^-1*\bar{D}_{ν}) * U_{ν} to the previous result
#pragma omp for collapse(2)
            for (int sample = 0; sample < u_numSamples; ++sample) {
                for (int dataPoint = 0;
                     dataPoint < u_numDataPointsPerSample; ++dataPoint) {
#ifdef USE_DPO_INFO
                    long newU_offset = calcDataOffset(
                        dpoInfo_U, sample, dataPoint, newU);
                    long ts_mul_D_bar_offset = calcDataOffset(
                        dpoInfo_timeStep_mul_D_bar, sample, dataPoint,
                        &timeStep_mul_D_bar);
                    long changeU_offset = calcDataOffset(
                        dpoInfo_U, sample, dataPoint, changeU);
#else
                    long newU_offset = newU->getDataOffset(sample, dataPoint);
                    long ts_mul_D_bar_offset =
                        timeStep_mul_D_bar.getDataOffset(sample, dataPoint);
                    long changeU_offset = changeU->getDataOffset(sample, dataPoint);
#endif

                    newU_rw[newU_offset] += (ts_mul_D_bar_ro[ts_mul_D_bar_offset]
                                             * changeU_ro[changeU_offset]);
                }
            }

            // Add h*(\bar{M}_{ν}^-1*R_{ν}) to the previous result
            for (index_t color = elements->minColor;
                 color <= elements->maxColor; ++color) {
#pragma omp for schedule(runtime)
                for (int el = 0; el < elements->numElements; ++el) {
                    if (elements->Color[el] != color) {
                        continue;
                    }

                    const Jacobian2d& jac = elemIndexJacobian_ptr[el];
                    const double jac_inv[2][2] =
                        { { jac.m_inv_a, jac.m_inv_b },
                          { jac.m_inv_c, jac.m_inv_d } };
                    const double jac_det = jac.m_determinant;

                    const std::vector<int> &xiEtaTraversal =
                        elemIndexXiEtaTraversal_ptr[el];
                    const int *xiEtaTraversal_ptr = xiEtaTraversal.data();

                    const long vertexOffset_0 = elements->getVertexOffset(el, 0);

                    // Precompute offsets for SEM nodes / integration points
                    std::vector<long> changeV_offsets(numTotalNodesPerElem);
                    long *changeV_offsets_ptr = changeV_offsets.data();

                    for (int p = 0; p < numTotalNodesPerElem; ++p) {
                        const int stdXiEtaIndexP = xiEtaTraversal_ptr[p];
#ifdef USE_DPO_INFO
                        changeV_offsets_ptr[stdXiEtaIndexP] =
                            calcDataOffset(dpoInfo_V, el, stdXiEtaIndexP, changeV);
#else
                        changeV_offsets_ptr[stdXiEtaIndexP] =
                            changeV->getDataOffset(el, stdXiEtaIndexP);
#endif
                    }

#ifdef USE_DPO_INFO
                    const long B_offset = calcDataOffset(dpoInfo_B, el, 0, &m_B);
#else
                    const long B_offset = m_B.getDataOffset(el, 0);
#endif

                    real_t bk_values[2][4];

                    for (int k = 0; k < 2; ++k) {
                        // jHat=0, j=0
                        real_t B_val = B_ro[B_offset + matB_relIndex[k][0]];
                        real_t K_val = jac_inv[0][0];
                        bk_values[k][0] = B_val * K_val;

                        // jHat=0, j=1
                        B_val = B_ro[B_offset + matB_relIndex[k][1]];
                        K_val = jac_inv[1][0];
                        bk_values[k][1] = B_val * K_val;

                        // jHat=1, j=0
                        B_val = B_ro[B_offset + matB_relIndex[k][0]];
                        K_val = jac_inv[0][1];
                        bk_values[k][2] = B_val * K_val;

                        // jHat=1, j=1
                        B_val = B_ro[B_offset + matB_relIndex[k][1]];
                        K_val = jac_inv[1][1];
                        bk_values[k][3] = B_val * K_val;
                    }

                    // Traverse SEM nodes
                    for (int p = 0; p < numTotalNodesPerElem; ++p) {
                        const int stdXiEtaIndexP = xiEtaTraversal_ptr[p];

                        const index_t nodeId = elements->Nodes[
                            vertexOffset_0 + stdXiEtaIndexP];
                        const index_t realNodeId = targetDof[nodeId];

#ifdef USE_DPO_INFO
                        const long M_bar_inv_offset = calcDataOffset(
                            dpoInfo_M_bar_inv, realNodeId, 0, &m_M_bar_inv);
#else
                        const long M_bar_inv_offset =
                            m_M_bar_inv.getDataOffset(realNodeId, 0);
#endif

                        const double M_bar_inv_value = M_bar_inv_ro[M_bar_inv_offset];

                        const double leftFactor_part = M_bar_inv_value * jac_det;
                        double aa = 0.0;

                        // Traverse the components of V^{(e)}_{k,q}
                        for (int k = 0; k < 2; ++k) {
                            double g_qpjHat[2];

                            // Traverse integration points
                            for (int q = 0; q < numTotalNodesPerElem; ++q) {
                                const rawNodeDerivatives2d rawNodeDerivs =
                                    rawCanonNodeDerivatives_ptr[q];
                                const double *dpsi_dxi = rawNodeDerivs.dpsi_dxi;
                                const double *dpsi_deta = rawNodeDerivs.dpsi_deta;

                                const int stdXiEtaIndexQ = xiEtaTraversal_ptr[q];

                                const double integrationWeight =
                                    integrationWeights_ptr[q];

                                const double leftFactor =
                                    leftFactor_part * integrationWeight;

                                const long changeV_offset =
                                    changeV_offsets_ptr[stdXiEtaIndexQ];
                                const double funcVal =
                                    changeV_ro[changeV_offset + k];

                                g_qpjHat[0] = dpsi_dxi[p];
                                g_qpjHat[1] = dpsi_deta[p];

                                // jHat=0
                                double g_qpjHat_val = g_qpjHat[0];
                                double leftFactor_mul_deriv =
                                    leftFactor * g_qpjHat_val * funcVal;

                                // jHat=0, j=0
                                aa += bk_values[k][0] * leftFactor_mul_deriv;

                                // jHat=0, j=1
                                aa += bk_values[k][1] * leftFactor_mul_deriv;

                                // jHat=1
                                g_qpjHat_val = g_qpjHat[1];
                                leftFactor_mul_deriv =
                                    leftFactor * g_qpjHat_val * funcVal;

                                // jHat=1, j=0
                                aa += bk_values[k][2] * leftFactor_mul_deriv;

                                // jHat=1, j=1
                                aa += bk_values[k][3] * leftFactor_mul_deriv;
                            }  // for (int q = ...
                        }  // for (int k = ...

                        // Accumulate <h*aa> to U_{ν}.
#ifdef USE_DPO_INFO
                        const long newU_offset = calcDataOffset(
                            dpoInfo_U, realNodeId, 0, newU);
#else
                        const long newU_offset = newU->getDataOffset(realNodeId, 0);
#endif

                        newU_rw[newU_offset] += (timeStep_ld * aa);
                    }  // for (int p = ...
                }  // for (int el = ...
            }  // for (index_t color = ...

#pragma omp single
{
            // Third part: Add h*\bar{M}_{ν}^-1*y_{ν} to the previous result
            if (m_inputSignal != nullptr) {
                const double signalStrength =
                    m_inputSignal->getValue(t - m_signalDelay);

                const int nDirac = m_diracPoints.size();

                for (int i2 = 0; i2 < nDirac; ++i2) {
                    const index_t nodeId = m_diracPoints[i2].node;
                    const index_t realNodeId = targetDof[nodeId];
#ifdef USE_DPO_INFO
                    const long M_bar_inv_offset = calcDataOffset(
                        dpoInfo_M_bar_inv, realNodeId, 0, &m_M_bar_inv);
#else
                    const long M_bar_inv_offset = m_M_bar_inv.getDataOffset(realNodeId, 0);
#endif

                    const real_t M_bar_inv_value = M_bar_inv_ro[M_bar_inv_offset];

#ifdef USE_DPO_INFO
                    const long newU_offset = calcDataOffset(
                        dpoInfo_U, realNodeId, 0, newU);
#else
                    const long newU_offset = newU->getDataOffset(realNodeId, 0);
#endif

                    newU_rw[newU_offset] += (m_timeStep
                        * M_bar_inv_value * signalStrength);
                }
            }
}  // #pragma omp single
        }  // for (int phase = ...

#pragma omp single
{
        // Combine predictor and corrector values.
        // Start by copying predictor values
        currV.copy(vPred);

        currV_rw = currV.getSampleDataRW(0, 0.0);
        vCorr_ro = &vCorr.getDataRO(0.0)[0];

        currU.copy(uPred);

        currU_rw = currU.getSampleDataRW(0, 0.0);
        uCorr_ro = &uCorr.getDataRO(0.0)[0];
}  // #pragma omp single

        // v: add corrector values and compute averages
#pragma omp for collapse(2)
        for (int sample = 0; sample < v_numSamples; ++sample) {
            for (int dataPoint = 0;
                 dataPoint < v_numDataPointsPerSample; ++dataPoint) {
#ifdef USE_DPO_INFO
                long currV_offset = calcDataOffset(
                    dpoInfo_V, sample, dataPoint, &currV);
                long vCorr_offset = calcDataOffset(
                    dpoInfo_V, sample, dataPoint, &vCorr);
#else
                long currV_offset = currV.getDataOffset(sample, dataPoint);
                long vCorr_offset = vCorr.getDataOffset(sample, dataPoint);
#endif

                currV_rw[currV_offset + 0] = 0.5 *
                    (currV_rw[currV_offset + 0] + vCorr_ro[vCorr_offset + 0]);
                currV_rw[currV_offset + 1] = 0.5 *
                    (currV_rw[currV_offset + 1] + vCorr_ro[vCorr_offset + 1]);
            }
        }

        // u: add corrector values and compute averages
#pragma omp for collapse(2)
        for (int sample = 0; sample < u_numSamples; ++sample) {
            for (int dataPoint = 0;
                 dataPoint < u_numDataPointsPerSample; ++dataPoint) {
#ifdef USE_DPO_INFO
                long currU_offset = calcDataOffset(
                    dpoInfo_U, sample, dataPoint, &currU);
                long uCorr_offset = calcDataOffset(
                    dpoInfo_U, sample, dataPoint, &uCorr);
#else
                long currU_offset = currU.getDataOffset(sample, dataPoint);
                long uCorr_offset = uCorr.getDataOffset(sample, dataPoint);
#endif

                currU_rw[currU_offset] = 0.5 *
                    (currU_rw[currU_offset] + uCorr_ro[uCorr_offset]);
            }
        }
    }  // for (int i1 = ...

    // End
    m_currentTime = t;
    m_u.copy(currU);
    m_v.copy(currV);
}

void ShirleyDomain::progressInSteps(int steps)
{
    if (steps < 1) {
        std::stringstream ss;
        ss << "ShirleyDomain::progressInSteps(): number of steps ("
           << steps << ") must be greater than zero";
        throw ShirleyException(ss.str());
    }

    double finalTime = m_currentTime + (m_timeStep * steps);
    progressToTime(finalTime);
}


}  // namespace shirley
