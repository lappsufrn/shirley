
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#include <shirley/Assemble.h>
#include <shirley/Util.h>

#include <escript/index.h>

#include <sstream>

namespace shirley {

/// Copies spatial coordinates of all nodes, in a node-by-node basis,
/// into expanded Data object <dest>
void Assemble_NodeCoordinates(const NodeFile* nodes, escript::Data& dest)
{
    if (nodes == nullptr) {
        return;
    }

    if (dest.isComplex()) {
        throw escript::ValueError(
            "shirley::Assemble_NodeCoordinates(): "
            "can't copy node coordinates into a Complex Data object");
    }

    if (!dest.numSamplesEqual(1, nodes->getNumNodes())) {
        throw escript::ValueError(
            "shirley::Assemble_NodeCoordinates(): "
            "<dest> Data object contains an illegal number of samples");
    }

    if (dest.getFunctionSpace().getTypeCode() != Nodes) {
        throw escript::ValueError(
            "shirley::Assemble_NodeCoordinates(): "
            "<dest> Data object is not defined on the function space of Nodes");
    }

    if (!dest.actsExpanded()) {
        throw escript::ValueError(
            "shirley::Assemble_NodeCoordinates(): "
            "expanded Data object expected in <dest>");
    }

    const escript::DataTypes::ShapeType expectedShape(1, nodes->numDim);

    if (dest.getDataPointShape() != expectedShape) {
        std::stringstream ss;
        ss << "shirley::Assemble_NodeCoordinates(): "
              "Data object of shape (" << nodes->numDim
           << ",) expected in <dest>";
        throw escript::ValueError(ss.str());
    }

    // Actual copy
    const double dummy = 0.0;
    const size_t dim_size = nodes->numDim * sizeof(double);

    dest.requireWrite();

#pragma omp parallel for
    for (dim_t n = 0; n < nodes->getNumNodes(); ++n) {
        memcpy(dest.getSampleDataRW(n, dummy),
               &nodes->Coordinates[INDEX2(0, n, nodes->numDim)], dim_size);
    }
}

} // namespace shirley
