
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#include <shirley/ElementFile.h>
#include <shirley/ShapeTable.h>

#include <escript/index.h>

namespace shirley {

static int arithmeticProgressionSum(int a_1, int a_n, int n) {
    return (n * (a_1 + a_n)) / 2;
}

ElementFile::ElementFile(ElementTypeId elementTypeId, escript::JMPI mpiInfo,
                         int order) :
    mpiInfo(mpiInfo),
    numElements(0),
    Id(nullptr),
    Tag(nullptr),
    Owner(nullptr),
    Nodes(nullptr),
    Color(nullptr),
    minColor(0),
    maxColor(-1),
    elemTypeId(elementTypeId),
    m_order(order)
{
    if ((m_order < 3) || (m_order > 10)) {
        throw ShirleyException(
            "ElementFile::ctor: element order must be >= 3 and <= 10");
    }

    // Previously, it was simply assumed that <m_numVerticesPerElem> should
    // be <numDim + 1> and <numShapes> should be <numLocalDim + 1>. Now
    // arrays are used instead.
    numDim = Dims[elementTypeId];
    m_numVerticesPerElem = NumVerticesElemTypeId[elementTypeId];
    numLocalDim = LocalDims[elementTypeId];
    numShapes = NumShapesElemTypeId[elementTypeId];
    elemTypeName = getElementTypeName(elementTypeId);

    // Defines how many SEM nodes we will need to work with GLL quadrature
    // points
    switch (elementTypeId) {
        case Shirley_Tri3:
            // See: https://doi.org/10.1093/imamat/hxh077
            m_numEdgesPerElem = 3;
            m_numInnerNodesPerEdge = m_order - 1;
            m_numFacesPerElem = 1;
            m_numInnerNodesPerFace = (m_order <= 2) ? 0 :
                arithmeticProgressionSum(
                    1, m_order - 2, m_order - 2);
            m_numInteriorNodesPerElem = 0;
            break;

        case Shirley_Tet4:
            // See: https://doi.org/10.1093/imamat/hxh111
            m_numEdgesPerElem = 6;
            m_numInnerNodesPerEdge = m_order - 1;
                m_numFacesPerElem = 4;
            m_numInnerNodesPerFace = (m_order <= 2) ? 0 :
                arithmeticProgressionSum(
                    1, m_order - 2, m_order - 2);
            m_numInteriorNodesPerElem = (m_order < 4) ? 0 :
                ((m_order - 3) * (m_order - 2) *
                 (m_order - 1) / 6);
            break;

        default:
            m_numEdgesPerElem = 0;
            m_numInnerNodesPerEdge = 0;
            m_numFacesPerElem = 0;
            m_numInnerNodesPerFace = 0;
            m_numInteriorNodesPerElem = 0;
            break;
    }

    m_numTotalNodesPerElem = calcNumTotalNodesPerElem();
}

ElementFile::~ElementFile()
{
    freeTable();
}

void ElementFile::allocTable(dim_t new_numElements)
{
    if (numElements > 0) {
        freeTable();
    }

    numElements = new_numElements;

    const int numTotalNodesPerElem = getNumTotalNodesPerElem();

    Id = new index_t[numElements];
    Tag = new int[numElements];
    Owner = new int[numElements];
    Nodes = new index_t[numElements * numTotalNodesPerElem];
    Color = new index_t[numElements];

#pragma omp parallel for
    for (index_t e = 0; e < numElements; e++) {
        for (int i = 0; i < numTotalNodesPerElem; i++) {
            Nodes[INDEX2(i, e, numTotalNodesPerElem)] = -1;
        }

        Id[e] = -1;
        Tag[e] = -1;
        Owner[e] = -1;
        Color[e] = -1;
    }

    maxColor = -1;
    minColor = 0;
}

void ElementFile::freeTable()
{
    delete[] Id;
    delete[] Tag;
    delete[] Owner;
    delete[] Nodes;
    delete[] Color;

    tagsInUse.clear();

    numElements = 0;
    maxColor = -1;
    minColor = 0;
}

void ElementFile::copyTable(index_t offset, index_t nodeOffset,
                            index_t idOffset, const ElementFile* in)
{
    const int numTotalNodesPerElem_in = in->getNumTotalNodesPerElem();
    const int numTotalNodesPerElem = getNumTotalNodesPerElem();

    if (numTotalNodesPerElem_in > numTotalNodesPerElem) {
        throw ShirleyException(
            "ElementFile::copyTable(): dimensions of element files don't match");
    }

    if (mpiInfo->comm != in->mpiInfo->comm) {
        throw ShirleyException(
            "ElementFile::copyTable(): MPI communicators of element files don't match");
    }

#pragma omp parallel for
    for (index_t e = 0; e < in->numElements; e++) {
        Id[offset + e] = in->Id[e] + idOffset;
        Tag[offset + e] = in->Tag[e];
        Owner[offset + e] = in->Owner[e];

        for (int n = 0; n < numTotalNodesPerElem; n++) {
            Nodes[INDEX2(n, offset + e, numTotalNodesPerElem)] =
                in->Nodes[INDEX2(n, e, numTotalNodesPerElem_in)] + nodeOffset;
        }
    }
}

void ElementFile::print(const index_t* nodesId) const
{
    std::cout << "=== " << elemTypeName << ":\n"
              << "number of elements=" << numElements << "\n"
              << "color range=[" << minColor << "," << maxColor
              << "]\n";

    if (numElements > 0) {
        std::cout << "Id,Tag,Owner,Color,NodesPerElem" << std::endl;

        const int numTotalNodesPerElem = getNumTotalNodesPerElem();

        for (index_t e = 0; e < numElements; e++) {
            std::cout << Id[e] << ","
                      << Tag[e] << ","
                      << Owner[e] << ","
                      << Color[e] << ",";

            for (int n = 0; n < numTotalNodesPerElem; n++) {
                std::cout << " " << nodesId[Nodes[INDEX2(n, e, numTotalNodesPerElem)]];
            }

            std::cout << std::endl;
        }
    }
}

void ElementFile::gather(const index_t* index, const ElementFile* in)
{
    const int numTotalNodesPerElem_in = in->getNumTotalNodesPerElem();
    const int numTotalNodesPerElem = getNumTotalNodesPerElem();

#pragma omp parallel for
    for (index_t e = 0; e < numElements; e++) {
        const index_t k = index[e];

        Id[e] = in->Id[k];
        Tag[e] = in->Tag[k];
        Owner[e] = in->Owner[k];
        Color[e] = in->Color[k] + maxColor + 1;

        for (int n = 0; n < std::min(numTotalNodesPerElem, numTotalNodesPerElem_in); n++) {
            Nodes[INDEX2(n, e, numTotalNodesPerElem)] =
                in->Nodes[INDEX2(n, k, numTotalNodesPerElem_in)];
        }
    }

    minColor = std::min(minColor, in->minColor + maxColor + 1);
    maxColor = std::max(maxColor, in->maxColor + maxColor + 1);
}

void ElementFile::swapTable(ElementFile* other)
{
    std::swap(numElements, other->numElements);
    std::swap(Id, other->Id);
    std::swap(Tag, other->Tag);
    std::swap(Owner, other->Owner);
    std::swap(Nodes, other->Nodes);
    std::swap(Color, other->Color);
    std::swap(minColor, other->minColor);
    std::swap(maxColor, other->maxColor);
    std::swap(tagsInUse, other->tagsInUse);
    std::swap(m_order, other->m_order);
}

void ElementFile::optimizeOrdering()
{
    if (numElements < 1) {
        return;
    }

    throw ShirleyException(
        "ElementFile::optimizeOrdering(): "
        "this member function was initially copied from Dudley, and is "
        "completely broken for Shirley semantics! Do *NOT* use it!");

    util::ValueAndIndexList itemList(static_cast<unsigned long>(numElements));
    IndexVector index(static_cast<unsigned long>(numElements));
    ElementFile* out = new ElementFile(elemTypeId, mpiInfo, m_order);
    out->allocTable(numElements);

    const int numTotalNodesPerElem = getNumTotalNodesPerElem();

#pragma omp parallel for
    for (index_t e = 0; e < numElements; e++) {
        IndexPair entry(Nodes[INDEX2(0, e, numTotalNodesPerElem)], e);

        for (int n = 1; n < numTotalNodesPerElem; n++) {
            entry.first = std::min(entry.first, Nodes[INDEX2(n, e, numTotalNodesPerElem)]);
        }

        itemList[e] = entry;
    }

    util::sortValueAndIndex(itemList);

#pragma omp parallel for
    for (index_t e = 0; e < numElements; e++) {
        index[e] = itemList[e].second;
    }

    out->gather(&index[0], this);
    swapTable(out);

    delete out;
}

void ElementFile::setTags(int newTag, const escript::Data& mask)
{
    if (mask.isComplex()) {
      throw ShirleyException(
          "ElementFile::setTags(): mask argument must not be Complex");
    }

    const int numQuad = hasReducedIntegrationOrder(mask) ?
        1 : getNumTotalNodesPerElem();

    if (1 != mask.getDataPointSize()) {
        throw ShirleyException(
            "ElementFile::setTags(): number of components of mask must be 1");
    } else if (!mask.numSamplesEqual(numQuad, numElements)) {
        throw ShirleyException(
            "ElementFile::setTags(): illegal number of samples of mask Data object");
    }

    real_t wantreal = 0;

    if (mask.actsExpanded()) {
#pragma omp parallel for
        for (index_t e = 0; e < numElements; e++) {
            if (mask.getSampleDataRO(e, wantreal)[0] > 0)
                Tag[e] = newTag;
        }
    } else {
#pragma omp parallel for
        for (index_t e = 0; e < numElements; e++) {
            const double* maskArray = mask.getSampleDataRO(e, wantreal);
            bool check = false;

            for (int q = 0; q < numQuad; q++) {
                check = check || maskArray[q];
            }

            if (check) {
                Tag[e] = newTag;
            }
        }
    }

    updateTagList();
}

void ElementFile::markNodes(IntVector& mask, index_t offset) const
{
    const int numTotalNodesPerElem = getNumTotalNodesPerElem();

#pragma omp parallel for
    for (index_t e = 0; e < numElements; e++) {
        for (int n = 0; n < numTotalNodesPerElem; n++) {
            mask[Nodes[INDEX2(n, e, numTotalNodesPerElem)] - offset] = 1;
        }
    }
}

void ElementFile::relabelNodes(const IndexVector& newNodes, index_t offset)
{
    const int numTotalNodesPerElem = getNumTotalNodesPerElem();

#pragma omp parallel for
    for (index_t e = 0; e < numElements; e++) {
        for (int n = 0; n < numTotalNodesPerElem; n++) {
            Nodes[INDEX2(n, e, numTotalNodesPerElem)] =
                newNodes[Nodes[INDEX2(n, e, numTotalNodesPerElem)] - offset];
        }
    }
}

int ElementFile::calcNumTotalNodesPerElem() const
{
    return m_numVerticesPerElem +
        (m_numEdgesPerElem * m_numInnerNodesPerEdge) +
        (m_numFacesPerElem * m_numInnerNodesPerFace) +
        m_numInteriorNodesPerElem;
}

bool ElementFile::hasSemNodes() const
{
    return ((m_numEdgesPerElem * m_numInnerNodesPerEdge) > 0) ||
        ((m_numFacesPerElem * m_numInnerNodesPerFace) > 0) ||
        (m_numInteriorNodesPerElem > 0);
}

int ElementFile::getVertexOffset(int elementNum, int vertexNum)
    const
{
    return (elementNum * getNumTotalNodesPerElem()) +
        vertexNum;
}

int ElementFile::getInnerEdgeNodeOffset(int elementNum, int edgeNum,
                                        int innerEdgeNodeNum) const
{
    return (elementNum * getNumTotalNodesPerElem()) +
        m_numVerticesPerElem +
        (edgeNum * m_numInnerNodesPerEdge) +
        innerEdgeNodeNum;
}

int ElementFile::getInnerFaceNodeOffset(int elementNum, int faceNum,
                                        int innerFaceNodeNum) const
{
    return (elementNum * getNumTotalNodesPerElem()) +
        m_numVerticesPerElem +
        (m_numEdgesPerElem * m_numInnerNodesPerEdge) +
        (faceNum * m_numInnerNodesPerFace) +
        innerFaceNodeNum;
}

int ElementFile::getInteriorNodeOffset(int elementNum,
                                       int interiorNodeNum) const
{
    return (elementNum * getNumTotalNodesPerElem()) +
        m_numVerticesPerElem +
        (m_numEdgesPerElem * m_numInnerNodesPerEdge) +
        (m_numFacesPerElem * m_numInnerNodesPerFace) +
        interiorNodeNum;
}

}  // namespace shirley
