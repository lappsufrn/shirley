
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#ifndef __SHIRLEY_JACOBIAN2D_H__
#define __SHIRLEY_JACOBIAN2D_H__


#include "ShirleyException.h"

namespace shirley {

/**
 * Jacobian2d is a simple class to hold the elements of a 2D
 * Jacobian matrix describing the change of coordinates from a general
 * triangle to a standard right triangle. The general triangle has its
 * vertices defined on the x and y axes, with x and y taking any value,
 * whereas the standard general triangle is defined on the ξ and η axes,
 * with ξ and η >= 0 and (ξ + η) <= 1.0. The determinant of the Jacobian
 * matrix and its inverse matrix are stored as well.
 */

class Jacobian2d {
public:
    // A no-parameter constructor is provided so that this class can
    // be used as a value in instances of std::map, std::unordered_map
    // and the like
    Jacobian2d() = default;

    Jacobian2d(
        double x1, double y1, double x2, double y2, double x3, double y3);

    ~Jacobian2d() = default;

public:
    // <m_a> through <m_d> store the change in (x,y) coordinates in relation
    // to the (ξ,η) coordinates. In matrix notation, this is:
    //
    // [ m_a m_b ] = [ ∂x/∂ξ ∂y/∂ξ ]
    // [ m_c m_d ]   [ ∂x/∂η ∂y/∂η ]
    //
    // We also store the determinant of the matrix above in <m_determinant>,
    // and the elements of the corresponding inverse matrix in member
    // attributes <m_inv_a> through <m_inv_d>:
    //
    // [ m_a m_b ]^-1 = [ m_inv_a m_inv_b ]
    // [ m_c m_d ]      [ m_inv_c m_inv_d ]
    //
    // The inverse matrix is useful to compute derivatives based on this
    // Jacobian matrix.

    double m_a;  // ∂x/∂ξ

    double m_b;  // ∂y/∂ξ

    double m_c;  // ∂x/∂η

    double m_d;  // ∂y/∂η

    double m_determinant;

    double m_inv_a;

    double m_inv_b;

    double m_inv_c;

    double m_inv_d;
};

}  // namespace shirley


#endif //__SHIRLEY_JACOBIAN2D_H__
