
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#ifndef __SHIRLEY_IWAVELET_H__
#define __SHIRLEY_IWAVELET_H__


namespace shirley
{

/**
 * Base class for wavelets, such as the Ricker wavelet.
 * This should be a pure abstract class, not a concrete class that actually
 * does nothing. But, if it is made to be pure abstract, Python won't be
 * able to import Shirley's shared library file. :(
 */

class IWavelet {
public:
  virtual ~IWavelet() = default;

  virtual double getValue(double t) { return 0.0; };
  virtual double getVelocity(double t) { return 0.0; };
  virtual double getAcceleration(double t) {return 0.0; };
};

}  // namespace shirley


#endif //__SHIRLEY_IWAVELET_H__
