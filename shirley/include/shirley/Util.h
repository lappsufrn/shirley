
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

/// Some utility routines

#ifndef __SHIRLEY_UTIL_H__
#define __SHIRLEY_UTIL_H__


#include <shirley/Shirley.h>

#include <escript/Data.h>

namespace shirley {
namespace util {

typedef std::pair<index_t, index_t> IndexPair;
typedef std::vector<IndexPair> ValueAndIndexList;

/// Orders a ValueAndIndexList by value.
void sortValueAndIndex(ValueAndIndexList& array);

/// Gathers values into array `out` from array `in` using `index`:
///   out(1:numData, 1:len) := in(1:numData, index(1:len))
void gather(int len, const index_t* index, int numData, const double* in,
            double* out);

/// Adds array `in` into `out` using an `index`:
///   out(1:numData,index[p])+=in(1:numData,p) where
///   p={k=1...len,index[k]<upperBound }
template<typename Scalar>
void addScatter(int len, const index_t* index, int numData,
                const Scalar* in, Scalar* out, index_t upperBound);

/// Multiplies two matrices: A(1:A1,1:A2) := B(1:A1,1:B2)*C(1:B2,1:A2)
void smallMatMult(int A1, int A2, double* A, int B2,
                  const DoubleVector& B,
                  const DoubleVector& C);

/// Multiplies a set of matrices with a single matrix:
///   A(1:A1,1:A2,i)=B(1:A1,1:B2,i)*C(1:B2,1:A2) for i=1,len
template<typename Scalar>
void smallMatSetMult1(int len, int A1, int A2, Scalar* A, int B2,
                      const std::vector<Scalar>& B,
                      const DoubleVector& C);

void invertSmallMat(int len, int dim, const double* A, double *invA,
                    double* det);

/// Returns the normalized vector normal[dim,len] orthogonal to A(:,0,q) and
/// A(:,1,q) in the case of dim=3, or the vector A(:,0,q) in the case of dim=2
void normalVector(int len, int dim, int dim1, const double* A, double* Normal);

index_t getMinInt(int dim, dim_t N, const index_t* values);

index_t getMaxInt(int dim, dim_t N, const index_t* values);

/// Calculates the minimum and maximum value from an integer array of length
/// N x dim
IndexPair getMinMaxInt(int dim, dim_t N, const index_t* values);

/// Calculates the minimum and maximum value from an integer array of length N
/// disregarding the value `ignore`
IndexPair getFlaggedMinMaxInt(dim_t N, const index_t* values, index_t ignore);

/// Extracts the positive entries in `mask` returning a contiguous vector of
/// those entries
IndexVector packMask(const IntVector& mask);

void setValuesInUse(const int* values, dim_t numValues,
                    IntVector& valuesInUse, escript::JMPI mpiInfo);

}  // namespace util
}  // namespace shirley


#endif  // __SHIRLEY_UTIL_H__
