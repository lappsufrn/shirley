
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

/****************************************************************************

  Assemblage routines: header file

*****************************************************************************/

#ifndef __SHIRLEY_ASSEMBLE_H__
#define __SHIRLEY_ASSEMBLE_H__


#include <shirley/Shirley.h>
#include <shirley/ElementFile.h>
#include <shirley/NodeFile.h>

#include <escript/AbstractSystemMatrix.h>

namespace shirley {

/// Copies spatial coordinates of all nodes, in a node-by-node basis,
/// into expanded Data object <dest>
void Assemble_NodeCoordinates(const NodeFile* nodes, escript::Data& dest);

/// Calculates the minimum distance between two vertices of elements and
/// assigns the value to each item in `dest`
void Assemble_getSize(const NodeFile* nodes, const ElementFile* elements,
                      escript::Data& dest);

}  // namespace shirley


#endif  // __SHIRLEY_ASSEMBLE_H__
