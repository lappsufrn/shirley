
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

/****************************************************************************/

/* Shirley: Converting an element list into a matrix shape     */

/****************************************************************************/

#ifndef __SHIRLEY_INDEXLIST_H__
#define __SHIRLEY_INDEXLIST_H__


#include <shirley/Shirley.h>

#include <escript/IndexList.h>

namespace shirley {

using escript::IndexList;

// Helpers to build system matrix

class ElementFile;

void IndexList_insertElements(IndexList* indexList, const ElementFile* elements,
                              const index_t* map);

void IndexList_insertElementsWithRowRangeNoMainDiagonal(IndexList* indexList,
                            index_t firstRow, index_t lastRow,
                            const ElementFile* elements, const index_t* map);

}  // namespace shirley


#endif  // __SHIRLEY_INDEXLIST_H__
