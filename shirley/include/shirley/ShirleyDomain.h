
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#ifndef __SHIRLEY_SHIRLEYDOMAIN_H__
#define __SHIRLEY_SHIRLEYDOMAIN_H__


/**
 * Shirley: Domain
 *
 * A mesh is built from nodes and elements which describe the domain,
 * surface and point sources. Currently, point sources can only take
 * the form of wavelets.
 * The nodes are stored in a NodeFile and elements in ElementFiles.
 * Shirley domains have three ElementFiles containing the elements,
 * surface and point sources, respetively. Currently, the ElementFile
 * corresponding to the surfaces is not used.
 *
 * The only allowed element type is Tri3, i.e. triangles.
 * Element sizes can be different, there unstructured meshes are supported.
 *
 * The numbering of the nodes starts with 0.
 *
 * Important: it is assumed that every node appears in at least one element or
 * surface element and that any node used in an element, surface element or as
 * a point is specified in the NodeFile, see also resolveNodeIds.
 *
 * All nodes and elements can be tagged. The tag allows to group nodes and
 * elements. A typical application is to mark surface elements on a
 * certain portion of the domain with the same tag. All these surface
 * elements can then be assigned the same value e.g. for the pressure.
 *
 * ShirleyDomain is strongly based on DudleyDomain.
 * Please see DudleyDomain.h for reference.
 */

#include <shirley/Shirley.h>
#include <shirley/ElementFile.h>
#include <shirley/NodeFile.h>
#include <shirley/Jacobian2d.h>
#include <shirley/NodeDerivatives2d.h>
#include <shirley/ElemNodeDerivatives2d.h>
#include <shirley/IWavelet.h>

#include <escript/AbstractContinuousDomain.h>
#include <escript/FunctionSpace.h>
#include <escript/FunctionSpaceFactory.h>

#ifdef ESYS_HAVE_PASO
#include <paso/SystemMatrixPattern.h>
#endif
#ifdef ESYS_HAVE_TRILINOS
#include <trilinoswrap/types.h>
#endif

#include <map>
#include <string>
#include <vector>
#include <unordered_map>

namespace shirley {

/**
   \brief
   A struct to contain the information of a Dirac point.
*/
struct DiracPoint
{
    dim_t node;
    int tag;
};

typedef std::map<std::string, int> TagMap;

// Node/element traversal strategies. These strategies are used
// at mesh loading time to relabel/reposition nodes and elements in
// memory in order to obtain better performance
const int CONNECTIVITY_TRAVERSAL        = 0;
const int DISTANCE_TRAVERSAL            = 1;
const int SPACE_FILLING_CURVE_TRAVERSAL = 2;

class ShirleyDomain : public escript::AbstractContinuousDomain
{
public:
    /**
     \brief
     Reads a gmsh mesh file.
     \param mpiInfo the MPI information structure
     \param filename the name of the gmsh file
     \param numDim spatial dimensionality (must be 2)
     \param order element order (>= 3 and <= 10)
     \param optimize whether to optimize the node labels
     \param strategy to use when traversing elements
    */
    static escript::Domain_ptr readGmsh(
        escript::JMPI mpiInfo, const std::string& filename,
        int numDim, int order, bool optimize,
        int traversalStrategy = CONNECTIVITY_TRAVERSAL);

    /**
     \brief
     Constructor for ShirleyDomain

     \param name a descriptive name for the domain
     \param numDim dimensionality of the domain (must be 2)
     \param mpiInfo shared pointer to MPI Information to be used
    */
    ShirleyDomain(const std::string& name, int numDim, escript::JMPI mpiInfo);

    /**
     \brief
     Copy constructor.
    */
    ShirleyDomain(const ShirleyDomain& in);

    /**
     \brief
     Destructor for ShirleyDomain
    */
    ~ShirleyDomain() override;

    /**
     \brief
     Returns a pointer to this domain's node file
    */
    NodeFile* getNodes() const { return m_nodes; }

    /**
     \brief
     Replaces the element file by `elements`
    */
    void setElements(ElementFile* elements);

    /**
     \brief
     Returns a pointer to this domain's element file
    */
    ElementFile* getElements() const { return m_elements; }

    /**
     \brief
     Replaces the face element file by `elements`
    */
    void setFaceElements(ElementFile* elements);

    /**
     \brief
     Returns a pointer to this domain's face element file
    */
    ElementFile* getFaceElements() const { return m_faceElements; }

    /**
     \brief
     Replaces the point element file by `elements`
    */
    void setPoints(ElementFile* elements);

    /**
     \brief
     Returns a pointer to this domain's point (nodal) element file
    */
    ElementFile* getPoints() const { return m_points; }

    /**
     \brief
     Returns a reference to the MPI information wrapper for this domain
    */
    escript::JMPI getMPI() const override { return m_mpiInfo; }

    /**
     \brief
     Returns the number of processors used for this domain
    */
    int getMPISize() const override { return m_mpiInfo->size; }

    /**
     \brief
     Returns the number MPI rank of this processor
    */
    int getMPIRank() const override { return m_mpiInfo->rank; }

    /**
     \brief
     If compiled for MPI then execute an MPI_Barrier, else do nothing
    */
    void MPIBarrier() const override;

    /**
     \brief
     Returns true if on MPI processor 0, else false
    */
    bool onMasterProcessor() const override { return getMPIRank() == 0; }

    MPI_Comm getMPIComm() const override { return m_mpiInfo->comm; }

    /**
     \brief
     Writes the current mesh to a file with the given name in the .msh file
     format.
     \param fileName Input - The name of the file to write to.
    */
    void write(const std::string& fileName) const override;

    /**
     \brief
     \param full whether to include coordinate values and id's
    */
    void Print_Mesh_Info(bool full) const override;

    /**
     \brief
     Dumps the mesh to a file with the given name.
     \param fileName Input - The name of the file
    */
    void dump(const std::string& fileName) const override;

    /**
     \brief
     Return the tag key for the given sample number.
     \param functionSpaceType Input - The function space type.
     \param sampleNo Input - The sample number.
    */
    int getTagFromSampleNo(int functionSpaceType, index_t sampleNo) const
        override;

    /**
     \brief
     Return the reference number of the given sample number.
     \param functionSpaceType Input - The function space type.
    */
    const index_t* borrowSampleReferenceIDs(int functionSpaceType) const
        override;

    /**
     \brief
     Returns true if the given integer is a valid function space type
     for this domain.
    */
    bool isValidFunctionSpaceType(int functionSpaceType) const override;

    /**
     \brief
     Return a description for this domain
    */
    std::string getDescription() const override;

    /**
     \brief
     Return a description for the given function space type code
    */
    std::string functionSpaceTypeAsString(int functionSpaceType) const override;

    /**
     \brief
     Build the table of function space type names
    */
    void setFunctionSpaceTypeNames();

    /**
     \brief
     Return a continuous FunctionSpace code
    */
    int getContinuousFunctionCode() const override;

    /**
     \brief
     Return a continuous on reduced order nodes FunctionSpace code
    */
    int getReducedContinuousFunctionCode() const override;

    /**
     \brief
     Return a function FunctionSpace code
    */
    int getFunctionCode() const override;

    /**
     \brief
     Return a function with reduced integration order FunctionSpace code
    */
    int getReducedFunctionCode() const override;

    /**
     \brief
     Return a function on boundary FunctionSpace code
    */
    int getFunctionOnBoundaryCode() const override;

    /**
     \brief
     Return a function on boundary with reduced integration order FunctionSpace code
    */
    int getReducedFunctionOnBoundaryCode() const override;

    /**
     \brief
     Return a FunctionOnContactZero code
    */
    int getFunctionOnContactZeroCode() const override;

    /**
     \brief
     Return a FunctionOnContactZero code  with reduced integration order
    */
    int getReducedFunctionOnContactZeroCode() const override;

    /**
     \brief
     Return a FunctionOnContactOne code
    */
    int getFunctionOnContactOneCode() const override;

    /**
     \brief
     Return a FunctionOnContactOne code  with reduced integration order
    */
    int getReducedFunctionOnContactOneCode() const override;

    /**
     \brief
     Return a Solution code
    */
    int getSolutionCode() const override;

    /**
     \brief
     Return a ReducedSolution code
    */
    int getReducedSolutionCode() const override;

    /**
     \brief
     Return a DiracDeltaFunctions code
    */
    int getDiracDeltaFunctionsCode() const override;

    /**
     \brief
    */
    typedef std::map<int, std::string> FunctionSpaceNamesMapType;

    /**
     \brief Returns the dimensionality of this domain
    */
    int getDim() const override { return m_nodes->numDim; }

    /**
     \brief
      Returns a status indicator of the domain. The status identifier should
      be unique over the lifetime of the object but may be updated if changes
      to the domain happen, e.g. modifications to its geometry.
    */
    StatusType getStatus() const override;

    /**
     \brief
     Return the number of data points summed across all MPI processes
    */
    dim_t getNumDataPointsGlobal() const override;

    /**
     \brief
     Return the number of data points per sample and the number of samples as a pair.
     \param functionSpaceCode Input -
    */
    std::pair<int, dim_t> getDataShape(int functionSpaceCode) const override;

    /**
     \brief
     Copies the location of data points into <dest>. The domain of <dest>
     has to match this. Has to be implemented by the actual Domain adapter.
    */
    void setToX(escript::Data& dest) const override;

    /**
     \brief
     Sets a map from a tag name to a tag key
     \param name Input - tag name.
     \param tag Input - tag key.
    */
    void setTagMap(const std::string& name, int tag) override;

    /**
     \brief
     Return the tag key for tag name.
     \param name Input - tag name
    */
    int getTag(const std::string& name) const override;

    /**
     \brief
     Returns true if name is a defined tag name.
     \param name Input - tag name to be checked.
    */
    bool isValidTagName(const std::string& name) const override;

    /**
     \brief
     Returns all tag names in a single string separated by commas
    */
    std::string showTagNames() const override;

    /**
     \brief
     Assigns new locations to the nodes of the domain
    */
    void setNewX(const escript::Data& arg) override;

    /**
     \brief
     Interpolates data given on <source> onto <target> where <source>
     and <target> have to be given on the same domain.
    */
    void interpolateOnDomain(escript::Data& target,
                             const escript::Data& source) const override;

    bool probeInterpolationOnDomain(int functionSpaceType_source,
                                    int functionSpaceType_target)
        const override;

    signed char preferredInterpolationOnDomain(int functionSpaceType_source,
                                               int functionSpaceType_target)
        const override;

    /**
    \brief Given a vector of FunctionSpace typecodes, pass back a code
           which they can all be interpolated to.
    \return true is result is valid, false if not
    */
    bool commonFunctionSpace(const IntVector& fs, int& resultCode)
        const override;

    /**
     \brief
     Interpolates data given on source onto target where source and target
     are given on different domains.
    */
    void interpolateAcross(escript::Data& target, const escript::Data& source)
        const override;

    /**
     \brief Determines whether interpolation from source to target is possible.
    */
    bool probeInterpolationAcross(int functionSpaceType_source,
                                  const escript::AbstractDomain& targetDomain,
                                  int functionSpaceType_target) const override;

    /**
     \brief
     Copies the surface normals at data points into out.
     The actual function space to be considered is defined by out.
     out has to be defined on this.
    */
    void setToNormal(escript::Data& out) const override;

    /**
     \brief
     Copies the size of samples into out.
     The actual function space to be considered is defined by out.
     out has to be defined on this.
    */
    void setToSize(escript::Data& out) const override;

    /**
     \brief
     Copies the gradient of arg into grad.
     The actual function space to be considered for the gradient is defined by grad.
     arg and grad have to be defined on this domain.
    */
    void setToGradient(escript::Data& grad, const escript::Data& arg)
        const override;

    /**
     \brief
     Copies the integrals of the function defined by arg into integrals.
     arg has to be defined on this.
    */
    void setToIntegrals(RealVector& integrals,
                        const escript::Data& arg) const override;
    void setToIntegrals(ComplexVector& integrals,
                        const escript::Data& arg) const override;

    /**
     \brief
     Return the identifier of the matrix type to be used for the global
     stiffness matrix when a particular solver, package, preconditioner,
     and symmetric matrix is used.

     \param options a SolverBuddy instance with the desired options set
    */
    int getSystemMatrixTypeId(const boost::python::object& options)
        const override;

    /**
     \brief
     Return the identifier of the transport problem type to be used when
     a particular solver, preconditioner, package and symmetric matrix is
     used.
     \param solver
     \param preconditioner
     \param package
     \param symmetry
    */
    int getTransportTypeId(int solver, int preconditioner, int package,
                           bool symmetry) const override;

    /**
     \brief
     Returns true if data on this domain and a function space of type
     functionSpaceCode is considered as being cell centered.
    */
    bool isCellOriented(int functionSpaceCode) const override;

    bool ownSample(int fsCode, index_t id) const override;

    /**
     \brief
     Adds a PDE onto the stiffness matrix mat and a rhs
    */
    void addPDEToSystem(
        escript::AbstractSystemMatrix& mat, escript::Data& rhs,
        const escript::Data& A, const escript::Data& B,
        const escript::Data& C, const escript::Data& D,
        const escript::Data& X, const escript::Data& Y,
        const escript::Data& d, const escript::Data& y,
        const escript::Data& d_contact,
        const escript::Data& y_contact,
        const escript::Data& d_dirac,
        const escript::Data& y_dirac) const override;

    /**
     \brief
     Adds a PDE onto the lumped stiffness matrix mat
    */
    void addPDEToLumpedSystem(escript::Data& mat,
                              const escript::Data& D,
                              const escript::Data& d,
                              const escript::Data& d_dirac,
                              bool useHRZ) const;

    /**
     \brief
     Adds a PDE onto the stiffness matrix mat and a rhs
    */
    void addPDEToRHS(escript::Data& rhs, const escript::Data& X,
                     const escript::Data& Y, const escript::Data& y,
                     const escript::Data& y_contact,
                     const escript::Data& y_dirac) const override;

    /**
     \brief
     Adds a PDE onto a transport problem
    */
    void addPDEToTransportProblem(
        escript::AbstractTransportProblem& tp,
        escript::Data& source, const escript::Data& M,
        const escript::Data& A, const escript::Data& B,
        const escript::Data& C, const escript::Data& D,
        const escript::Data& X, const escript::Data& Y,
        const escript::Data& d, const escript::Data& y,
        const escript::Data& d_contact,
        const escript::Data& y_contact,
        const escript::Data& d_dirac,
        const escript::Data& y_dirac) const override;

    /**
     \brief
     Creates a stiffness matrix and initializes it with zeros
    */
    escript::ASM_ptr newSystemMatrix(
        int row_blocksize,
        const escript::FunctionSpace& row_functionspace,
        int column_blocksize,
        const escript::FunctionSpace& column_functionspace,
        int systemMatrixType) const override;

    /**
     \brief
      Creates a TransportProblem
    */
    escript::ATP_ptr newTransportProblem(int blocksize,
        const escript::FunctionSpace& functionSpace, int type) const override;

    /**
     \brief Returns locations in the SEM nodes
    */
    escript::Data getX() const override;

    /**
     \brief Returns boundary normals at the quadrature point on the face
            elements
    */
    escript::Data getNormal() const override;

    /**
     \brief Returns the element size
    */
    escript::Data getSize() const override;

    /**
     \brief Comparison operators
    */
    bool operator==(const escript::AbstractDomain& other) const override;
    bool operator!=(const escript::AbstractDomain& other) const override;

    /**
     \brief Assigns new tag newTag to all samples of functionspace with a
            positive value of mask for any of its sample points.
    */
    void setTags(int functionSpaceType, int newTag,
                 const escript::Data& mask) const override;

    /**
      \brief
       Returns the number of tags in use
    */
    int getNumberOfTagsInUse(int functionSpaceCode) const override;

    /**
      \brief
       Returns a pointer to an array with the tags in use
    */
    const int* borrowListOfTagsInUse(int functionSpaceCode) const override;

    /**
     \brief Checks if this domain allows tags for the specified
            functionSpace code.
    */
    bool canTag(int functionSpaceCode) const override;

    /**
     \brief Returns the approximation order used for a function space
            functionSpaceCode
    */
    int getApproximationOrder(int functionSpaceCode) const override;

    bool supportsContactElements() const override { return false; }

    escript::Data randomFill(const escript::DataTypes::ShapeType& shape,
                             const escript::FunctionSpace& what, long seed,
                             const boost::python::tuple& filter) const override;

    /// Adds Dirac point sources to the domain
    void addPoints(const std::vector<double>& coords,
                   const std::vector<int>& tags);

    /// Clears the current input signal
    void clearInputSignal();

    /// Sets a Ricker wavelet as the current input signal. The same
    /// input signal will be generated at all Dirac point sources.
    void setRickerSignal(double f_dom, double t_dom, double signalDelay = 0.0);

    /// Sets all PDE coefficients relevant to the wave equation.
    void setPdeCoefficients(const escript::Data& M,
        const escript::Data& B, const escript::Data& D,
        const escript::Data& E, const escript::Data& F);

    /// Sets the initial solution of the wave equation.
    /// This function can be used to define wave patterns completely
    /// independent of input signals.
    void setInitialSolution(
        const escript::Data& u, const escript::Data& v, double currentTime);

    /// Gets the current value of the continuous variable u.
    escript::Data getU();

    /// Gets the current value of the discontinuous variable v.
    escript::Data getV();

    /// Gets the current time of the simulation.
    double getCurrentTime();

    /// Calculates the computation acceleration matrices for the wave
    /// equation, considering a certain time step for the time integration
    /// scheme.
    void calculateTimeIntegrationMatrices(double timeStep);

    /// Progresses the simulation until a certain point in time. This
    /// member function is useful to avoid successive calls that only
    /// advance the simulation by a short amount of time.
    void progressToTime(double finalTime);

    /// Progresses the simulation by a certain number of time steps.
    /// This method is an alternative to progressToTime().
    void progressInSteps(int steps);

    /// Computes the Jacobian matrices for all elements in this domain.
    void fillJacobians();

    /// Computes the canonical node derivatives (i.e. partial derivatives
    /// of the basis functions) at the (ξ,η) interpolation points of the
    /// "standard" triangle.
    void fillCanonicalNodeDerivatives();

    /// Computes the (ξ,η) coordinates for all SEM nodes of all elements
    /// in this domain.
    void fillXiEtaCoordinates();

    /// Computes the SEM node traversal order for all elements of this
    /// domain, as to comply with the canonical order for (ξ,η) coordinates
    /// dictated by the interpolation points of the "standard" triangle.
    void fillXiEtaTraversal();

protected:
    /// Interpolate values from the Function function space contained in
    /// <source> to values in the Solution function space, storing them
    /// in <target>
    void interpolateFromFunctionToSolution(
        escript::Data& target, const escript::Data& source) const;

    /// Given function values contained in <arg> defined at all SEM nodes
    /// of all elements, compute their partial derivatives and store the
    /// results in <grad>
    void calc2DGradientFromElements(
        escript::Data& grad, const escript::Data& arg,
        ElementFile *elements) const;

    /// Given function values contained in <arg> defined at all global
    /// nodes, compute their partial derivatives and store the results
    /// in <grad>
    void calc2DGradientFromDegreesOfFreedom(
        escript::Data& grad, const escript::Data& arg,
        ElementFile *elements) const;

    /// Dirac point sources of the domain
    std::vector<DiracPoint> m_diracPoints;

    /// Node IDs of the Dirac point sources
    IndexVector m_diracPointNodeIDs;  // For addPoints()

    /// Wavelet that represents the current input signal
    /// (please see clearInputSignal() and setRickerSignal())
    IWavelet *m_inputSignal;

    /// Time delay to apply when calling m_inputSignal->getValue()
    double m_signalDelay;

    /// Values of continuous variable u
    /// (please see setInitialSolution(), getU() and progressToTime()).
    escript::Data m_u;

    /// Values of discontinuous variable u
    /// (please see setInitialSolution(), getV() and progressToTime()).
    escript::Data m_v;

    /// Size of the time step to use in simulations.
    /// Defined via calculateTimeIntegrationMatrices().
    double m_timeStep;

    /// Time of the current simulation.
    double m_currentTime;

    /// Acceleration matrix expressed by the summation of
    /// M^{(e)}.J^{(e)}.ŵ_q, defined at each global node
    /// (please see calculateTimeIntegrationMatrices()).
    escript::Data m_M_bar;

    /// Acceleration matrix expressed by 1.0 divided by the summation of
    /// M^{(e)}.J^{(e)}.ŵ_q, defined at each global node (please see
    /// calculateTimeIntegrationMatrices()).
    escript::Data m_M_bar_inv;

    /// Acceleration matrix expressed by the summation of
    /// D^{(e)}.J^{(e)}.ŵ_q divided by the summation of M^{(e)}.J^{(e)}.ŵ_q,
    /// defined at each global node (please see
    /// calculateTimeIntegrationMatrices()).
    escript::Data m_D_bar;

    /// This vector associates an element index to its Jacobian matrix.
    /// The determinant and the inverse of the Jacobian matrix come
    /// as an added bonus.
    std::vector<shirley::Jacobian2d> m_elemIndexJacobian;

    /// Stores the partial derivatives of our chosen basis function (an
    /// Appell polynomial) over each of the SEM nodes of an element (any
    /// element, actually; more on this below).  Each SEM node of an
    /// element corresponds to an (ξ,η) ordered pair of coordinates over
    /// the "standard" triangle. The canonical order of (ξ,η) pairs, and
    /// therefore that of the SEM nodes, is described by Eq. 1.13, pg.
    /// 3 of doi:10.1093/imamat/hxh111 (it is also described by Eq. 3.1,
    /// pg. 11 of doi:10.1093/imamat/hxh077, but unfortunately this
    /// second definition has typographical errors); see
    /// LobattoPolynomials::fillXiAndEtaArraysTri3() for reference.
    /// The partial derivatives are computed with respect to the ξ and η
    /// directions. Each SEM node unfolds into a number of integration
    /// points, and the derivatives are computed based on the (ξ,η)
    /// coordinates of the SEM node and the index of each integration
    /// point. When combined with the inverse of the Jacobian matrix
    /// of an element and the values of a function over the SEM nodes
    /// of that element, the derivatives over the (ξ,η) directions can
    /// be used to compute the derivatives of the function over the
    /// (x,y) directions for that element.
    /// This vector is populated in fillCanonicalNodeDerivatives().
    /// ATTENTION! Since the canonical order of (ξ,η) pairs does not
    /// depend on the element in any way, this vector can be used in
    /// conjunction with ANY element.
    std::vector<NodeDerivatives2d> m_canonNodeDerivatives;

    /// Stores the (ξ,η) coordinates of the SEM nodes of all elements
    /// in this domain. Each element contains a vector of double numbers,
    /// stored in pairs. Items 0 and 1 correspond to the (ξ,η) coordinates
    /// of the first SEM node, items 2 and 3 correspond to the (ξ,η)
    /// coordinates of the second SEM node, and so on.
    /// This vector of vectors is filled in fillXiEtaCoordinates().
    std::vector<std::vector<real_t>> m_elemIndexXiEtaCoords;

    /// This vector associates an element index to the canonical order at
    /// which the SEM nodes of the element should be traversed if it
    /// were converted to a "standard" triangle, as described by Eq.
    /// 1.13, pg. 3 of doi:10.1093/imamat/hxh111 (it is also described
    /// by Eq. 3.1, pg. 11 of doi:10.1093/imamat/hxh077, but unfortunately
    /// this second definition has typographical errors); see
    /// LobattoPolynomials::fillXiAndEtaArraysTri3() for reference.
    /// The node IDs of the SEM nodes of an element are given by
    /// m_elements->Nodes[m_elements->getVertexOffset(el, semNodeIndex)],
    /// and the (x,y) coordinates of each SEM node are given by
    /// m_nodes->Coordinates[INDEX2({0,1}, nodeId, m_nodes->numDim)].
    /// The (x,y) coordinates of a SEM node, which correspond to a
    /// "general" triangle, can be converted to the (ξ,η) coordinates
    /// corresponding to a "standard" triangle (see
    /// LobattoPolynomials::calcXiFromGeneralToStandardTriangle() and
    /// LobattoPolynomials::calcEtaFromGeneralToStandardTriangle() for
    /// reference). For example, given an element with 10 SEM nodes,
    /// instead of traversing the nodes in the intuitive index order
    /// of (0, 1, 2, 3, 4, 5, 6, 7, 8, 9), the traversal could be done
    /// in a canonical manner using the order (5, 8, 2, 1, 7, 0, 4, 9, 6, 3),
    /// which should be stored on this vector for that specific element.
    /// This vector of vectors is filled in fillXiEtaTraversal().
    std::vector<std::vector<int>> m_elemIndexXiEtaTraversal;

private:
    void prepare(bool optimize);

    /// Initially the element nodes refer to the numbering defined by the
    /// global id assigned to the nodes in the NodeFile. It is also not ensured
    /// that all nodes referred to by an element are actually available on the
    /// process. At the output, a local node labeling is used and all nodes are
    /// available. In particular the numbering of the element nodes is between
    /// 0 and Nodes->numNodes.
    /// The function does not create a distribution of the degrees of freedom.
    void resolveNodeIds();

    void createColoring(const index_t* node_localDOF_map);

    void distributeByRankOfDOF(const IndexVector& distribution);

    void markNodes(IntVector& mask, index_t offset) const;

    void optimizeDOFDistribution(IndexVector& distribution);

    void optimizeDOFLabeling(const IndexVector& distribution);

    void optimizeElementOrdering();

    void updateTagList();

    void createMappings(const IndexVector& dofDistribution,
                        const IndexVector& nodeDistribution);

    /// Assigns new node reference numbers to all element files.
    /// If k is the old node, the new node is newNodes[k-offset].
    void relabelElementNodes(const IndexVector& newNodes,
                             index_t offset);

    template<typename Scalar>
    void setToIntegralsWorker(std::vector<Scalar>& integrals,
                              const escript::Data& arg) const;

    /// finds the node that the given point coordinates belong to
    dim_t findNode(const double *coords) const;

    /// MPI information
    escript::JMPI m_mpiInfo;

    /// Domain description
    std::string m_name;

    /// Table of nodes
    NodeFile* m_nodes;

    /// Table of elements
    ElementFile* m_elements;

    /// Table of face elements
    ElementFile* m_faceElements;

    /// Table of points (treated as elements of dimension 0)
    ElementFile* m_points;

    /// The tag map mapping names to tag keys
    TagMap m_tagMap;

    /// Number of unique edges between vertices of elements, face elements
    /// and points. Edges between SEM nodes should *NOT* be accounted for!
    int m_numUniqueEdges;

    /// Number of unique faces present at elements, face elements and points.
    int m_numUniqueFaces;

    /// PDE coefficient.
    /// M should be a Scalar() defined in the Function() function space.
    escript::Data m_M;

    /// PDE coefficient.
    /// B should be a Tensor() defined in the Function() function space
    /// (most likely a non-zero constant multiplied by kronecker(2)).
    escript::Data m_B;

    /// PDE coefficient.
    /// D should be a Scalar() defined in the Function() function space,
    /// or be left empty.
    escript::Data m_D;

    /// PDE coefficient.
    /// E should be a Tensor() defined in the Function() function space,
    /// or be left empty.
    escript::Data m_E;

    /// PDE coefficient.
    /// F should be a Tensor() defined in the Function() function space
    /// (most likely a non-zero constant multiplied by kronecker(2)).
    escript::Data m_F;

    static FunctionSpaceNamesMapType m_functionSpaceTypeNames;
};

}  // namespace shirley


#endif  // __SHIRLEY_SHIRLEYDOMAIN_H__
