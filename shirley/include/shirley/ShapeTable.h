
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

/* Shape Function info
These tables are a much simplified version of content from finley's ShapeFunctions files

This file is not to be included in .h files - only .cpp files should have any use for it
*/

#ifndef __SHIRLEY_SHAPETABLE_H__
#define __SHIRLEY_SHAPETABLE_H__


#include <shirley/Shirley.h>
#include <shirley/ElementType.h>

namespace shirley {

// Index the following by ElementTypeId
// The number of local dimensions (as opposed to dimension of the embedding
// space)
static const int LocalDims[8] =
    { 0, 1, 2, 3,
      0, 1, 2, 0 };
static const int Dims[8] =
    { 0, 1, 2, 3,
      1, 2, 3, 0 };

static const int NumVerticesElemTypeId[8] = {
    1, 2, 3, 4,
    2, 3, 4, 0 };
static const int NumShapesElemTypeId[8] = {
    1, 2, 3, 4,
    1, 2, 3, 0 };

const char* getElementTypeName(ElementTypeId elemTypeId);

}  // namespace shirley


#endif  // __SHIRLEY_SHAPETABLE_H__
