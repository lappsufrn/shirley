
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

/*
  NodeMapping provides a mapping from the local nodes typically to the
  degrees of freedom, the reduced degrees of freedom or the reduced node set.
*/

#ifndef __SHIRLEY_NODEMAPPING_H__
#define __SHIRLEY_NODEMAPPING_H__


#include <shirley/Util.h>

namespace shirley {

/// NodeMapping provides a mapping from the local nodes typically to the
/// degrees of freedom, the reduced degrees of freedom or the reduced node set
struct NodeMapping
{
    NodeMapping() : numNodes(0), target(nullptr), numTargets(0), map(nullptr) { }

    /// Resets both map and target
    void clear()
    {
        delete[] target;
        delete[] map;

        numNodes = 0;
        target = nullptr;
        numTargets = 0;
        map = nullptr;
    }

    /// Initializes a node mapping. The target array is copied and a reverse
    /// map created.
    /// newTarget[i]=unused means that no target is defined for SEM node i.
    void assign(const index_t* newTarget, dim_t new_numNodes, index_t unused) {
        clear();

        if (new_numNodes == 0)
            return;

        numNodes = new_numNodes;

        IndexPair range(util::getFlaggedMinMaxInt(numNodes, newTarget, unused));

        if (range.first < 0) {
            throw escript::ValueError(
                "NodeMapping::assign(): target has negative entry.");
        }

        numTargets = (range.first <= range.second) ? (range.second + 1) : 0;

        target = new index_t[numNodes];
        map = new index_t[numTargets];

        bool err = false;

#pragma omp parallel
        {
#pragma omp for
            for (index_t n = 0; n < numNodes; ++n) {
                target[n] = newTarget[n];

                if (target[n] != unused)
                    map[target[n]] = n;
            }

            // Sanity check
#pragma omp for
            for (index_t i = 0; i < numTargets; ++i) {
                if (map[i] == -1) {
#pragma omp critical
                    err = true;
                }
            }
        }

        if (err) {
            throw escript::ValueError(
                "NodeMapping::assign(): target does not define a continuous labeling.");
        }
    }

    /// Returns the number of target nodes (number of items in the map array)
    inline dim_t getNumTargets() const { return numTargets; }

    /// Size of `target` (number of SEM nodes)
    dim_t numNodes;

    /// Target[i] defines the target of SEM node i=0,...,numNodes
    index_t* target;

    /// Size of `map` (number of target nodes, e.g. DOF, reduced DOF, etc.)
    dim_t numTargets;

    /// Maps the target nodes back to the SEM nodes: target[map[i]]=i
    index_t* map;
};

}  // namespace shirley


#endif  // __SHIRLEY_NODEMAPPING_H__
