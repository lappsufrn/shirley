
/*****************************************************************************
 *
 * esys-escript:
 * Copyright (c) 2003-2021 by The University of Queensland
 * http://www.uq.edu.au
 *
 * Primary Business: Queensland, Australia
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Development until 2012 by Earth Systems Science Computational Center (ESSCC)
 * Development 2012-2013 by School of Earth Sciences
 * Development from 2014-2017 by Centre for Geoscience Computing (GeoComp)
 * Development from 2019 by School of Earth and Environmental Sciences
 * 
 * Shirley domain module:
 * Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
 * and The University of Queensland
 * https://www.ufrn.br
 * http://www.uq.edu.au
 * 
 * Development from 2018 by Departamento de Engenharia de Computação e Automação
 * 
 * Primary Business: Rio Grande do Norte, Brazil
 * Licensed under the Apache License, version 2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 **
 *****************************************************************************/

#ifndef __SHIRLEY_ELEMNODEDERIVATIVES2D_H__
#define __SHIRLEY_ELEMNODEDERIVATIVES2D_H__


#include <vector>

#include "ShirleyException.h"
#include "NodeDerivatives2d.h"

namespace shirley {

/**
 * Class to store the partial derivatives of basis functions Ψ_i in
 * relation to spatial directions ξ (xi) and η (eta) for all SEM
 * nodes located at quadrature points (ξ_j,η_j) within an element.
 * The expressions for Ψ_i(ξ_j,η_j) are those of Eqs. (1.8) and (3.2)
 * at doi:10.1093/imamat/hxh077 (looking at Eq. (1.4) is also a good idea).
 */

class ElemNodeDerivatives2d {
public:
    // A no-parameter constructor is provided so that this class can
    // be used as a value in instances of std::map, std::unordered_map
    // and the like
    ElemNodeDerivatives2d() = default;

    explicit ElemNodeDerivatives2d(int numNodes) :
        m_nodeDerivatives(numNodes)
    {
        for (int i = 0; i < numNodes; ++i) {
            NodeDerivatives2d& nodeDerivs = m_nodeDerivatives[i];
            nodeDerivs.m_dpsi_dxi.resize(numNodes);
            nodeDerivs.m_dpsi_deta.resize(numNodes);
        }
    }

    ~ElemNodeDerivatives2d()
    {
        m_nodeDerivatives.clear();
    }

public:
    // <m_nodeDerivatives> stores the partial derivatives of Ψ_i(ξ_j,η_j)
    // for all (ξ_j,η_j) quadrature points within an element.
    // Each item in <m_nodeDerivatives> corresponds to a (ξ_j,η_j)
    // coordinate pair.

    std::vector<NodeDerivatives2d> m_nodeDerivatives;
};

}  // namespace shirley


#endif //__SHIRLEY_ELEMNODEDERIVATIVES2D_H__
