#!/bin/bash

##############################################################################
#
# esys-escript:
# Copyright (c) 2003-2021 by The University of Queensland
# http://www.uq.edu.au
#
# Primary Business: Queensland, Australia
# Licensed under the Apache License, version 2.0
# http://www.apache.org/licenses/LICENSE-2.0
#
# Development until 2012 by Earth Systems Science Computational Center (ESSCC)
# Development 2012-2013 by School of Earth Sciences
# Development from 2014 by Centre for Geoscience Computing (GeoComp)
# Development from 2019 by School of Earth and Environmental Sciences
#
# Shirley domain module:
# Copyright (c) 2018-2021 by Universidade Federal do Rio Grande do Norte
# and The University of Queensland
# https://www.ufrn.br
# http://www.uq.edu.au
#
# Development from 2018 by Departamento de Engenharia de Computação e Automação
#
# Primary Business: Rio Grande do Norte, Brazil
# Licensed under the Apache License, version 2.0
# http://www.apache.org/licenses/LICENSE-2.0
#
##############################################################################

# Echoes commands and expands variables
#set -x

# Include paths
SHIRLEY_INCLUDE_PATH="./include"
PYTHON_INCLUDE_PATH="/usr/include/python3.8"
BOOST_PYTHON_INCLUDE_PATH="/usr/include"
ESCRIPT_INCLUDE_PATH="/path/to/escript-5.7/include"

INCLUDE_PATHS="-I${SHIRLEY_INCLUDE_PATH} \
  -I${PYTHON_INCLUDE_PATH} \
  -I${BOOST_PYTHON_INCLUDE_PATH} \
  -I${ESCRIPT_INCLUDE_PATH}"

# Library paths
PYTHON_LIB_PATH="."
BOOST_PYTHON_LIB_PATH="."
ESCRIPT_LIB_PATH="/path/to/escript-5.7/lib"

LIB_PATHS="-L${PYTHON_LIB_PATH} \
  -L${BOOST_PYTHON_LIB_PATH} \
  -L${ESCRIPT_LIB_PATH}"

# Library names
# All these library names resolve to "lib${LIB_NAME}.so"
PYTHON_LIB_NAME="python3.8"
BOOST_PYTHON_LIB_NAME="boost_python38"
ESCRIPT_LIB_NAME="escript"
#RIPLEY_LIB_NAME="ripley"
#PASO_LIB_NAME="paso"

#LIB_NAMES="-l${PYTHON_LIB_NAME} \
#  -l${BOOST_PYTHON_LIB_NAME} \
#  -l${ESCRIPT_LIB_NAME} \
#  -l${RIPLEY_LIB_NAME} \
#  -l${PASO_LIB_NAME}"
LIB_NAMES="-l${PYTHON_LIB_NAME} \
  -l${BOOST_PYTHON_LIB_NAME} \
  -l${ESCRIPT_LIB_NAME}"

# Compiler flags.
# -Werror worked fine in Escript 5.2, but Escript 5.4 includes a few
# Clang specific pragmas that caused compilation errors, so that flag
# has been removed.
#CPP_FLAGS="-Wall -Werror -fpic -fopenmp -O3 -g -std=c++11 -march=native"
CPP_FLAGS="-Wall -fpic -fopenmp -O3 -g -std=c++11 -march=native"

# Source files and object files
SOURCE_FILES="\
  NodeFile.cpp \
  NodeFile_createDenseLabelings.cpp \
  NodeFile_createMappings.cpp \
  NodeFile_gather.cpp \
  ElementFile.cpp \
  ElementFile_createColoring.cpp \
  ElementFile_distributeByRankOfDOF.cpp \
  ShirleyDomain.cpp \
  Mesh_distributeByRankOfDOF.cpp \
  Mesh_optimizeDOFDistribution.cpp \
  Mesh_optimizeDOFLabeling.cpp \
  Mesh_readGmsh.cpp \
  Mesh_resolveNodeIds.cpp \
  Util.cpp \
  ShapeTable.cpp \
  Assemble_NodeCoordinates.cpp \
  Assemble_getSize.cpp \
  IndexList.cpp \
  LobattoPolynomials.cpp \
  VdmSystemsTri3.cpp \
  Jacobian2d.cpp \
  IWavelet.cpp \
  Ricker.cpp \
  shirleycpp.cpp"

OBJECT_FILES="\
  NodeFile.o \
  NodeFile_createDenseLabelings.o \
  NodeFile_createMappings.o \
  NodeFile_gather.o \
  ElementFile.o \
  ElementFile_createColoring.o \
  ElementFile_distributeByRankOfDOF.o \
  ShirleyDomain.o \
  Mesh_distributeByRankOfDOF.o \
  Mesh_optimizeDOFDistribution.o \
  Mesh_optimizeDOFLabeling.o \
  Mesh_readGmsh.o \
  Mesh_resolveNodeIds.o \
  Util.o \
  ShapeTable.o \
  Assemble_NodeCoordinates.o \
  Assemble_getSize.o \
  IndexList.o \
  LobattoPolynomials.o \
  VdmSystemsTri3.o \
  Jacobian2d.o \
  IWavelet.o \
  Ricker.o \
  shirleycpp.o"

# Remove old object files
#echo "Removing old object files..."
#rm *.o

# Compilation
echo "Compiling source files..."
g++ ${CPP_FLAGS} ${INCLUDE_PATHS} ${LIB_PATHS} -c ${SOURCE_FILES} ${LIB_NAMES}

OUTPUT_LIB_NAME="libshirley-release.so"

echo "Linking object files to generate shared library..."
g++ ${CPP_FLAGS} -shared -o ${OUTPUT_LIB_NAME} ${OBJECT_FILES}

if [ -f "${OUTPUT_LIB_NAME}" ]; then
  echo "${OUTPUT_LIB_NAME} generated."
else
  echo "Error generating ${OUTPUT_LIB_NAME}."
  echo "Please check compiler output for any error messages."
fi
