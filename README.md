
## The Shirley software library - an esys-escript domain module

Copyright (c) 2018-2021 by
[Universidade Federal do Rio Grande do Norte][link-ufrn]
and [The University of Queensland][link-uq]

### 1. Overview
Here you will find instructions on how to compile and use "Shirley",
a software library produced as a research project to assess the use of
[Hilbert](https://en.wikipedia.org/wiki/Hilbert_curve)
[space-filling curves](https://en.wikipedia.org/wiki/Space-filling_curve)
to reorder mesh-related data for use with the
[spectral element method (SEM)][link-sem],
aiming to attain fewer cache misses, better locality of data reference and
faster execution speed. The technique is demonstrated with a simulator of
[wave-type](https://en.wikipedia.org/wiki/Wave_equation)
[partial differential equations (PDEs)](https://en.wikipedia.org/wiki/Partial_differential_equation)
in 2D domains using the SEM over unstructured meshes of triangular elements.
The Shirley library provides other memory reordering strategies besides Hilbert
curves as a means to perform memory-efficient data traversals. Shirley
leverages [esys-escript](https://esys-escript.github.io/), an open-source
mathematical modeling tool that provides
[finite element method (FEM)](https://en.wikipedia.org/wiki/Finite_element_method)
and SEM capabilities. Esys-escript enables users to describe and implement
mathematical models using [Python][link-python] scripts.

Whereas the frontend through which users interact with esys-escript is the
Python language, under the hood esys-escript uses natively compiled shared
libraries written in C++. These native libraries allow esys-escript to fully
utilize all resources of the underlying hardware, such as multi-threading
and specialized CPU features. The standard distribution of esys-escript
provides four domain modules to solve PDEs, called Finley, Dudley, Ripley and
Speckley, each of them offering unique capabilities. The Shirley library
extends esys-escript with a new domain module, and users can employ its
features through Python scripts in the same way as with the standard domain
modules.

### 2. Dependencies
Before you begin, make sure your system meets the following dependencies:

- A C++11-capable compiler. [G++](https://gcc.gnu.org/) (versions 4.8, 7.3, 7.5
  and 9.3) was used during development and testing;
- [Python][link-python] version 3.6 or later;
- [Boost](https://www.boost.org/) C++ libraries, version 1.67 or later;
- (Optional) [Gmsh][link-gmsh] version 3.0.6 or later.
- (Optional) [Intel VTune Profiler][link-vtune] version v2020 (build 605129) or
  later.

Software development for this research was done on GNU/Linux systems running
[Ubuntu](https://ubuntu.com/) versions 18.04.5 LTS and 20.04.2 LTS. Tests and
experiments were conducted in a GNU/Linux supercomputer running
[CentOS](https://www.centos.org/) 6.5 64-bit in the
[High Performance Computing Center (NPAD/UFRN)](http://npad.imd.ufrn.br/) at
[Universidade Federal do Rio Grande do Norte (UFRN)][link-ufrn].

Other compilers and operating systems should work, too, but they are completely
untested.

### 3. Compile esys-escript v5.7
You need esys-escript version 5.7 to compile the Shirley module and run the
experiments described here. The official web page of esys-escript, its GitHub
repository and its releases can be found
[here](https://esys-escript.github.io/),
[here](https://github.com/esys-escript/esys-escript.github.io/)
and [here](https://github.com/esys-escript/esys-escript.github.io/releases).

Whereas esys-escript provides precompiled binaries for some platforms, for the
purposes of this research we recommend to compile it from source. This practice
provides greater control in the generation of the shared libraries required to
build the Shirley module. Before compiling esys-escript in a Ubuntu 20.04 LTS
system, we recommend to run the following commands:

```
sudo apt install subversion git g++ scons
sudo apt install libboost-python-dev libboost-numpy-dev libboost-iostreams-dev
sudo apt install libopenmpi-dev libsuitesparse-dev
sudo apt install libnetcdf-dev libnetcdf-c++4-dev libnetcdf-cxx-legacy-dev
sudo apt install python3-pip python-numpy python3-scipy python3-pyproj
sudo apt install python3-gdal python3-sympy
sudo apt install gmsh
python3 -m pip install numpy
python3 -m pip install --upgrade matplotlib
```

The source code, install guide and official documentation of esys-escript 5.7
can be found
[here](https://github.com/esys-escript/esys-escript.github.io/archive/refs/tags/5.7.tar.gz),
[here](https://github.com/esys-escript/esys-escript.github.io/blob/5.7/install.pdf)
and [here](https://github.com/esys-escript/esys-escript.github.io/blob/5.7/docs.zip).

After you extract the source code of esys-escript in the directory of your
choice, you can compile it in a Ubuntu 20.04 LTS system using the following
command:

```
scons -j1 options_file=scons/templates/focal_options.py boost_libs='boost_python38'
```

To compile esys-escript in different operating systems, refer to the install
guide and official documentation.

After you compile esys-escript in your system, take note of the directory
where you compiled it, as you will have to supply it when assigning the
`ESCRIPT_INCLUDE_PATH` and `ESCRIPT_LIB_PATH` variables in several shell
scripts later.

### 4. Compile the Shirley module
Esys-escript has four standard modules: Finley, Dudley, Ripley and Speckley.
For the purposes of this research, a new module named Shirley was created in
the form of a shared library. No precompiled binaries are provided, and you
need to compile the Shirley module from source. The steps are as follows:

1. In the `shirley/` directory, open the `compile-lib-release.sh` shell script
  in your text editor. Change the `PYTHON_INCLUDE_PATH`,
  `BOOST_PYTHON_INCLUDE_PATH`, `ESCRIPT_INCLUDE_PATH`, `BOOST_PYTHON_LIB_PATH`,
  `ESCRIPT_LIB_PATH`, `PYTHON_LIB_NAME`, `BOOST_PYTHON_LIB_NAME` and
  `ESCRIPT_LIB_NAME` variables so they point to the appropriate paths and file
  names in your system;

2. Open a Bash shell, navigate to the `shirley/` directory and execute the
  `compile-lib-release.sh` shell script. When the compilation is complete, you
  will see the message `"libshirley-release.so generated"`, and a file named
  `libshirley-release.so` will be available in the same directory as the shell
  script. The intermediate `.o` files created during compilation may be safely
  deleted;

3. Navigate to the `shirley/lib/` directory and execute the `release-link.sh`
  shell script to create a symbolic link to the `libshirley-release.so` file
  in the parent directory;

4. Navigate to the `shirley/shirley/` directory and execute the
  `release-link.sh` shell script to create a symbolic link to the
  `libshirley-release.so` file in the parent directory.

After you compile the Shirley module, take note of the directory where you
compiled it, as you will have to supply it when assigning the
`SHIRLEY_LIB_PATH` and `SHIRLEY_PATH` variables in several shell scripts later.
Now you can proceed to test the module and run its experiments.

### 5. Test the Shirley module
To test whether your compilation of the Shirley module is working correctly,
the steps are as follows:

1. In the `check-version/` directory, open the `check-version.sh` shell script
  in your text editor;

2. If your system provides [Environment Modules][link-envmod], check whether
  the lines that begin with `"module load"` are valid and/or necessary to
  provide the capabilities required by the Shirley module. If your system does
  not provide Environment Modules, or if it does but you believe you do not
  need to use Environment Modules, simply comment out the lines that begin with
  `"module load"`;
  
3. Change the `BOOST_LIB_PATH`, `ESCRIPT_LIB_PATH`, `SHIRLEY_LIB_PATH`,
  `ESCRIPT_PATH` and `SHIRLEY_PATH` so they point to the appropriate paths in
  your system;

4. Open a Bash shell, navigate to the `check-version/` directory and run the
  `check-version.sh` shell script. If your Shirley compilation is working
  correctly, you will see a message similar to the one below:

```
    Shirley version = 20211020
```

### 6. Run the first experiment - Bump-shaped traveling wave
The first experiment is meant to verify whether the Shirley module produces
consistent results. It does so by numerically propagating a
[bump-shaped traveling plane wave](https://en.wikipedia.org/wiki/Traveling_plane_wave)
with well-defined analytical behavior and checking whether the simulated
results are sufficiently close to the exact analytical values. This experiment
was designed to run in a GNU/Linux system with [Slurm][link-slurm] version
v15.08.7 and [Environment Modules][link-envmod] v3.2.9. However, this is not a
hard requirement. The steps are as follows:

1. In the `bump-test/meshes` directory, unzip the `bump-mesh.msh.zip` file, as
  it contains the mesh file used by the experiment;

2. In the `bump-test/` directory, open the `bump-test.sh` shell script in your
  text editor;

3. If your system provides [Environment Modules][link-envmod], check whether
  the lines that begin with `"module load"` are valid and/or necessary to
  provide the capabilities required by the Shirley module. If your system does
  not provide Environment Modules, or if it does but you believe you do not
  need to use Environment Modules, simply comment out the lines that begin with
  `"module load"`;

4. Change the `BOOST_LIB_PATH`, `ESCRIPT_LIB_PATH`, `SHIRLEY_LIB_PATH`,
  `ESCRIPT_PATH` and `SHIRLEY_PATH` variables so they point to the appropriate
  paths in your system;

5. In the `bump-test/` directory, run the `bump-test.sh` shell script, or
  submit it using `"sbatch"`, and wait for the script to complete. It should
  execute quickly. It generates about 290MB of data in output files, so make
  sure there is enough storage space available. The output messages and files
  of the experiment will be generated in the `bump-test/output/` directory. The
  intermediate pressure values in the domain during wave propagation will be
  saved to `.csv` files. They can be parsed by [NumPy](https://www.numpy.org/)
  and plotted into figures by [Matplotlib](https://matplotlib.org/), as
  demonstrated in the `bump-test/plot-wave.py` Python script.

If you would like to adjust any aspect of the simulation, make the desired
changes in the `bump-test/bump-test.sh` shell script and/or the
`bump-test/bump-test.py` Python script. You can also change the way figures are
plotted by editing the `bump-test/plot-wave.py` Python script as desired.

To view the mesh used in this experiment, use [Gmsh][link-gmsh] to open the
`bump-mesh.msh` file in the `bump-test/meshes/` directory. If you would like to
make adjustments to the mesh, use your text editor to modify the `bump-mesh.geo`
file in the `bump-test/meshes/` directory and regenerate the mesh by running
the `make-bump-mesh.sh` shell script. The mesh generation procedure requires
Gmsh.

### 7. Run the second experiment - Benchmark 1
The second experiment measures the CPU time required to simulate a single time
step of the propagation of a wave in meshes of mostly homogeneous edge size,
using several polynomial orders, mesh granularities and memory reordering
strategies. This experiment was designed to run in a GNU/Linux system with
[Slurm][link-slurm] version v15.08.7 and [Environment Modules][link-envmod]
v3.2.9. However, this is not a hard requirement. The steps are as follows:

1. In the `bench-1/meshes` directory, unzip the files whose names end with
  `.msh.zip`, as they contain the mesh files used by the experiment;

2. In the `bench-1/` directory, according to the number of processors you have
  or the number of threads you want to use, open the
  `run-regular-thr-<number>-all.sh` shell script in your text editor;

3. If your system provides [Environment Modules][link-envmod], check whether
  the lines that begin with `"module load"` are valid and/or necessary to
  provide the capabilities required by the Shirley module. If your system does
  not provide Environment Modules, or if it does but you believe you do not
  need to use Environment Modules, simply comment out the lines that begin with
  `"module load"`;

4. Change the `BOOST_LIB_PATH`, `ESCRIPT_LIB_PATH`, `SHIRLEY_LIB_PATH`,
  `ESCRIPT_PATH` and `SHIRLEY_PATH` variables so they point to the appropriate
  paths in your system;

5. In the `bench-1/` directory, run the `run-regular-thr-<number>-all.sh` shell
  script, or submit it using `"sbatch"`, and wait for the script to complete
  (**WARNING**: this can take several hours). The resulting files for the
  experiment will be generated in the `bench-1/regular-output/thr-<number>/`
  directory. Look for files named `exec-sim-<a>-<b>-<c>.txt`, where `<a>` is
  the mesh granularity (0=250 thousand elements, 1=500 thousand elements,
  2=1 million elements), `<b>` is the memory reordering strategy (0=node
  connectivity, 1=node distance, 2=Hilbert curves, 3=no strategy) and `<c>` is
  the polynomial order (5, 6 or 7). To find out the CPU time required to
  compute a single time step of the wave, use `"cat"` and `"grep"`:

```
    cat exec-sim-<a>-<b>-<c>.txt | grep -i "per step"
```

If you would like to adjust any aspect of the simulation, make the desired
changes in the `bench-1/run-regular-thr-<number>-all.sh` shell script and/or
the `bench-1/regular-sim.py` Python script. To view the meshes used in this
experiment, use [Gmsh][link-gmsh] to open any of the `.msh` files in the
`bench-1/meshes/` directory. If you would like to make adjustments to the
meshes, use your text editor to modify the relevant `.geo` file in the
`bench-1/meshes/` directory and regenerate the mesh by running the appropriate
shell script `bench-1/meshes/make-<g>-mesh.sh`, where `<g>` is the mesh
granularity. The mesh generation procedure requires Gmsh.

If [Intel VTune Profiler][link-vtune] is available in your system, it is also
possible to measure the number of last-level cache (LLC) misses and the
percentage of stalled slots in the memory pipeline (SSMP) caused by the
simulation. The steps are as follows:

6. In the `bench-1/meshes` directory, in case you have not done so before,
  unzip the files whose names end with `.msh.zip`, as they contain the mesh
  files used by the experiment;

7. In the `bench-1/` directory, open the `run-vtune-all.sh` shell script in
  your text editor;

8. According to the number of processors you have or the number of threads
  you want to use, change the `OMP_NUM_THREADS` variable appropriately;

9. Change the `BOOST_LIB_PATH`, `ESCRIPT_LIB_PATH`, `SHIRLEY_LIB_PATH`,
  `ESCRIPT_PATH` and `SHIRLEY_PATH` variables so they point to the appropriate
  paths in your system;

10. In the `bench-1/` directory, run the `run-vtune-all.sh` shell script and
  wait for its completion (**WARNING**: this can take several hours). The
  resulting files for the experiment will be generated in the
  `bench-1/vtune-output/thr-<OMP_NUM_THREADS>/` directory. Look for files named
  `exec-sim-<a>-<b>-<c>.txt` and `exec-rpt-<a>-<b>-<c>.csv`, where `<a>` is the
  mesh granularity (0=250 thousand elements, 1=500 thousand elements, 2=1
  million elements), `<b>` is the memory reordering strategy (0=node
  connectivity, 1=node distance, 2=Hilbert curves, 3=no strategy) and `<c>` is
  the polynomial order (5, 6 or 7). To find out the CPU time required to
  compute a single time step of the wave, use cat and grep:

```
    cat exec-sim-<a>-<b>-<c>.txt | grep -i "per step"
```

  To find out the number of LLC misses and the SSMP percentages, respectively,
  the commands are:

```
    cat exec-rpt-<a>-<b>-<c>.csv | grep -i llc
    cat exec-rpt-<a>-<b>-<c>.csv | grep -i "memory bound"
```

If you would like to adjust any aspect of the simulation, make the desired
changes in the `bench-1/run-vtune-all.sh` shell script and/or the
`bench-1/vtune-sim.py` Python script.

### 8. Run the third experiment - Benchmark 2
The third experiment is essentially identical to the second experiment (see
Section 7), the only difference being that it uses meshes with large variations
in element sizes. Aside from the fact that the relevant files for the second
experiment are located in the `bench-2/` directory rather than the `bench-1/`
directory, the instructions for the third experiment are the same as those
given for the second experiment.

### 9. Run the fourth experiment - Wave simulation in a multilayered domain
The fourth experiment demonstrates the propagation of a wave (more specifically,
a [Ricker wavelet](https://en.wikipedia.org/wiki/Ricker_wavelet)) in a
multilayered domain, a typical scenario in applications such as synthetic
seismic data analysis or microseismic data migration. Rather than measuring
execution performance, the focus of this experiment is on showing that the
[SEM][link-sem] tied to unstructured meshes of triangular elements that is
proposed in this research is capable of properly simulating waves in complex,
multilayered geological structures. This experiment was designed to run in a
GNU/Linux system with [Slurm][link-slurm] version v15.08.7 and
[Environment Modules][link-envmod] v3.2.9. However, this is not a hard
requirement. The steps are as follows:

1. In the `wave-sim/meshes` directory, unzip the files whose names end with
  `.msh.zip`, as they contain the mesh files used by the experiment;

2. In the `wave-sim/` directory, open the `run-wave-sim.sh` shell script in
  your text editor;

3. According to the number of processors you have or the number of threads
  you want to use, change the `OMP_NUM_THREADS` variable;

4. If your system provides [Environment Modules][link-envmod], check whether
  the lines that begin with `"module load"` are valid and/or necessary to
  provide the capabilities required by the Shirley module. If your system does
  not provide Environment Modules, or if it does but you believe you do not
  need to use Environment Modules, simply comment out the lines that begin with
  `"module load"`;

5. Change the `BOOST_LIB_PATH`, `ESCRIPT_LIB_PATH`, `SHIRLEY_LIB_PATH`,
  `ESCRIPT_PATH` and `SHIRLEY_PATH` so they point to the appropriate paths in
  your system;

6. In the `wave-sim/` directory, run the `run-wave-sim.sh` shell script, or
  submit it using `"sbatch"`, and wait for the script to complete. Some words
  of warning: this is a **VERY** CPU-intensive simulation -- in our test
  hardware, according to the time step size used, it takes between eight and
  nine days to complete. In addition, it generates about 72GB of data in output
  files, so make sure there is enough storage space available. The output
  messages and files of the experiment will be generated in the
  `wave-sim/output/` directory. To find out the CPU time required to compute
  the complete simulation, use `"cat"` and `"grep`":

```
    cat wave-sim-output.txt | grep -i "elapsed time"
```

  The intermediate pressure values in the domain during wave propagation will
  be saved to `.csv` files. They can be parsed by
  [NumPy](https://www.numpy.org/) and plotted into figures by
  [Matplotlib](https://matplotlib.org/), as demonstrated in the
  `wave-sim/plot-wave.py` Python script. In the `plot-wave.py` Python script,
  the pressure values are amplified by a multiplication factor in order to make
  them stand out when plotted.

If you would like to adjust any aspect of the simulation, make the desired
changes in the `wave-sim/run-wave-sim.sh` shell script and/or the
`wave-sim/wave-sim.py` Python script. You can also change the way figures are
plotted by editing the `wave-sim/plot-wave.py` Python script as desired.

To view the meshes used in this experiment, use [Gmsh][link-gmsh] to open any
of the `.msh` files in the `wave-sim/meshes/` directory. If you would like to
make adjustments to the meshes, use your text editor to modify the relevant
`.geo` file in the `wave-sim/meshes/` directory and regenerate the mesh by
running the appropriate shell script `wave-sim/meshes/make-<g>-mesh.sh`, where
`<g>` is the mesh granularity (250k=250 thousand elements, 500k=500 thousand
elements, 1m=1 million elements). The mesh generation procedure requires Gmsh.

### 10. License
Unless otherwise stated, all files contained in this distribution are licensed
under the
[Apache License, version 2.0](https://www.apache.org/licenses/LICENSE-2.0).

### 11. Research results

The results of this research were published in the December 2021 issue of the
[Computers & Geosciences](https://www.journals.elsevier.com/computers-and-geosciences)
scientific journal, and are available in the link below:

https://doi.org/10.1016/j.cageo.2021.104938

A detailed discussion of this research is available in the form of a doctoral
thesis, which can be freely downloaded from the institutional repository of
[Universidade Federal do Rio Grande do Norte][link-ufrn]:

https://repositorio.ufrn.br/handle/123456789/45677

### 12. Research team
[Roger R. F. Araújo, PhD](https://www.researchgate.net/profile/Roger-Araujo-2)
([roger_rf@dca.ufrn.br](mailto:roger_rf@dca.ufrn.br))  
[Programa de Pós-Graduação em Engenharia Elétrica e de Computação](http://www.posgraduacao.ufrn.br/ppgeec),  
[Universidade Federal do Rio Grande do Norte][link-ufrn]

[Prof. Lutz Gross, PhD](https://sees.uq.edu.au/profile/9065/lutz-gross)
([l.gross@uq.edu.au](mailto:l.gross@uq.edu.au))  
[School of Earth and Environmental Sciences](https://sees.uq.edu.au/),  
[The University of Queensland][link-uq]

[Prof. Samuel Xavier-de-Souza, PhD](https://www.dca.ufrn.br/~samuel/)
([samuel@dca.ufrn.br](mailto:samuel@dca.ufrn.br))  
[Departamento de Engenharia de Computação e Automação](https://www.dca.ufrn.br),  
[Universidade Federal do Rio Grande do Norte][link-ufrn]

[link-ufrn]: https://www.ufrn.br/
[link-uq]: https://www.uq.edu.au
[link-sem]: https://en.wikipedia.org/wiki/Spectral_element_method
[link-python]: https://www.python.org
[link-gmsh]: https://www.gmsh.info
[link-vtune]: https://software.intel.com/content/www/us/en/develop/tools/oneapi/components/vtune-profiler.html
[link-slurm]: https://slurm.schedmd.com/
[link-envmod]: http://modules.sourceforge.net/
